#!./luajit
--------------------------------------------------------------------------------

require ('config')

arguments = get_arguments()

if arguments.test ~= nil then
    test_dir = arguments.test
    table_for_check = get_files_from_test_dir(test_dir)
    run_check(table_for_check, simple_patterns, true)
end


if arguments.start ~= nil then
    table_for_check = get_files_from_nginx_log()
    run_check(table_for_check, simple_patterns)
end

-- Set output method
function send_output()
    if arguments.send ~= 'json' then
        message = ''
        for domain,domain_params in pairs(WORKING_TABLE) do
            for request,request_params in pairs(domain_params.requests) do
                if #request_params.virus_types ~= 0 then
                    for k,virus_type in pairs(request_params.virus_types) do
                        if not string.find(virus_type,'exclude_') then
                            --print(domain,request, DataDumper(request_params.virus_types))
                             message = message..string.format('%s %s %s \n',domain,request, DataDumper(request_params.virus_types))
                        end
                    end
                end
            end
        end
        send_logs(host, message)
    else
        remove_empty_request(WORKING_TABLE)
        local json_table = cjson.encode(WORKING_TABLE)
        send_logs(host, json_table)
    end
end


if arguments.nginx ~= nil then
    date, datetime = hours_ago(12)
    table_for_check = get_files_from_nginx_log()
end

if arguments.house ~= nil then
    date, datetime = hours_ago(12)
    table_for_check = get_files_from_clickhouse_log(hostname, date, datetime)
end

if arguments.get_logs ~= nil then
    local table_log = get_all_log()
    local message = ''
    for k, line in pairs(table_log) do
        message = message .. line .. '\n'
    end
    send_logs(host, message)
end


if arguments.nginx ~= nil or arguments.house ~= nil then
    -- Start check
    run_check(table_for_check, simple_patterns)
    run_additional_check(table_for_check, misc_signatures)

    -- Start output
    if arguments.send ~= 'json' then
        message = ''
        for domain,domain_params in pairs(WORKING_TABLE) do
            for request,request_params in pairs(domain_params.requests) do
                if #request_params.virus_types ~= 0 then
                    for k,virus_type in pairs(request_params.virus_types) do
                        if not string.find(virus_type,'exclude_') then
                            --print(domain,request, DataDumper(request_params.virus_types))
                             message = message..string.format('%s %s %s %s \n',domain,request, DataDumper(request_params.virus_types), request_params.ip )
                        end
                    end
                end
            end
        end
        send_logs(host, message)
    else
        remove_empty_request(WORKING_TABLE)
        local json_table = cjson.encode(WORKING_TABLE)
        send_logs(host, json_table)
    end

end
