#!./luajit
--------------------------------------------------------------------------------

package.path = './libs/?.lua;' .. package.path
package.cpath = './libs/?.so;' .. package.cpath

rex   = require('rex_pcre')
curl  = require('lcurl')
cjson = require('cjson')
redis = require('redis')


require ('DataDumper')
require ('helper')
require ('checker')
require ('form')

default_nginx_log_path    = '/var/log/nginx/access.log'
default_nginx_vhosts_path = '/etc/nginx/sites-enabled/'

clickhouse_url   = 'http://192.168.13.2:8123/?query='
clickhouse_auth  = 'root:incidentsOrDie120'

host = getHostname()
hostname = string.match(host, '(.+)%.beget')
