#!./luajit
--------------------------------------------------------------------------------

function get_info(query)
  output = nil
  encod_quer = urlencode(query)
  url = clickhouse_url..encod_quer
  curl.easy()
    :setopt_userpwd(clickhouse_auth)
    :setopt_url(url)
    :setopt_writefunction(return_out)
    :perform()
  :close()
  return output
end

function return_out(string)
  if output == nil
   then output = string
  else output = output..string
  end
end

function dump(...)
    print(DataDumper(...), "\n---")
end

function explode(p,d)
   local t, ll
   t={} ll=0
   if(#p == 1) then return {p} end
      while true do
         l=string.find(p,d,ll,true)
         if l~=nil then
            table.insert(t, string.sub(p,ll,l-1))
            ll=l+1
         else
            table.insert(t, string.sub(p,ll))
            break
         end
      end
   return t
end

function parse_nginx_log(p)
   local t, ll d=' '
   t={} ll=0
   if(#p == 1) then return {p} end
      while true do
         l=string.find(p,d,ll,true)
         if l~=nil then
            table.insert(t, string.sub(p,ll,l-1))
            ll=l+1
         else
            table.insert(t, string.sub(p,ll))
            break
         end
      end
   return t[1], t[2], t[7]:sub(2), t[8], t[10]
end

function parse_clickhouse_log(p)
   local t, ll d='\009' d=' '
   t={} ll=0
   if(#p == 1) then return {p} end
      while true do
         l=string.find(p,d,ll,true)
         if l~=nil then
            table.insert(t, string.sub(p,ll,l-1))
            ll=l+1
         else
            table.insert(t, string.sub(p,ll))
            break
         end
      end
      local exploded_request = explode(t[3], ' ')
   return t[1], t[2], exploded_request[1], exploded_request[2], t[4]
end

function get_file(file)
    local f = io.open(file, "r")
    if f == nil then error('Cant read file '..file) end
    local content = f:read("*all")
    f:close()
    return content
end

function trim(s)
    local n = s:find"%S"
    return n and s:match(".*%S", n) or ""
end

function count_element(table)
    local count = 0
    for k,v in pairs(table) do
        count = count + 1
    end
    return count
end

function return_out_z(string)
  if output_z == nil
   then output_z = string
  else output_z = output_z..string
  end
end


function send_logs(host, message)
  curl.easy()
    :setopt_url('http://bagetok.ru/vir_check/post.php')
    :setopt_writefunction(return_out_z)
    :setopt_httppost(curl.form()
      :add_content('host', host)
      :add_content('content', message)
    )
    :perform()
  :close()
end

function getHostname()
    local f = io.popen ("/bin/hostname")
    local hostname = f:read("*a") or ""
    f:close()
    hostname =string.gsub(hostname, "\n$", "")
    return hostname
end

function execute(command)
   local file = io.popen (command)
   local data = string.sub(file:read("*a"), 1, -2)
   file:close()
   if (data == "") then
       data = nil
   end
   return data
end

function list_dir(full_path) 
    local cmd_output = execute('ls '..full_path)
    local list_files = explode(cmd_output, '\n')
    return list_files
end

function get_arguments()
    local arg_conf = {}

    for k,argument in pairs(arg) do
        if argument:sub(0, 1) == '-' then
            local param = arg[k]:sub(2)
            local value = arg[k+1] or true
            arg_conf[param] = value
        end
    end

    return arg_conf
end

function urlencode(str)
   if (str) then
      str = string.gsub (str, "\n", "\r\n")
      str = string.gsub (str, "([^%w ])",
         function (c) return string.format ("%%%02X", string.byte(c)) end)
      str = string.gsub (str, " ", "+")
   end
   return str
end

function today()
    return os.date('%Y-%m-%d')
end

function hours_ago(hours)
    local t = os.date("*t")
    t.hour = t.hour - hours
    local ago = os.time(t)
    return os.date('%Y-%m-%d', ago), os.date('%Y-%m-%d %X', ago)
end

