
require 'signatures'
require 'rev_signatures'


function run_virfindgo(domain, root_dir)
    local output = execute('./beget_virfindgo -l  -p '..root_dir..' | wc -l')
    return output
end


function check_file(domain, path, patterns)
 
    local windows_file = ''
    local count_matched = 0
    local full_path = WORKING_TABLE[domain]['root_dir']..path
    local status, file_content = pcall(get_file,full_path)
    if status == false or file_content == nil then goto skip_check end

    for vir_type, patterns_list in pairs(patterns) do -- Begin check file by regexp

        count_matched = 0
        for number=1,#patterns_list  do
            local status, matched = pcall(rex.match, file_content, patterns_list[number])
            if status == false then print(matched, vir_type, full_path) goto skip_check end

            if matched ~= nil then
                if vir_type == 'FIRST_LINE' then
                    local exploded_file         = explode(file_content, '\n')
                    local exploded_file_windows = explode(file_content, '\r')
                    if #exploded_file_windows > 5 then
                        windows_file = '_WINDOWS'
                    else
                        windows_file = ''
                    end
                        
                    if string.len(exploded_file[1]) > 1000 then
                        if string.find(exploded_file[1],'eval') then
                            vir_type = 'FIRST_LINE_EVAL'..windows_file
                            count_matched = count_matched + 1
                        else
                            vir_type = 'FIRST_LINE_SIMPLE'..windows_file
                            count_matched = count_matched + 1
                        end
                    end
                else
                    count_matched = count_matched + 1
                end
            end
        end


        -- if all patterns for vir_type matched, add it
        if count_matched == #patterns_list then
            table.insert(WORKING_TABLE[domain].requests[path].virus_types, vir_type)
        end


    end  -- End check file by regexp
    ::skip_check::
end



function run_check(given_table, patterns, testing)

    for domain,info in pairs(given_table) do
        for path,v in pairs(info.requests) do
            local full_path = given_table[domain]['root_dir']..path
            if check_file(domain, path, patterns) == true then
                --given_table[domain].requests[path].virfindgo = run_virfindgo(domain, given_table[domain]['root_dir']) 
            end
        end
    end


    return given_table
end

function run_additional_check(given_table, patterns, testing)

    for domain,info in pairs(given_table) do
        for path,request_info in pairs(info.requests) do
            if #request_info.virus_types > 0 then
                local full_path = given_table[domain]['root_dir']..path
                if check_file(domain, path, patterns) == true then
                    --given_table[domain].requests[path].virfindgo = run_virfindgo(domain, given_table[domain]['root_dir'])
                end
            end
        end
    end


    return given_table
end


function remove_empty_request(given_table)
    for domain,domain_params in pairs(given_table) do
        for request,request_params in pairs(domain_params.requests) do
            if #request_params.virus_types == 0 then
                given_table[domain].requests[request] = nil
            end
        end
    end

    for domain,domain_params in pairs(given_table) do
        if next(domain_params.requests) == nil then
            given_table[domain] = nil
        end
    end

    return given_table
end

