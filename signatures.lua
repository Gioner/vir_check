
-- exclude
-- doubleval
-- /home/a/alexan0l/zernokorm-vereya.ru/public_html/d0xi3p39virusdie.php
--
--                 header("Cache-Control: no-cache, must-revalidate");
--

-- '(' must be escaped

simple_patterns = {
    ['exclude_comebaker']  = {[[comebacker.ru]]},  
    ['exclude_vbulletin']     = {[[vBulletin]]},
    ['exclude_phpbb']   = {[[phpBB3]]},
    ['exclude_other']  = {[[время для звонка]]},
    ['exclude_other2'] = {[[define('ADMIN_MODE', true)]]},
    ['exclude_bitrix']  = {[[1c-bitrix.ru]]},
    ['exclude_puzzle'] = {[[Alexander Myznikov]]},
    ['exclude_serval'] = {[[SerVal site safe PHP-scanner]]},
    ['exclude_socialclicker'] = {[[sociallocker]]},
    ['exclude_PunBB']           = {[[PunBB]]},
    ['exclude_other3'] = {[[<title>]]},
    ['exclude_other5'] = {[[a href]]},
    ['exclide_other6'] = {[[responsive_map]]},
    ['exclude_other4'] = {[[require('header.php')]]},
    ['exclude_other7'] = {[[DOCTYPE html]]},
    ['exclide_badsite_1'] = {[[CHEMIN]]},
    ['exclude_ami']    = {[[AMI_ENV_SETTINGS]]},
    ['exclude_wbs']    = {[[kernel/wbs.xml]]},
    ['exclude_glype']  = {[[glype.com]]},
    ['FIRST_LINE']     = {[[.{1100}]]},
    ['SIMPLE-1']       = {[[eval\(]], [[base64]]},
    ['FilesMan-1']     = {[[\Qpreg_replace("/.*/e"\E]],[[\$default_action\s*=\s*['"]FilesMan['"];]], [[\Qeval(\E]]},
    ['FilesMan-2']     = {[[\$default_action\s*=\s*['"]FilesMan['"];]], [[\$auth_pass\s*=\s*['"]#, qr#\$default_use_ajax\s*=\s*(true|false);]], [[\$default_charset\s*=\s*["']Windows-1251['"];]], [[\Qpreg_replace("/.*/e"\E]]},
    ['mini-shell1']    = {[[isset\(\$_REQUEST\[\'en\'\]\)\)\s+eval\(stripslashes\(\$_REQUEST\[\'en\'\]\)]]},
    ['mini-shell3']    = {[[\$_REQUEST\['\w+'\];\s*eval\(\$\w+\);\s*exit\(\);]],[[fopen\(\$\w+,\s*'w'\);\s*\$\w+\s*=\s*fwrite\(\$\w+,\s*\$\w+\);\s*fclose\(\$\w+\);\s*echo\s*\$\w+;\s*exit\(\);]]},
    ['mini-shell4']    = {[[(\$\w+)\s*=\s*\"[stop_STOP]+\";\s*\$\w+\s*=\s*(strtolower|strtoupper)\((\1\[\d\]\.?){5}\);]]},
    ['mini-shell5']    = {[[if\s*\(\s*\!\s*isset\(\$_REQUEST\[(chr\(\d+\)\.?){1,}\]\)\)]],[[\$\w+\s*=\s*eval\((chr\(\d+\)\.?){20,}]]},
    ['mini-shell6']    = {[[(\$\w+)\s*=\s*\"[PCT4BA6ODSE_pctbaodse]+\";\s*\$\w+\s*=\s*(strtolower|strtoupper)\((\1\[\d+\]\.?){13}\);]]},
    ['mini-shell7']    = {[[\$strings\s*=\s*\"as\"\;\$strings\s*\.=\s*\"sert\"\;]],[[\@\$strings\s*\(\s*str_rot13\s*\(\s*\'riny\s*\(\s*onfr64_qrpbqr\s*\(]]},
    ['mini-shell8']    = {[[try\s*\{\s*\@touch\s*\(\s*basename\s*\(\s*\$_SERVER\[SCRIPT_FILENAME\]\s*\)\s*\,\s*time\(\)[\-\+]\d+\s*\)\;\}]],[[(\$\w+)\s*=\s*[\'\"base64decode_\.]+\;\s*if\s*\(isset\s*\(\s*(\s*\$_REQUEST\[\w+\]\s*\))\s*\)\s*eval\s*\(\s*\1\(\2\s*\)]]},
    ['mini-shell11']   = {[[(.*)eval\(\$GLOBALS(.*)]],[[XML-RPC]]},
    ['mini-shell12']   = {[[if\( \$_POST\['_upl'\] == "Upload" \) {]]},
    ['mini-shell13']   = {[[if \(isset\(\$_POST\['proxy'\]\)\) {curl_setopt\(\$ch, CURLOPT_PROXY, \$_POST\['proxy'\]\);]]},
    ['shell-1']        = {[[\Q!function_exists("posix_getpwuid")\E]],[[\Qfunction posix_getpwuid(\E]],[[\Q!function_exists("posix_getgrgid")\E]],[[\Qfunction posix_getgrgid(\E]],[[\Q!function_exists("scandir")\E]]},
    ['shell-51']       = {[[md5\(\$_REQUEST\[]],"\\$_SESSION\\['LoGiN'\\]"},
    ['shell-10']       = {[[\@gzinflate\(\@base64_decode\(\@str_replace\(]],[[eval\(]],[[urldecode\(\$_POST\[]]},
    ['shell-11']       = {[[eval\s*\(\s*gzinflate\s*\(\s*base64_decode\s*\(([\'\"])[^\'\"]{100,}\1\s*\)\s*\)\s*\)\s*;]]},
    ['shell-12']       = {[[\<\?(php)?\s*(\$\w+)\s*=\s*['"\.\s]*b['"\.\s]*a['"\.\s]*s['"\.\s]*e['"\.\s]*6['"\.\s]*4['"\.\s]*_['"\.\s]*d['"\.\s]*e['"\.\s]*c['"\.\s]*o['"\.\s]*d['"\.\s]*e['"\.\s]*;\s*assert\s*\(\s*\2\s*\(\s*\S+\s*\)\s*\)\s*;\s*\?\>]]},
    ['shell-13']       = {[[\$v\s=\s\@ob_get_contents\(\)\;\s\@ob_clean\(\)\;\spassthru\(\$cmd\)\;\s\$result\s=\s\@ob_get_contents\(\)\;\s\@ob_clean\(\)\;\secho\s\$v\;]]},
    ['shell-14']       = {[[(\$\w+)\s*=\s*\"[^\"]+\";\s*\$\w+\s*=\s*(\1\[\d+\]\.?){12}]],[[(chr\(\d+\)\s*\.\s*\"[^\"]*\"\.?){8,}]],[[\$\w+\((\s*\$\w+\[\d+\]\s*\.?){5}\,\s*\$\w+\s*\,\"\d+\"\)\;]]},
    ['shell-15']       = {[[\$GLOBALS\['[^\']+'\]\s*=\s*Array\((base64_decode\(('[^\']*'\s*\.?){1,}\),?\s*){10,}]],[[if\s*\(\s*isset\s*\(\s*\$_POST\[.*\]\s*\)\s*\)\s*\{\s*echo\s*\$GLOBALS]]},
    ['shell-16']       = {[[\Qdie(PHP_OS.chr(49).chr(48).chr(43).md5(0987654321\E]]},
    ['shell-18']       = {[[(\$GLOBALS\[\'\w+\'\])\s*=\s*\"[^\"]+\";\s*\$GLOBALS\[(\1\[\d+\]\.?){4,}\]\s*=\s*(\1\[\d+\]\.?){3,}]],[[eval\(\$\w+\[\$GLOBALS\[\'\w+\'\]\[\d+\]\]\)]]},
    ['shell-22']       = {[[\$\w+\s*=\s*sprintf\([eval\'\!\s\.]+\s*\([base64_decode\'\s\.]+\s*\(\"\%s\"\)\)\'\,\s*\$\w+\)\;]],[[\$\w+\s*=\s*[asert\'\.\s]+\;]],[[\$\w+\(stripslashes\(\$\w+\)\)\;]]},
    ['wso-1']          = {[[function\s+wso[a-zA-Z]+\s*\(]],[[function\s+action[a-zA-Z]+\s*\(]],[[eval\(\$_POST\[]],[[\@base64_decode\(]]},
}

