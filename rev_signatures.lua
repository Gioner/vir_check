misc_signatures = {
  sig_0={
    "<\\?php\\s*if\\s*\\(isset\\(\\$_REQUEST\\[\\w+\\]\\)\\)\\s*\\{\\s*stripslashes\\(\\$_REQUEST\\[\\w+\\]\\(\\$_REQ.+?\\?>" 
  },
  sig_10={
    "<\\?php\\s+\\$[a-z].+?strtoupper.+?isset.+?eval\\s*\\(\\s*\\${\\s*\\$[a-z0-9]+\\s*}\\s*\\[\\s*'[a-z0-9]+'\\s*\\]\\s*\\);\\s*}\\s*\\?>\\s*" 
  },
  sig_100={
    "if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\)echo\\s+shell_exec\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\);if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\)echo\\s+eval\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\);" 
  },
  sig_101={
    "((\\$\\w+='.';)+\\$\\w+=(\\$\\w+\\.?)+;)+eval\\(\\$\\w+\\(\\$\\w+\\(\\$\\w+\\(\\$\\w+\\('[^']+'\\)\\)\\)\\)\\);" 
  },
  sig_102={
    "<\\?php\\s*(\\$.\\s*=\\s*[\\wchr\\(\\)\\d+\\./\\*;\\s\"']+)+\\$.\\([chr\\(\\)\\d\\.,\\s*\\$abc]+;\\s*$" 
  },
  sig_103={
    "if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\){if\\(isset\\(\\$_FILES\\['(\\w+)'\\]\\)\\){(\\$\\w+)=getcwd\\(\\)\\.'/';(\\$\\w+)=\\$_FILES\\['\\1'\\];@move_uploaded_file\\(\\3\\['tmp_name'\\], \\2\\.\\3\\['name'\\]\\);.*?<form method=\"POST\" enctype=\"multipart/form-data\"><input type=\"file\" name=\"\\1\"/><input type=\"Submit\"/></form><\\?php }}" 
  },
  sig_104={
    "if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\){\\s*(\\$\\w+) = \\$_SERVER\\[DOCUMENT_ROOT\\];\\s*(\\$\\w+) = <<<'EOD'\\s*if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\){if\\(isset\\(\\$_FILES\\['(\\w+)'\\]\\)\\){(\\$\\w+)=getcwd\\(\\)\\.'/';(\\$\\w+)=\\$_FILES\\['\\3'\\];@move_uploaded_file\\(\\5\\['tmp_name'\\], \\4\\.\\5\\['name'\\]\\);.*?<form method=\"POST\" enctype=\"multipart/form-data\"><input type=\"file\" name=\"\\3\"/><input type=\"Submit\"/></form><\\?php }}\\s*EOD;\\s*(\\$\\w+) = <<<'EOD'\\s*<\\?php if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\){if\\(isset\\(\\$_FILES\\['(\\w+)'\\]\\)\\){(\\$\\w+)=getcwd\\(\\)\\.'/';(\\$\\w+)=\\$_FILES\\['\\3'\\];@move_uploaded_file\\(\\5\\['tmp_name'\\], \\4\\.\\5\\['name'\\]\\);.*?<form method=\"POST\" enctype=\"multipart/form-data\"><input type=\"file\" name=\"\\3\"/><input type=\"Submit\"/></form><\\?php }} \\?>\\s*EOD;\\s*(\\$\\w+) = array\\((\\1\\.\"/[a-z0-9\\-_\\s\\/]+\\.php\"?,?\\s*)+\\);\\s*foreach\\(\\10 as (\\$\\w+)\\){\\s*(\\$\\w+) = file_get_contents\\(\\12\\);\\s*if \\(stripos\\(\\13, '\\w+'\\) == false\\){\\s*if\\(preg_match\\('/\\\\\\?>\\(\\\\s\\+\\)\\?\\$/i', \\13\\) == false\\){\\s*\\13 \\.= \\2;\\s*file_put_contents\\(\\12, \\13\\);\\s*}else{\\s*(\\$\\w+) = '/\\\\\\?>\\(\\\\s\\+\\)\\?\\$/i';\\s*(\\$\\w+) = '\\?>';\\s*\\13\\s*= preg_replace\\(\\14, \\15, \\13\\);\\s*\\13 \\.= \\6;\\s*file_put_contents\\(\\12, \\13\\);\\s*}\\s*}\\s*}\\s*}" 
  },
  sig_105={
    "<\\?php\\s*if\\(strpos\\(strtolower\\(\\$_SERVER\\['REQUEST_URI'\\]\\),'essay'\\)\\){include\\(getcwd\\(\\)\\.'/config-info\\.php'\\);\\s*exit;}\\s*\\?>" 
  },
  sig_106={
    "<\\?php\\s*error_reporting\\(0\\);\\s*(\\$\\w+)=array\\(\\);\\s*if \\(strlen\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['(\\w+)'\\]\\)>0\\){\\1=explode\\(' :: ',urldecode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\2'\\]\\)\\);}\\s*if \\(strlen\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['(\\w+)'\\]\\)>0\\){array_push\\(\\1,\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\3'\\]\\);}\\s*(\\$\\w+)=\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\];\\s*(\\$\\w+) = urldecode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\);\\s*foreach \\(\\1 as (\\$\\w+)\\).*?\\$\\w+=fwrite\\(\\$\\w+,\\5\\);.*?\\?>" 
  },
  sig_107={
    "if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['(\\w+)'\\]\\)\\){ \\$(\\w+) = base64_decode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\1'\\]\\); @eval\\(\\$\\2\\); }" 
  },
  sig_108={
    "<\\?php\\s*/\\*\\s*\\w+\\s*\\*/\\s*\\?><\\?php\\s*(\\$\\w+)\\s*=\\s*\"[^\"]+\";\\s*(\\$\\w+)\\s*=\\s*base64_decode\\(\\1\\);\\s*(\\$\\w+)\\s*=\\s*'';\\s*for\\s*\\(\\$\\w+\\s*=\\s*0;\\s*\\$\\w+\\s*<\\s*strlen\\(\\2\\);\\s*\\$\\w+\\+\\+\\)\\s*{\\s*\\3\\s*\\.=\\s*chr\\(ord\\(\\2\\[\\$\\w+\\]\\)\\s*\\^\\s*ord\\('x'\\)\\);\\s*}\\s*eval\\(\\3\\)\\s*\\?>\\s*<\\?php\\s*/\\*\\s*\\w+\\s*\\*/\\s*\\?>" 
  },
  sig_109={
    "(\\$\\w+)\\s*=\\s*['|\"][assert]+(\\|\\|\\|\\w+\\|\\|\\|)[assert]+['|\"];\\s*(\\$\\w+)\\s*=\\s*['|\"][base64_decode]+(\\2)[base64_decode]+['|\"];\\s*(\\$\\w+)\\s*=\\s*str_replace\\(['|\"]\\2['|\"],['|\"]+,\\1\\);\\s*(\\$\\w+)\\s*=\\s*str_replace\\(['|\"]\\2['|\"],['|\"]+,\\3\\);\\s*(\\$\\w+)\\s*=\\s*['|\"][\\w=]+\\2[\\w+=]+';\\s*\\$\\w+\\s*=\\s*str_replace\\(['|\"]\\2['|\"],['|\"]+,\\7\\);\\s*\\1\\(\\3\\(\\7\\)\\);" 
  },
  sig_11={
    "if\\s*\\(\\s*isset\\s*\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w*'\\]\\s*\\)\\s*\\)\\s*\\{\\s*\\(\\s*\\$www\\s*=\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w*'\\]\\s*\\)\\s*&&\\s*@preg_replace\\(\\s*'.ad.e'\\s*,\\s*'@'\\s*.\\s*str_rot13\\(\\s*'riny'\\s*\\)\\s*\\.\\s*'\\(\\$www\\)'\\s*,\\s*'add'\\s*\\);\\s*exit;\\s*}" 
  },
  sig_110={
    "(\\$\\w+)\\s*=\\s*scandir\\(\\$_SERVER\\['DOCUMENT_ROOT'\\]\\);\\s*for\\s*\\((\\$\\w+)=0;\\2<count\\(\\1\\);\\2\\+\\+\\)\\s*{\\s*if\\(stristr\\(\\1\\[\\2\\],\\s*'php'\\)\\)\\s*{\\s*(\\$\\w+)\\s*=\\s*filemtime\\(\\$_SERVER\\['DOCUMENT_ROOT'\\].\"/\".\\1\\[\\2\\]\\);\\s*break;\\s*}\\s*}\\s*touch\\(dirname\\(__FILE__\\),\\s*\\3\\);touch\\(\\$_SERVER\\['SCRIPT_FILENAME'\\],\\s*\\3\\);\\s*@\\$_REQUEST\\['\\w+'\\]\\(str_rot13\\('riny\\(\\$_ERDHRFG\\[\"\\w+\"\\]\\);'\\)\\);\\s*if\\(\\s*!\\s*defined\\(\\s*'DATALIFEENGINE'\\s*\\)\\s*\\)\\s*{\\s*die\\(\\s*\"Hacking\\s*attempt!\"\\s*\\);\\s*}\\s*if\\(\\s*isset\\(\\s*\\$view_template\\s*\\)\\s*and\\s*\\$view_template\\s*==\\s*\"rss\"\\s*\\)\\s*{\\s*}\\s*elseif\\(\\s*\\$category_id\\s*and\\s*\\$cat_info\\[\\$category_id\\]\\['shot_tpl'\\]\\s*!=\\s*''\\s*\\)\\s*\\$tpl->load_template\\(\\s*\\$cat_info\\[\\$category_id\\]\\['shot_tpl'\\]\\s*.\\s*'.tpl'\\s*\\);\\s*else\\s*\\$tpl->load_template\\(\\s*'\\w+.tpl'\\s*\\);\\s*" 
  },
  sig_111={
    "(\\$\\w+)\\s*=\\s*scandir\\(\\$_SERVER\\['DOCUMENT_ROOT'\\]\\);\\s*for\\s*\\((\\$\\w+)=0;\\2<count\\(\\1\\);\\2\\+\\+\\)\\s*{\\s*if\\(stristr\\(\\1\\[\\2\\],\\s*'php'\\)\\)\\s*{\\s*(\\$\\w+)\\s*=\\s*filemtime\\(\\$_SERVER\\['DOCUMENT_ROOT'\\]\\.\"/\"\\.\\1\\[\\2\\]\\);\\s*break;\\s*}\\s*}\\s*touch\\(dirname\\(__FILE__\\),\\s*\\3\\);\\s*touch\\(\\$_SERVER\\['SCRIPT_FILENAME'\\],\\s*\\3\\);\\s*chmod\\(\\$_SERVER\\['SCRIPT_FILENAME'\\],\\s*0444\\);" 
  },
  sig_112={
    "(\\$\\w+)=['|\"](\\w+)\\.zip['|\"];\\s*(\\$\\w+)\\s*=\\s*file_get_contents\\(['|\"][^'|^\"]+['|\"]\\);\\s*file_put_contents\\(\\1,\\s*\\3\\);.*?pclzip\\.lib\\.php.*(\\$\\w+)\\s*=\\s*new\\s*PclZip\\(\\1\\);\\s*if\\s*\\(\\4->extract\\(\\)\\s*==\\s*0\\)\\s*{\\s*die\\(\"Error\\s*:\\s*\"\\.\\4->errorInfo\\(true\\)\\);\\s*}" 
  },
  sig_113={
    "<html>\\s*<head>\\s*<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-utf-8\">\\s*<title>\\w+</title>\\s*</head>\\s*<body>\\s*<\\?php\\s*print '<h1>.*?</h1>';\\s*echo \".*?\";.*?echo\\s*\\$_SERVER\\['REMOTE_ADDR'\\];\\s*echo\\s*\"<form method=\\\\\"post\\\\\"\\s*enctype=\\\\\"multipart/form-data\\\\\">.*?if\\s*\\(\\s*@is_uploaded_file\\(\\s*\\$_FILES\\[\"\\w+\"\\]\\[\"\\w+\"\\]\\s*\\)\\)\\s*{\\s*.*?move_uploaded_file\\(\\$_FILES\\[\"\\w+\"\\]\\[\"tmp_name\"],.*?\\$_FILES\\[\"\\w+\"\\]\\[\"name\"\\]\\);.*?echo\\s*\"<a\\s*href=\\\\\"\\$\\w+\\\\\">\\$\\w+</a>\";\\s*.*?\\$\\w+\\s*=\\s*\\$_SERVER\\['SCRIPT_FILENAME'\\];\\s*touch\\(\\s*\\$\\w+\\s*\\);\\s*\\?>\\s*</body>\\s*</html>" 
  },
  sig_114={
    "if\\s*\\(\\$mode=='upload'\\)\\s*{\\s*if\\(is_uploaded_file\\(\\$_FILES\\[\"filename\"\\]\\[\"tmp_name\"\\]\\)\\)\\s*{\\s*move_uploaded_file\\(\\$_FILES\\[\"filename\"\\]\\[\"tmp_name\"\\],\\s*\\$_FILES\\[\"filename\"\\]\\[\"name\"\\]\\);\\s*echo \\$_FILES\\[\"filename\"\\]\\[\"name\"\\];\\s*}\\s*}" 
  },
  sig_115={
    "error_reporting\\(0\\);\\s*set_time_limit\\(0\\);\\s*if\\s*\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]=='1'\\){echo\\s*'200';\\s*exit;}\\s*if\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]=='\\w+'\\)eval\\(base64_decode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\);\\s*if\\(md5\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)=='\\w+'\\)eval\\(base64_decode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\);" 
  },
  sig_116={ "(\\$\\w+)=\"base64_decode\";assert\\(\\1\\('[\\w=]+'\\)\\);" },
  sig_117={
    "(\\$\\w+\\s*=\\s*(chr\\(\\s*(\\d+|ord\\([\"']\\w+[\"']\\))\\s*[\\^\\+\\-/\\*]\\s*(\\d+|ord\\([\"']\\w+[\"']\\))\\s*\\)\\.?)+;\\s*){2,}.+?exit\\(\\${\\w+\\(" 
  },
  sig_118={
    "<\\?php\\s*\\$\\w+\\s*=\\s*Array\\(('\\w+'=>'\\w+',?\\s*){20,}\\s*\\);\\s*function\\s*\\w+\\(\\$\\w+,\\s*\\$\\w+\\){\\$\\w+.+?base64_decode.+eval\\(\\w+\\(\\$\\w+,\\s*\\$\\w+\\)\\);\\?>" 
  },
  sig_119={ "<\\?php\\s*eval\\(\"(\\\\x?[\\w]+){6,}.+(\\\\x?[\\w]{3,})\"\\);\\s*\\?>$" },
  sig_12={
    "\\s*<\\?php\\s*\\(\\$\\w+\\s*=\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\) && @preg_replace\\(\\s*'/ad/e'\\s*,\\s*'@'\\s*\\.\\s*str_rot13\\(\\s*'riny'\\s*\\)\\s*\\.\\s*'\\(\\$\\w+\\)'\\s*,\\s*'add'\\);\\s*\\?>\\s*" 
  },
  sig_120={ "//###=CACHE START=###.*?//###=CACHE END=###" },
  sig_121={
    "<\\?php\\s*if\\(!\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\){\\s*header\\('HTTP/1\\.1 404 Not Found'\\);\\s*exit\\(\\);.+?}\\s*\\?>" 
  },
  sig_122={
    "<\\?php\\s*\\$\\w+\\s*=\\s*[\"base64\\s\\._decode\"]+;\\s*assert\\(\\$\\w+\\('[a-zA-Z0-9\\d\\./]{500,}'\\)\\);\\s*\\?>" 
  },
  sig_123={
    "<\\?.+?echo\"<center>.+?</center>\\s*\";\\s*echo\\s*'uname:'.php_uname\\(\\)\\.\"\\\\n\";\\s*echo\\s*getcwd\\(\\)\\s*\\.\\s*\"\\\\n\";\\s*\\?>" 
  },
  sig_124={
    "<\\?php\\s*/\\*.+?\\*/\\s*@eval\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\);\\s*\\?>" 
  },
  sig_125={
    "if\\s*\\(\\s*\\!\\s*defined\\s*\\(\\s*\\'IPB_FIREWALL'.*_findIpbRoot.*conf_global\\.php.*IPB_Firewall\\:\\:run\\(\\);\\s*\\}" 
  },
  sig_126={
    "\\$(\\w+)=\"create_\";global\\s+\\$(\\w+);\\s*\\$\\2=array\\('\\$\\2[\\d+]=array_pop(\\$\\2);\\$(\\w+)=\\3\\(\\d+\\);\\$\\2[\\d+]=\\$\\3\\(\\$\\2[\\d+]\\);.?\\$\\2[\\d+]=gzuncompress\\(.+?\\);if\\(function_exists\\(\\$\\1\\.='function'\\)&&!function_exists\\('\\3'\\)\\){\\s*function\\s+\\3\\(\\$\\w+,\\$\\w+=\\d+\\){global\\s+\\$\\2;.?;\\$\\1.?;unset\\(\\$\\2\\);" 
  },
  sig_127={
    "\\s+if\\(@?isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\w+\\]\\)\\){\\s*echo\\s*'<form\\s+action=\"\\s*\"\\s+method=\".+?assert\\(stripslashes\\(\\$_REQUEST\\[\\w+\\]\\)\\);else exit;" 
  },
  sig_128={
    "\\s+if\\(isset\\(\\$_REQUEST\\['?\\w+'?\\]\\)\\)\\s*assert\\(stripslashes\\(\\$_REQUEST\\[\\w+]\\)\\);" 
  },
  sig_129={ "arr2html\\(\\$_REQUEST\\['\\w+'\\]\\);" },
  sig_13={ "<\\?php eval\\(\"[^\"]+\"\\.\\$_REQUEST\\['.'\\]\\.\"[^\"]+\"\\);\\?>" },
  sig_130={
    "\\(\\$\\w+\\s*=\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\s*'\\w+'\\s*\\]\\)\\s*&&\\s*@preg_replace\\('/\\w+/e'\\s*,\\s*'@'\\s*\\.\\s*base64_decode\\(\\s*\"[\\w=+/]+\"\\s*\\)\\s*\\.'\\s*\\(\\s*\\$\\w+\\s*\\)'\\s*,\\s*'\\w+'\\s*\\);" 
  },
  sig_131={ "eval\\(strrev\\(file_get_contents\\('[\\-\\w.]+'\\)\\)\\);" },
  sig_132={
    "error_reporting(0);\\s*ini_set(\\s*\"display_errors\"\\s*,\\s*0)\\;\\s*include_once(sys_get_temp_dir()\\.\"/SESS_[a-f0-9]+\");" 
  },
  sig_133={
    "\\$url\\s*=\\s*\".*?\";\\s*if\\s*\\(isset\\(\\$_SERVER\\['HTTP_REFERER'\\]\\)\\s*AND\\s*!isset\\(\\$_COOKIE\\[\"\\w+\"\\]\\)\\)\\s*{\\s*setcookie\\(\"\\w+\",\\s*\"1\",\\s*time\\(\\)\\+\\d+\\);\\s*\\$urls\\s*=\\s*array\\(\"google\\.\",\\s*\"yandex\\.\",\\s*\"yahoo\\.\",\\s*\"aol\\.\",\\s*\"msn\\.\",\\s*\"rambler\\.\",\\s*\"mail\\.\",\\s*\"ya\\.\",\\s*\"bing\\.\",\\);\\s*for\\s*\\(\\$i=0;\\s*\\$i\\s*<\\s*count\\(\\$urls\\);\\s*\\$i\\+\\+\\)\\s*if\\s*\\(strpos\\(\\$_SERVER\\['HTTP_REFERER'\\],\\s*\\$urls\\[\\$i\\]\\)!==false\\)\\s*exit\\('<script>document\\.location\\.href\\s*=\\s*\"'\\.\\$url\\.'\";</script>'\\);\\s*}" 
  },
  sig_134={
    "error_reporting\\(0\\);\\s*ini_set\\(\\s*\"display_errors\"\\s*,\\s*0\\)\\;\\s*include_once\\(\\s*sys_get_temp_dir\\(\\s*\\)\\s*\\.\\s*\"/SESS_[a-f0-9]{32}\"\\);" 
  },
  sig_135={
    "\\$GLOBALS\\['_(\\d+)_'\\]=Array\\(base64_decode\\(.*?if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[_(\\d+)\\(\\d+\\)\\]\\)\\){\\$_\\d+=\\$GLOBALS\\['_\\1_'\\]\\[\\d+\\]\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[_\\2\\(\\d+\\)\\]\\);\\$GLOBALS\\['_\\1_'\\]\\[\\d+\\]\\(\\$_\\d+,\\$_\\d+\\);}echo \\$_\\d+;exit;}" 
  },
  sig_136={
    "if\\s*\\(\\$_SERVER\\['QUERY_STRING'\\]\\s*==\\s*'index'\\)\\s*{\\s*echo\\s*'<form\\s*action=\"\"\\s*method=post\\s*enctype=multipart/form-data><input\\s*type=file\\s*name=uploadfile><input\\s*type=submit\\s*value=Upload></form>';\\s*\\$uploaddir\\s*=\\s*'';\\s*\\$uploadfile\\s*=\\s*\\$uploaddir\\.basename\\(\\$_FILES\\['uploadfile'\\]\\['name'\\]\\);\\s*if\\s*\\(copy\\(\\$_FILES\\['uploadfile'\\]\\['tmp_name'\\],\\s*\\$uploadfile\\)\\)\\s*{\\s*echo\\s*\"<h3>OK</h3>\";\\s*exit;\\s*}else{\\s*echo\\s*\"<h3>NO</h3>\";\\s*exit;\\s*}\\s*exit;\\s*}" 
  },
  sig_137={
    "if\\(isset\\(\\$_REQUEST\\[\"(\\w+)\"\\]\\)\\)\\s*\\$_REQUEST\\[\"\\1\"\\]\\(\\$_REQUEST\\[\"\\w+\"\\]\\);" 
  },
  sig_138={
    "<\\?php\\s*\\$\\w+\\s*=\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\];\\s*if\\s*\\(\\$\\w+\\s*!=\\s*\"\"\\)\\s*{\\s*\\$\\w+=base64_decode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\);\\s*@eval\\(\"\\\\\\$\\w+\\s*=\\s*\\$\\w+;\"\\);\\s*}\\s*\\?>" 
  },
  sig_139={
    "@preg_replace\\(\"/\\[\\w+\\]/e\"\\s*,\\s*\\$_(COOKIE|POST|GET|REQUEST|SERVER)\\s*\\[\\s*'\\w+'\\s*\\]\\s*,\\s*\"\\w+\"\\s*\\);" 
  },
  sig_14={
    "\\$.\\s*=\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST);\\s*@\\(\\$.\\[\\d\\]\\s*!=\\s*\\$.\\[\\d\\]\\)\\s*\\?\\s*@\\$.\\[\\d\\]\\(\\$.\\[\\d\\]\\)\\s*:\\s*\\(int\\)\\$.;" 
  },
  sig_140={
    "<\\?php\\s*\\$GLOBALS\\['\\w+'\\]\\s*=\\s*\"\\w+\";\\s*\\$\\w+=\"[create_function\\.\\\"\\s]+\";\\$\\w+=\\$\\w+\\('[\\$evalx',\\.(\\\"\\?>gzinfltbs64dcode_);]+\\$\\w+\\(\".+\"\\);\\?>" 
  },
  sig_141={
    "<\\?php\\s*\\$\\w+=\\\".+?\\\";\\$GLOBALS\\['\\w+'\\]\\s*=\\s*\\${(\\$\\w+\\[\\d+\\]\\.?)+};\\$GLOBALS\\['\\w+'\\]\\s*=\\s*(\\$\\w+\\[\\d+\\]\\.?)+;if\\s*\\(!empty\\(\\$GLOBALS\\['\\w+'\\]\\['\\w+'\\]\\)\\)\\s*{\\s*eval\\(\\$GLOBALS\\[.+?echo\\s+(\\$\\w+\\[\\d+\\]\\.?){10,};" 
  },
  sig_142={
    "\\$\\w+\\s*=\\s*\"[preg_replace\\s\\.\\\"]+\";\\s*\\$\\w+\\(\"/\\[discuz\\]/e\",\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\],\"[^\\\"]+\"\\);" 
  },
  sig_143={
    "RewriteEngine\\s+on\\s*RewriteCond\\s+%{HTTP_USER_AGENT}\\s+android\\|avantgo\\|bada.+?zeto\\|zte\\\\-\\)\\s*\\[NC\\]\\s+RewriteRule\\s*\\^\\$\\s+http://.+?\\[R,L\\]" 
  },
  sig_144={
    "<IfModule\\s+mod_rewrite\\.c>\\s*RewriteCond\\s+%{HTTP_USER_AGENT}\\s+\\(google\\|yahoo\\|msn\\|aol\\|bing\\) \\[OR\\]\\s*RewriteCond\\s+%{HTTP_REFERER}\\s+\\(google\\|yahoo\\|msn\\|aol\\|bing\\)\\s*RewriteRule \\^\\.\\*\\$\\s+index\\.php\\s+\\[L\\]\\s*<\\/IfModule>" 
  },
  sig_145={
    "if\\(isset\\(\\$_(GET|POST|COOKIE|REQUEST)\\[\"\\w+\"\\]\\)\\){@\\$_(GET|POST|COOKIE|REQUEST)\\[\"\\w+\"\\]\\(stripslashes\\(\\$_(GET|POST|COOKIE|REQUEST)\\[\"\\w+\"\\]\\)\\);};" 
  },
  sig_146={
    "if\\s*\\(\\s*\\$_REQUEST\\[\"\\w+\"\\]\\s*\\)\\s*{\\s*@assert\\(\\s*base64_decode\\(\\s*\\$_REQUEST\\[\\s*\"\\w+\"\\s*\\]\\)\\);\\s*//[\\w\\s]+\\s*echo\\s*\"[\\w\\s]+\";\\s*exit\\(\\s*\\);\\s*}" 
  },
  sig_147={
    "/\\*.+?\\*/\\s*(\\$\\w+\\s*=\\s*'[^']+'\\s*\\^\\s*'[^']+'\\s*;\\s*){3,10}\\$\\w+\\s*=\\s*\\$\\w+\\('',\\$\\w+\\(\\$\\w+\\('[^']+'\\^'[^']+'\\)\\)\\);\\s*\\$\\w+\\(\\s*\\);" 
  },
  sig_148={
    "(//\\w+\\s*)?(\\$\\w+\\s*=\\s*\"[^\"]+\"\\s*\\^\\s*\"[^\"]+\";\\s*)+\\$\\w+\\(\\$\\w+\\s*,\\s*\"[^\"]+\"\\^\"[^\"]+\"\\s*,\\s*\"\\w+\"\\);(//\\w+\\s*)?" 
  },
  sig_149={
    "<\\?php\\s+@?eval\\(\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\"\\w+\"\\]\\);\\?>" 
  },
  sig_15={
    "<\\?php\\s*preg_replace\\(\"/\\w+/e\"\\s*,\\s*\"e[\"'.va]+l\\('\"\\.\\$_REQUEST\\['\\w+'\\]\\.\"'\\)\"\\s*,\\s*\"[a-zA-Z\\s0-9_]+\"\\);\\s*\\?>" 
  },
  sig_150={
    "<\\?php\\s*if\\(\\s*isset\\(\\s*\\$_(REQUEST|GET|POST|COOKIE|SERVER)\\[\"\\w+\"\\]\\s*\\)\\s*\\)\\s*{\\s*echo\\s*\"[^\"]+\";\\s*}\\s*\\$f\\s*=\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"\\];\\s*\\$id=\\$f;\\s*\\$current\\s*=\\s*file_get_contents\\(\"[^\"]+\"\\);\\s*file_put_contents\\(\\$\\w+\\s*,\\s*\\$\\w+\\);" 
  },
  sig_151={
    "<\\?php\\s*\\$password=.+?if\\s*\\(!empty\\s*\\(\\$_FILES\\['\\w+'\\]\\)\\)\\s*{\\s*move_uploaded_file.+?<input type=\"submit\" name=\"login\" value=\"go!\" /\\s*></form>\\s*<\\?php\\s*}\\s*\\?>" 
  },
  sig_152={
    "<\\?php\\s*\\$\\w+\\s*=\\s*\"[assertevl\"\\.]+\"\\s*;\\s*\\$\\w+\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\"\\w+\"\\]\\); <?php" 
  },
  sig_153={
    "error_reporting\\(E_ALL\\);\\s*\\$\\w+\\s*=\\s*'';\\s*\\$\\w+\\s*=\\s*'RewriteEngine On.+if\\s*\\(file_exists\\('index\\.php'\\)\\)\\s*{\\s*file_put_contents\\('index\\.php'\\s*,\\s*str_replace\\(array\\(\\$\\w+,\\$\\w+\\),\\s*''\\s*,\\s*file_get_contents\\('index\\.php'\\)\\)\\);\\s*}" 
  },
  sig_154={
    "<\\?php\\s*add_action\\(\\s*'wp_head'\\s*,\\s*'\\w+'\\s*\\);\\s*function\\s+\\w+\\(\\)\\s*{\\s*if\\s*\\(\\s*md5\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\s*\\)\\s*==\\s*'\\w+'\\s*\\)\\s*{\\s*require\\(\\s*'wp-includes/registration\\.php'\\s*\\);\\s*if\\s*\\(\\s*!username_exists\\(\\s*'\\w+'\\s*\\)\\s*\\)\\s*{\\s*\\$user_id\\s*=\\s*wp_create_user\\(\\s*'\\w+',\\s*'\\w+'\\s*\\);\\s*\\$\\w+\\s*=\\s*new\\s+WP_User\\(\\s*\\$user_id\\s*\\);\\s*\\$user->set_role\\(\\s*'\\w+'\\s*\\);\\s*}\\s*}\\s*}\\s*\\?>" 
  },
  sig_155={
    "<\\?php\\s*echo\\s*'<b><br><br>'\\.php_uname\\(\\s*\\).+echo\\s*'<b>Upload[^<]+</b><br><br>';\\s*}\\s*}\\s*\\?>" 
  },
  sig_156={
    "<\\?php\\s*@?eval\\(\\$_(POST|GET|COOKIE|REQUEST|SERVER)\\['\\w+'\\]\\); <?php" 
  },
  sig_157={
    "<\\?php\\s+function\\s+\\w+\\(\\$\\w+\\){\\s*\\$\\w+\\s*=\\s*'[base64decode_\\.']+';\\s*\\$\\w+\\s*=\\s*gzinflate\\(\\$code\\(\\$\\w+\\)\\);\\s*for\\(\\$i=0;\\$i<strlen\\(\\$\\w+\\);\\$i\\+\\+\\)\\s*{\\s*\\$\\w+\\[\\$i\\]\\s*=\\s*chr\\(ord\\(\\$\\w+\\[\\$i\\]\\)-1\\);\\s*}\\s*return\\s+\\$\\w+;\\s*}eval\\(\\w+\\(\"[^\"]+\"\\)\\);\\?>" 
  },
  sig_158={
    "<\\?php\\s*(\\$\\w+\\s*=\\s*\"[a-zA-Z_=/0-9\\?]+\";\\s*)+\\s*(\\$\\w+\\s*=\\s*(str_replace|\\$\\w+)\\s*\\(\\s*\"\\w+\"\\s*,\\s*\"\"\\s*,\\s*\"\\w+\"\\);\\s*)+\\$\\w+\\s*=\\s*\\$\\w+\\s*\\(\\s*''\\s*,\\s*\\$\\w+\\s*\\(\\s*\\$\\w+\\s*\\(\\s*\\$\\w+\\(\"[^\"]+\"\\s*,\\s*\"\"\\s*,\\s*.+?\\$\\w+\\(\\); <?php" 
  },
  sig_159={
    "<\\?php\\s*(\\$\\w+\\s*=\\s*\"[a-zA-Z0-9/=_]+\";\\s*\\$\\w+\\s*=\\s*(str_replace|\\$\\w+)\\(\\s*\"\\w+\"\\s*,\\s*\"\"\\s*,\\s*\"\\w+\"\\s*\\);\\s*)+\\$\\w+\\s*=\\s*\"\\w+\";\\s*\\$\\w+\\s*=\\s*\\$\\w+\\s*\\(\\s*''\\s*,\\s*\\$*.+?\\$\\w+\\(\\); <?php" 
  },
  sig_16={ "preg_replace\\(\"\\\\x2f(\\\\x..)+\"\\s*,\\s*\"(\\\\x..)+\",\"\"\\);" },
  sig_160={
    "<\\?php\\s*\\$\\w+\\s*=\\s*base64_decode\\('.+?;@?\\$c\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[['\"]?\\w+['\"]?\\]\\);\\s*\\?>" 
  },
  sig_161={
    "<\\?php\\s*\\$\\w+\\s*=\\s*\"[preg_replace\\.\\\"]+\";\\s*@?\\$\\w+\\(\\s*\"/\\[\\w+\\]/e\"\\s*,\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"\\],\"\\w+\"\\);\\s*\\?> <?php" 
  },
  sig_162={
    "<\\?(php)?\\s*\\$\\w+\\s*=\\s*@?\\$_SERVER\\[\"HTTP_USER_AGENT\"\\].+?@eval\\(\\s*\\$\\w+\\[\\w+\\]\\s*\\);\\s*echo\\s*\"[^\"]+\";\\?>" 
  },
  sig_163={
    "\\s+\\$\\w+\\s*=\\s*\"\\w+\"\\s*;\\s*\\$\\w+\\s*=\\s*strtoupper\\s*\\((\\s*\\$\\w+\\[\\s*\\d+\\s*]\\s*\\.?)+\\)\\s*;\\s*if\\s*\\(\\s*isset.+?{\\s*eval\\s*\\(\\s*\\${\\s*\\$\\w+\\s*}\\s*\\[\\s*'\\w+'\\s*\\]\\s*\\)\\s*;\\s*}" 
  },
  sig_164={
    "\\s+\\$\\w+\\s*=\\s*\"\\w+\"\\s*;\\s*\\$\\w+\\s*=\\s*strtolower\\s*\\((\\s*\\$\\w+\\[\\s*\\d+\\s*]\\s*\\.?)+\\)\\s*;.+?eval\\s*\\(\\s*\\$\\w+\\s*\\w+\\s*\\(\\s*\\$\\s*{\\s*\\$\\w+\\s*}\\s*\\[\\s*'\\w+'\\s*\\]\\s*\\)\\s*\\)\\s*;\\s*}" 
  },
  sig_165={
    "\\s+\\$\\w+\\s*=\\s*\"\\w+\"\\s*;\\s*\\$\\w+\\s*=(\\s*\\$\\w+\\[\\d+\\]\\s*\\.?){3,};\\s*\\$\\w+\\s*=\\s*\\$\\w+\\s*\\((\\s*\\$\\w+\\[\\d+\\]\\s*\\.?){3,}\\)\\s*;.+?eval\\s*\\(\\s*\\${\\s*\\$\\w+\\s*}\\s*\\[\\s*'\\w+'\\s*\\]\\s*\\)\\s*;\\s*}" 
  },
  sig_166={
    "\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\[\\s*'\\w+'\\s*\\]\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\[\\s*'\\w+'\\s*\\]\\(''\\s*,\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\[\\s*'\\w+'\\s*\\]\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\['\\w+'\\]\\s*\\)\\s*\\)\\s*\\);" 
  },
  sig_167={
    "if\\(\\s*!empty\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\)\\){\\s*extract\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\);\\$(\\s*\\w+\\s*)\\s*=\\s*\\$\\w+\\(\\s*''\\s*,\\s*\\$\\w+\\(\\$\\w+\\(\\s*\"\\w+\"\\s*,\\s*\"\"\\s*,\\s*\\$\\w+\\)\\s*\\)\\s*\\);\\s*\\$\\1\\(\\s*\\);\\s*}" 
  },
  sig_168={
    "<\\?php\\s*\\$str\\s*=\\s*'[^']+';\\s*echo base64_decode\\(\\$str\\);\\s*\\?>" 
  },
  sig_169={
    "\\$\\w+=\"create.+?return\\([^}]+};for\\(\\$\\w+=-\\d+;\\+\\+\\$\\w+<\\d+;\\$\\w+\\('','}'\\.\\$\\w+\\[\\s*\\$\\w+\\s*\\]\\s*\\.\\s*'{'\\)\\);};unset\\(\\$\\w+\\);" 
  },
  sig_17={
    "<\\?php\\s*eval\\(gzinflate\\(str_rot13\\(base64_decode\\('[a-zA-Z0-9+/\\s]+AQ=='\\)\\)\\)\\);\\s*\\?>" 
  },
  sig_170={
    "<\\?(php)?\\s*@\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w*'\\]\\s*\\(str_rot13\\s*\\('\\w*\\s*\\(\\s*\\$_\\w*\\[\"\\w*\"\\]\\s*\\);'\\s*\\)\\s*\\);\\s*\\?>" 
  },
  sig_171={
    "<IfModule mod_rewrite\\.c>.+?google\\|yahoo\\|msn\\|aol\\|bing.+?index\\.php\\s+\\[L\\]\\s*</IfModule>" 
  },
  sig_172={
    "\\$\\w+\\s*=\\s*getenv\\(\"SCRIPT_NAME\"\\);\\s*\\$\\w+\\s*=\\s*getenv\\(\"SCRIPT_FILENAME\"\\);\\s*\\$\\w+\\s*=\\s*substr\\(\\$\\w+,\\s*0,\\s*strpos\\(\\$\\w+,\\s*\\$\\w+\\)\\);\\s*\\$\\w+\\s*=\\s*\\$\\w+\\s*\\.\\s*'/xm1rpc\\.php';\\s*if\\s*\\(!file_exists\\(\\$\\w+\\)\\s*\\|\\|\\s*file_exists\\(\\$\\w+\\)\\s*&&\\s*\\(filesize\\(\\$\\w+\\)\\s*<\\s*3000\\)\\s*\\|\\|\\s*file_exists\\(\\$\\w+\\)\\s*&&\\s*\\(time\\(\\)\\s*-\\s*filemtime\\(\\$\\w+\\)\\s*>\\s*60\\s*\\*\\s*60\\s*\\*\\s*1\\)\\)\\s*{\\s*file_put_content.+?\\$enc1\\s*=\\s*\\$enc2\\s*=\\s*\\$enc3\\s*=\\s*\\$enc4\\s*=\\s*\"\";\\s*}\\s*while\\s*\\(\\$i\\s*<\\s*strlen\\(\\$\\w+\\)\\);\\s*return\\s*\\$\\w+;\\s*}" 
  },
  sig_173={
    "\\$user_agent_to_filter\\s*=\\s*array\\(\\s*'#Ask.+?\\$ch\\s*=\\s*curl_init\\(\\s*\\);\\s*curl_setopt\\(\\$ch,\\s*CURLOPT_URL,\\s*\"https?://.+?\\?useragent=\\$_SERVER\\[\\s*HTTP_USER_AGENT\\s*\\]&domain=\\$_SERVER\\[\\s*HTTP_HOST\\s*\\]\"\\);\\s*\\$result\\s*=\\s*curl_exec\\(\\s*\\$ch\\s*\\);\\s*curl_close\\s*\\(\\s*\\$ch\\s*\\);\\s*echo\\s*\\$result;\\s*}" 
  },
  sig_174={
    "\\$query\\s*=\\s*isset\\(\\$_SERVER\\['QUERY_STRING'\\]\\)\\?\\s*\\$_SERVER\\['QUERY_STRING'\\]:\\s*'';\\s*if\\s*\\(false\\s*!==\\s*strpos\\(\\$query,\\s*'[\\w-]+'\\)\\)\\s*{\\s*__\\w+_\\w+\\(\\);.+?stream_context_create\\(\\$options\\);\\s*\\$contents\\s*=\\s*@file_get_contents\\(\\$url,\\s*false,\\s*\\$context\\);\\s*}\\s*}\\s*}\\s*return\\s*\\$contents;\\s*}" 
  },
  sig_175={
    "if\\s*\\(\\s*strpos\\(\\s*strtolower\\(\\s*\\$_SERVER\\[\\s*'REQUEST_URI'\\s*\\]\\s*\\)\\s*,\\s*'\\w+'\\s*\\)\\s*\\)\\s*{\\s*include\\s*\\(\\s*getcwd\\s*\\(\\s*\\)\\s*\\.\\s*'/\\w+\\.php'\\s*\\);\\s*exit;\\s*}" 
  },
  sig_176={
    "if\\s*\\(\\s*isset\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*['\"]\\w+['\"]\\s*\\]\\s*\\)\\s*\\)\\s*{\\s*(\\s*/\\*\\w+\\*/)?@?preg_replace\\(\\s*['\"]/\\(\\.\\*\\)/e'\\s*,\\s*@?\\$_REQUEST\\[\\s*['\"]\\w+['\"]\\s*\\]\\s*,\\s*['\"]['\"]\\s*\\);\\s*(/\\*\\w+\\*/)?\\s*}" 
  },
  sig_177={
    "if\\s*\\(\\s*isset\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[[\"']\\w+[\"']\\]\\)\\)\\s*{(/\\*\\w+\\*/)?@?extract\\(\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\);\\s*(/\\*\\w+\\*/)?@?die\\(\\$\\w+\\(\\$\\w+\\)\\);\\s*(/\\*\\w+\\*/)?}" 
  },
  sig_178={
    "@?filter_var\\s*\\(\\s*\\$_(GET|POST|REQUEST|SERVER|COOKIE){\\s*(/\\*\\w+\\*/)?['\"]\\w+['\"]/\\*\\w+\\*/\\s*}\\s*,\\s*FILTER_CALLBACK\\s*,\\s*array\\(\\s*(/\\*\\w+\\*/)?['\"]\\w+['\"]\\s*=>\\s*@?\\$_(GET|POST|REQUEST|SERVER|COOKIE){\\s*(/\\*\\w+\\*/)?\\w+(/\\*\\w+\\*/)?\\s*}\\s*\\)\\s*\\);" 
  },
  sig_179={
    "@?array_filter\\(\\s*/\\*\\w+\\*/\\s*array\\(\\s*/\\*\\w+\\*/@?\\$_(GET|POST|COOKIE|REQUEST|SERVER){\\s*(/\\*\\w+\\*/)?\"\\w+\"/\\*\\w+\\*/}\\)\\s*,\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER){\\s*/\\*\\w+\\*/['\"]\\w+['\"]/\\*\\w+\\*/\\s*}\\s*/\\*\\w+\\*/\\s*\\);" 
  },
  sig_18={
    "<\\?php\\s*eval\\(gzinflate\\(base64_decode\\('[a-zA-Z0-9+/\\s]+AQ=='\\)\\)\\);\\s*\\?>" 
  },
  sig_180={
    "\\$\\w+\\s*=\\s*'\\w+';\\s*if\\(\\s*isset\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\s*'\\w+'\\s*\\]\\)\\)\\s*{\\s*if\\s*\\(\\s*md5\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\s*'\\w+'\\s*\\]\\)\\s*===\\s*\\$\\w+\\s*\\)\\s*@?eval\\(\\s*base64_decode\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\s*'\\w+'\\s*\\]\\s*\\)\\s*\\);\\s*exit;\\s*}\\s*if\\(\\s*isset\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\s*'\\w+'\\s*\\]\\s*\\)\\s*\\){\\s*phpinfo\\(\\s*\\);\\s*}" 
  },
  sig_181={
    "\\$\\w+=\\$_(GET|POST|COOKIE|REQUEST|SERVER);\\s*\\$\\w+\\s*=\\s*\\$\\w+\\[\\s*\\w+\\s*\\];\\s*if\\s*\\(\\s*\\$\\w+\\s*\\)\\s*{\\s*\\$\\w+=\\s*\\$\\w+\\s*\\(\\$\\w+\\s*\\[\\s*\\w+\\s*\\]\\s*\\);\\s*\\$\\w+\\s*=\\s*\\$\\w+\\s*\\(\\s*\\$\\w+\\s*\\[\\s*\\w+\\s*\\]\\s*\\);\\s*\\$\\w+\\s*=\\s*\\$\\w+\\s*\\(\"\"\\s*,\\s*\\$\\w+\\s*\\);\\s*\\$\\w+\\s*\\(\\s*\\);\\s*}" 
  },
  sig_182={
    "if\\s*\\(\\s*md5\\(\\s*@?\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*\\w+\\s*\\]\\)\\s*==\\s*'\\w+'\\s*\\)\\s*\\s*\\(\\s*\\$_=\\s*@\\$_REQUEST\\[\\s*\\w+\\s*\\]\\)\\s*\\.\\s*@?\\$_\\(\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*\\w+\\s*\\]\\s*\\);" 
  },
  sig_183={
    "<\\?php\\s*\\$\\w+\\s*=\\s*file_get_contents\\(\\s*\"https?[^\"]+\"\\s*\\);\\s*\\$\\w+\\s*=\\s*fopen\\(\\s*\"\\w+\\.php\"\\s*,\\s*\"w\\+\"\\s*\\);\\s*fwrite\\(\\s*\\$\\w+\\s*,\\s*\\$\\w+\\);\\s*fclose\\(\\s*\\$fo\\s*\\);\\s*(\\?>)?" 
  },
  sig_184={
    "RewriteRule\\s+\\^\\(\\[A-Za-z0-9-\\]\\+\\)\\.html\\$\\s+\\w+\\.php\\?hl=\\$1\\s+\\[L\\]" 
  },
  sig_185={
    "set_time_limit\\(0\\);\\s*ignore_user_abort\\(\\);\\s*if\\s*\\(\\s*filesize\\(\\s*\"\\.htaccess\"\\s*\\)\\s*>\\s*\\d+\\s*\\)\\s*{\\s*\\$\\w+\\s*=\\s*fopen.+?\\(\\s*sys_get_temp_dir\\(\\)\\s*\\.\\s*\"/\\$\\w+\"\\s*\\);\\s*}" 
  },
  sig_186={
    "<\\?php\\s*error_reporting\\(0\\);ini_set\\(\"display_errors\", 0\\);\\$localpath=getenv\\(\"SCRIPT_NAME\"\\);\\$absolutepath=getenv\\(\"SCRIPT_FILENAME\"\\);\\$\\w+=substr\\(\\$\\w+,0,strpos\\(\\$absolutepath,\\$localpath\\)\\);include_once\\(\\$root_path\\.\"/\\w+.zip\"\\);\\s*\\?>" 
  },
  sig_187={
    "\\$var= \\$_SERVER\\['PHP_SELF'\\];\\s*\\$form =<<<HTML\\s*<form enctype=\"multipart/form-data\" action=\"\\$var\" method=\"POST\">\\s*<input name=\"uploadFile\" type=\"file\"/><br/>\\s*<input type=\"submit\" value=\"Upload\" />\\s*</form>\\s*HTML;\\s*if \\(!empty\\(\\$_FILES\\['uploadFile'.+?print\\s*\"OK\";\\s*}\\s*else\\s*{\\s*print\\s*\\$form;\\s*}" 
  },
  sig_188={
    "error_reporting\\(0\\);ini_set\\(\"display_errors\", 0\\);\\$localpath=getenv\\(\"SCRIPT_NAME\"\\);\\$absolutepath=getenv\\(\"SCRIPT_FILENAME\"\\);\\$root_path=substr\\(\\$absolutepath,0,strpos\\(\\$absolutepath,\\$localpath\\)\\);include_once\\(\\$root_path\\.\"/SESS_\\w{32}\\.php\"\\);" 
  },
  sig_189={ "<\\?php\\s*eval\\(base64_decode\\(\"IGVycm9yX.+=\"\\)\\);" },
  sig_19={ "//#\\+=\\+#\\+.+?//#\\+=\\+#\\+" },
  sig_190={
    "\\$GLOBALS\\['\\w+'\\];global\\$\\w+;\\$\\w+=\\$GLOBALS;\\${['\"](\\\\x[a-f0-9A-F]+)+['\"]}\\[['\"]\\w+['\"]\\]=\"(\\\\x[a-f0-9A-F]+)+\";.+?\\]\\){eval\\(\\$\\w+\\[\\$\\w+\\[[\"']\\w+[\"']\\]\\[\\d+\\]\\]\\);}exit\\(\\);}" 
  },
  sig_191={
    "if\\s*\\(strpos\\(\\$_SERVER\\['REQUEST_URI'\\]\\s*,\\s*'.+?'\\)\\s*!==\\s*false\\)\\s*{\\s*error_reporting\\(0\\);\\s*ini_set\\('display_errors',\\s*0\\);\\s*set_time_limit\\(0\\);.+add_filter\\( 'wp_title',\\s*'\\w+'\\s*\\);\\s*}\\s*}" 
  },
  sig_192={
    "<\\?\\s*\\$GLOBALS\\['_\\d+_'\\]=Array\\(base64_decode\\((\\s*'[\\w=]+'\\s*\\.?)+\\)\\);\\s*\\?><\\?\\s*function\\s*(_\\d+)\\((\\$\\w+)\\)\\{(\\$\\w+)=Array\\((\\s*'[\\w=]+'\\s*\\.?,?)+\\);return\\s*base64_decode\\(\\4\\[\\3\\]\\);}\\s*\\?><\\?php\\s*\\$password=\\2\\(\\d+\\);\\$GLOBALS\\['_\\d+_'\\]\\[\\d+\\]\\(\\2\\(\\d\\),\\2\\(\\d\\),\\2\\(\\d\\)\\);\\s*\\?>" 
  },
  sig_193={
    "\\$localpath\\s*=\\s*getenv\\(\"SCRIPT_NAME\"\\);\\s*\\$absolutepath\\s*=\\s*getenv\\(\"SCRIPT_FILENAME\"\\);\\s*\\$root_path\\s*=\\s*substr\\(\\$absolutepath,\\s*0,\\s*strpos\\(\\$absolutepath, \\$localpath\\)\\);\\s*\\$xml\\s*=\\s*\\$root_path\\s*\\.\\s*'/xm1rpc\\.php'.*?\\$chr1\\s*=\\s*\\$chr2\\s*=\\s*\\$chr3 =\\s*\"\";\\s*\\$enc1\\s*=\\s*\\$enc2\\s*=\\s*\\$enc3\\s*=\\s*\\$enc4\\s*=\\s*\"\";\\s*}\\s*while\\s*\\(\\$i\\s*<\\s*\\s*strlen\\(\\$input\\)\\);\\s*return\\s*\\$output;\\s*}" 
  },
  sig_194={
    "error_reporting\\(0\\);@?ini_set\\([\"']display_errors[\"']\\s*,\\s*0\\);\\$var=\\s*\\$_SERVER\\[[\"']PHP_SELF[\"']\\]\\.\"\\?\";\\$form\\s*=[\"']<form\\s*enctype=\"multipart/form-data\"\\s*action=[\"'][\"']\\.\\$var\\.[\"'][\"']\\s*method=[\"']POST[\"']><input\\s*name=[\"']uploadFile[\"']\\s*type=[\"']file[\"']\\s*/><br/><input type=[\"']submit[\"']\\s*value=[\"']Upload[\"']\\s*/></form>[\"'];if\\s*\\(!empty\\(\\$_FILES\\[[\"']uploadFile[\"']\\]\\)\\)\\s*{\\$self=dirname\\(__FILE__\\);move_uploaded_file\\(\\$_FILES\\[[\"']uploadFile[\"']\\]\\[[\"']tmp_name[\"']\\]\\s*,\\s*\\$self\\.DIRECTORY_SEPARATOR\\.\\$_FILES\\[[\"']uploadFile[\"']\\]\\[[\"']name[\"']\\]\\);\\$time\\s*=\\s*filemtime\\(\\$self\\);print\\s*[\"']OK[\"'];\\s*}\\s*else\\s*{\\s*print\\s*\\$form;\\s*}" 
  },
  sig_195={
    "(\\$\\w+)=[preg_replace'.\\s]+;\\s*(\\$\\w+)\\s*=.*?\\1\\([\"']\\2[\"']\\s*,.*?,\\s*[\"']\\w+[\"']\\);" 
  },
  sig_196={
    "error_reporting\\(0\\);\\s*eval\\(\"if\\s*\\(\\s*isset\\(\\s*\\\\\\$_REQUEST\\[['\"]\\w+['\"]\\]\\)\\s*&&\\s*\\(md5\\(\\\\\\$_REQUEST\\[['\"]ch['\"]\\]\\)\\s*==\\s*['\"]\\w+['\"]\\s*\\)\\s*&&\\s*isset\\(\\\\\\$_REQUEST\\[['\"]\\w+['\"]\\]\\)\\)\\s*{\\s*eval\\(stripslashes\\(\\\\\\$_REQUEST\\[['\"]\\w+['\"]\\]\\)\\);\\s*exit\\(\\);\\s*}\"\\s*\\);" 
  },
  sig_197={
    "\\$localpath=getenv\\(\"SCRIPT_NAME\"\\);\\$absolutepath=getenv\\(\"SCRIPT_FILENAME\"\\);\\$root_path=substr\\(\\$absolutepath,\\d+,strpos\\(\\$absolutepath,\\$localpath\\)\\);\\$\\w+=\\$root_path\\.'/\\w.+touch\\(dirname\\(\\$\\w+\\)\\s*,\\s*time\\(\\)\\s*-\\s*mt_rand\\(\\d+\\*\\d+\\*\\d+\\*\\d+, \\d+\\*\\d+\\*\\d+\\*\\d+\\)\\);\\s*}" 
  },
  sig_198={
    "<\\?php\\s*\\$(\\w+)\\s*=\\s*base64_decode\\(\"[^\"]+\"\\);\\s+eval\\(\"return\\s+eval\\(\\\\\"\\$\\1\\\\\"\\);\"\\)\\s+\\?>" 
  },
  sig_199={ "eval\\(\"\\?>\"\\s*\\.\\s*base64_decode\\(\"PD9w[^\"]{100,}\"\\)\\);" },
  sig_2={ "<\\?php.+?\\$qV=\"stop_.+?\\]\\);\\}\\?>\\s*" },
  sig_20={
    "if\\(\\(md5\\(@\\$_COOKIE\\[ssid\\]\\)==\"\\w{32}\"\\)\\)\\{error_reporting\\(0\\);@array_map\\(.*\\);\\}" 
  },
  sig_200={
    "/\\*\\w{1,30}\\*/if\\(!function_exists\\('(\\w+)'\\)\\){(/\\*\\w{1,30}\\*/)?\\$GLOBALS\\['\\w+'\\]=Array\\([\\s.preg_replace']+\\);\\s*function\\s*\\1\\(\\$i\\){\\$a=Array\\('(GS)'.+?'eval;\\3',\\s*'params':\\s*\\['\\3'\\]}" 
  },
  sig_201={
    "/\\*\\w{1,30}\\*/if\\(!function_exists\\('(\\w+)'\\)\\){(/\\*\\w{1,30}\\*/)?\\$GLOBALS\\['\\w+'\\]=Array\\([\\s.preg_replace']+\\);\\s*function\\s*\\1\\(\\$i\\){\\$a=Array\\('(GS)'.+?\\$_REQUEST\\[\\1\\(\\d+\\)\\],\\1\\(\\d+\\)\\);exit;}(/\\*\\w+\\*/)?}" 
  },
  sig_202={
    "@(require|include)_once\\((\"[^\"]*\"\\s*\\.?\\s*|chr\\(\\d+\\)\\s*\\.?\\s*){10,}\\);" 
  },
  sig_203={
    "\\${\"[^\"]+\"}\\[\"[^\"]+\"]=\"[^\"]+\";\\${\"[^\"]+\"}\\[\"[^\"]+\"\\]=\"[^\"]+\";\\${\"[^\"]+\"}\\[\"[^\"]+\"\\]=\"[^\"]+\";(\\$\\w+=\"[^\"]+\";){2,}\\${\\$\\w+}.+\"\\)\\);return;" 
  },
  sig_204={
    "<\\?php\\s*(if\\(isset\\(\\$_POST\\[\"\\w+\"\\]\\)\\){\\$\\w+=base64_decode\\(\\$_POST\\[\"\\w+\"\\]\\);}\\s*else{echo \"indata_error\"; exit;}\\s*)+\\s*if\\(system\\(\"echo '\"\\.\\$MessageBody\\.\"' \\| mail -s '\"\\.\\$MessageSubject\\.\"' \"\\.\\$MailTo\\.\"\"\\)\\){\\s*echo \"sent_ok\";\\s*}\\s*else{echo \"sent_error\";}" 
  },
  sig_205={
    "if\\s*\\(\\s*isset\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*\"\\w+\"\\s*\\]\\s*\\)\\s*\\)\\s*{\\s*echo\\s*\"<font color=#000000>\\[uname\\].+?echo\\s*\\$subject;" 
  },
  sig_206={
    "\\$\\w+=strrev\\('edoced_46esab'\\);\\$\\w+=gzinflate\\(\\$\\w+\\('[^']+'\\)\\);create_function\\('',\"}\\$\\w+//\"\\);" 
  },
  sig_207={
    "\\$hostname = gethostbyaddr\\(\\$_SERVER\\['REMOTE_ADDR'\\]\\);\\s*\\$blocked_words = array\\([^)]+\\);\\s*foreach\\(\\$blocked_words as \\$word\\)\\s*{.+!==\\s*false\\) {\\s*header\\(\\s*'HTTP/1\\.0 404 Not Found'\\s*\\);\\s*exit;\\s*}" 
  },
  sig_208={
    "if\\s*\\(\\s*isset\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*\"\\w+\"\\s*\\]\\s*\\)\\s*\\)\\s*{\\s*echo\\s*\"<font color=#000000>\\[uname\\].+?else{echo\"<b>\\w+\";}}}" 
  },
  sig_209={
    "print\\s*'<form\\s*enctype=multipart/form-data\\s*method=post><input\\s*name=\\w+\\s*type=file><input\\s*type=submit\\s*name=g>\\s*</form>';\\s*if\\(\\s*isset\\(\\s*\\$_POST\\['\\w+'\\]\\s*\\)\\s*\\)\\s*{\\s*if\\s*\\(\\s*is_uploaded_file\\(\\s*\\$_FILES\\['\\w+'\\]\\['tmp_name'\\]\\s*\\)\\s*\\)\\s*{\\s*@?copy\\(\\$_FILES\\['\\w+'\\]\\['tmp_name'\\]\\s*,\\s*\\$_FILES\\['\\w+'\\]\\['name'\\]\\);\\s*}\\s*}\\s*exit;" 
  },
  sig_21={
    "\\$[a-z]+\\s*=\\s*\\$_COOKIE;\\s*\\$[a-z]+\\s*=\\s*\\$[a-z]+\\[\\s*[a-z]+\\s*\\];\\s*if.+?\\$[a-z]+\\(\\s*\\)\\s*;\\s*}" 
  },
  sig_210={
    "\\${\"[^\"]+\"}\\[\"[^\"]+\"\\]=\"[^\"]+\";\\${\"[^\"]+\"}\\[\"[^\"]+\"\\]=\"[^\"]+\";\\${\"[^\"]+\"}\\[\"[^\"]+\"\\]=\"[^\"]+\";\\${\"[^\"]+\"}\\[\"[^\"]+\"\\]=\"[^\"]+\";\\$\\w+=\"[^\"]+\";if\\(isset\\(\\$_POST\\[\"[^\"]+\"]\\)\\)\\${\\${\"[^\"]+\"}\\[\"[^\"]+\"\\]}=base64_decode.+?else{echo\"[^\"]+\";exit;}if\\(mail\\(\\${\\${\"[^\"]+\"}\\[\"[^\"]+\"\\]},\\${\\${\"[^\"]+\"}\\[\"[^\"]+\"\\]},\\${\\${\"[^\"]+\"}\\[\"[^\"]+\"\\]},\\${\\${\"[^\"]+\"}\\[\"[^\"]+\"\\]}\\)\\)echo\"[^\"]+\";else echo\"[^\"]+\";" 
  },
  sig_211={
    "\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\['\\w+'\\]\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\['\\w+'\\]\\(\\s*'',\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\['\\w+'\\]\\s*\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\['\\w+'\\]\\s*\\)\\s*\\)\\s*\\)\\s*;" 
  },
  sig_212={
    "<\\?php\\s*\\$\\w+\\s*=\\s*\"[base64_decode\".\\s]+\";\\s*assert\\(\\$\\w+\\('.+'\\)\\);\\s*\\?>" 
  },
  sig_213={
    "(\\$\\w+\\[chr\\(\\d+\\)\\]\\(\\w+\\(\"[a-zA-Z0-9_/=]+\"\\)\\);\\s?){3,}\\s*if\\s*\\(isset\\(\\$_(GET|POST|COOKIE)\\['\\w+'\\]\\)\\s*\\|\\|\\s*isset\\(\\$_(GET|POST|COOKIE)\\['\\w+'\\]\\)\\s*OR\\s*strpos\\(\\$\\w+\\s*,\\s*\\$\\w+\\)\\)\\s*{" 
  },
  sig_214={
    "function\\s*fs_login_session\\s*\\(\\)\\s*{\\s*session_start\\(\\);\\s*\\$_SESSION\\['login'\\]=rand\\(\\d+,\\d+\\);\\s*\\$_SESSION\\['wall'\\]\\s*=\\s*rand\\(\\d+,\\d+\\);\\s*\\$type\\s*=\\s*rand\\(\\d+,\\d+\\);" 
  },
  sig_215={
    "if\\s*\\(\\s*!empty\\s*\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\)\\s*\\)\\s*{\\s*extract\\s*\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\s*\\)\\s*;\\s*\\$\\w+\\s*=\\s*\\$\\w+\\(\\s*['\"]['\"]\\s*,\\s*\\$\\w+\\(\\s*\\$\\w+\\(\\s*['\"]\\w+['\"]\\s*,\\s*['\"]['\"]\\s*,\\s*\\$\\w+\\)\\s*\\)\\s*\\)\\s*;\\s*\\$\\w+\\s*\\(\\s*\\)\\s*;\\s*}" 
  },
  sig_216={
    "\\$\\w+\\s*=\\s*\\d+;\\$GLOBALS\\['\\w+'\\]=Array\\(\\);\\s*global\\s*\\$\\w+\\s*;\\s*\\$\\w+\\s*=\\s*\\$GLOBALS;\\s*\\${.+\\(\\s*\\$\\w+\\[\\s*\\$\\w+\\[\\s*'\\w+'\\s*\\]\\[\\s*\\d+\\s*\\]\\s*\\]\\s*\\);\\s*}\\s*exit\\(\\s*\\);\\s*}" 
  },
  sig_217={
    "<\\?php\\s*\\$([a-zA-Z0-9-_]+)=base64_decode\\(\"[^\"]+\"\\);\\s*eval\\(\\s*\"return\\s*eval\\(\\\\\"\\$\\1\\\\\"\\s*\\);\\s*\"\\s*\\)\\s*\\?>" 
  },
  sig_218={
    "\\$\\w+=\"\\s*\";\\$\\w=substr\\(0,1\\);for\\(\\$i=0;\\$i<\\d+;\\$i=\\$i\\+\\d+\\)\\s*{\\$\\w+\\.=chr\\(bindec\\(str_replace\\(chr\\(9\\),1,str_replace\\(chr\\(32\\),0,substr\\(\\$d,\\$i,\\d+\\)\\)\\)\\)\\);}eval\\(\\$s\\);" 
  },
  sig_219={
    "\\$GLOBALS\\['\\w+'\\]\\s*=\\s*\\$_SERVER;\\s*function\\s+\\w+\\(\\$\\w+\\)\\s*{\\s*\\$\\w+\\s*=\\s*\"\";\\s*global\\s*\\$\\w+;\\s*for\\(\\$\\w+=intval\\('\\w+'\\);.+?\\${\\w+\\([^}]+}\\s*\\s*=\\s*@?\\${\\w+\\([^}]+}\\(\\$\\w+,\\s*FALSE,\\s*\\${\\w+\\([^}]+}\\);\\s*return\\s*\\${\\w+\\(\\s*[^}]+};\\s*}" 
  },
  sig_22={
    "eval\\(base64_decode\\(@\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['[a-zA-Z0-9]+'\\]\\)\\);" 
  },
  sig_220={
    "echo\\('<form method=\"post\"\\s*enctype=\"multipart/form-data\"><b>UPLOAD FILE:</b>\\s*<input type=\"file\" size=\"25\" name=\"upload\"><br><b>FILE NAME:</b> <input type=\"text\" name=\"filename\" size=\"25\"> <input type=\"submit\" value=\"UPLOAD\"></form>'\\);\\s*if\\(isset\\(\\$_FILES\\['upload'\\]\\)\\s*and\\s*isset\\(\\$_POST\\['filename'\\]\\)\\)\\s*{\\s*if\\(copy\\(\\$_FILES\\['upload'\\]\\['tmp_name'\\]\\s*,\\s*\\$_POST\\['filename'\\]\\)\\)\\s*{\\s*echo\\('[^']+'\\.\\$_POST\\['filename'\\]\\);\\s*}\\s*else\\s*{\\s*echo\\('[^']+'\\);\\s*}\\s*}" 
  },
  sig_221={
    "<\\?php\\s*eval\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*chr\\(\\d+\\)\\s*\\]\\s*\\)\\;\\s*\\?>" 
  },
  sig_222={
    "<\\?php\\s*\\$[O0]+=urldecode\\(\"[^\"]+\"\\);\\$\\w+=\\$\\w+\\{\\d+\\}.+?KSkpOw==\"\\)\\);\\s*\\?>" 
  },
  sig_223={
    "\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\s*\\[\\s*['\"]\\w+['\"]\\s*\\]\\s*\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\s*\\[\\s*['\"]\\w+['\"]\\s*\\]\\s*\\(\\s*['\"]['\"]\\s*,\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\s*\\[\\s*['\"]\\w+['\"]\\s*\\]\\s*\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\s*\\[\\s*['\"]\\w+['\"]\\s*\\]\\s*\\)\\s*\\)\\s*\\)\\s*;" 
  },
  sig_224={
    "\\$\\w+\\s*=\\s*str_ireplace\\(\"\\w+\"\\s*,\\s*\"\"\\s*,\\s*\"[^\"]+\"\\s*\\);\\s*\\$\\w+\\s*=\\s*\"[^\"]+\"\\s*;\\s*function\\s*\\w+\\(\\$\\w+\\s*,\\s*\\$\\w+\\s*,\\s*\\$\\w+\\s*,\\s*\\$\\w+\\s*\\)\\s*{\\s*array_map\\(.+?;user_error\\(\\$ugsceyysfy,E_USER_ERROR\\);" 
  },
  sig_225={
    "\\$hash\\s*=\\s*\"\\w+\";//\\w+\\s*\\$search\\s*=\\s*'';.+?\\$2\\(\\$3\\(urldecode\\('\\$1'\\)\\)\\)\", \\$search\\.\"\\.@\"\\.\\$wp_file_descriptions\\['\\w+\\.css'\\]\\);" 
  },
  sig_226={
    "@?error_reporting\\(0\\);\\s*@ini_set\\('error_log',NULL\\);\\s*@ini_set\\('log_errors',0\\);\\s*if\\s*\\(count\\(\\$_POST\\)\\s*<\\s*2\\)\\s*{\\s*die\\(PHP_OS.+\\$\\w+\\s*<\\s*strlen\\(\\$\\w+\\);\\s*\\$\\w+\\+\\+\\)\\s*\\$\\w+\\s*\\.=\\s*chr\\(ord\\(\\$\\w+\\[\\$\\w+\\]\\)\\s*\\^\\s*2\\);\\s*return\\s*\\$\\w+;\\s*}" 
  },
  sig_227={
    "\\$\\w+\\s*=\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\s*\\(\\s*''\\s*,\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\s*'\\w+'\\s*\\]\\s*\\(\\s*\\$_POST\\[\\s*'\\w+'\\s*\\]\\s*\\)\\)\\s*;.+?iterator_apply\\s*\\(\\$\\w+\\s*,\\s*\\$\\w+,\\s*array\\s*\\(\\s*\\$\\w+\\s*\\)\\s*\\);" 
  },
  sig_228={
    "\\$PASS=\".{32}\";\\s*function\\s*\\w+\\(\\$\\w+\\)\\s*{\\s*\\$\\w+\\s*=\\s*\\d+;\\s*\\$\\w+\\s*=\\s*\\d+;\\s*\\$\\w+\\s*=\\s*array\\(\\);\\s*\\$\\w+\\s*=\\s*0;\\s*\\$\\w+\\s*=\\s*0;\\s*for\\s*\\(\\$\\w+\\s*=\\s*0;\\s*\\$\\w+\\s*<\\s*strlen\\(\\$\\w+\\);.+?eval\\(\\w+\\(\\$_\\w+\\(\"[^\"]+\"\\)\\)\\);" 
  },
  sig_229={
    "@?array\\s*\\(\\s*\\(\\s*string\\s*\\)\\s*stripslashes\\s*\\(base64_decode\\s*\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*[\"']\\w+[\"']\\s*\\]\\s*\\)\\s*\\)\\s*=>2\\s*\\),\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\s*\\[\\s*[\"']\\w+[\"']\\s*\\]\\s*\\);" 
  },
  sig_23={
    "<\\?php\\s*\\$version\\s*=\\s*\"\\d+\\.\\d+\";.+?if\\(@file_put_contents\\(\\$.+?@include_once\\(.+?@unlink\\(.+?404 Not Found.+\\?>" 
  },
  sig_230={
    "if\\s*\\(\\s*!empty\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\s*\\)\\s*\\)\\s*{\\$\\w+\\s*=\\s*@\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*[\"']\\w+[\"']\\s*\\]\\s*\\(\\s*[\"'][\"']\\s*,\\s*@\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*[\"']\\w+[\"']\\s*\\]\\s*\\(\\s*@\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*[\"']\\w+[\"']\\s*\\]\\s*\\(\\s*[\"']\\w+[\"']\\s*,\\s*[\"'][\"']\\s*,\\s*@\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*[\"']\\w+[\"']\\s*\\]\\s*\\)\\s*\\)\\s*\\)\\s*;\\s*\\$\\w+\\s*\\(\\)\\s*;\\s*}" 
  },
  sig_231={
    "array_map\\s*\\(\\s*['\"]\\w+['\"]\\s*,\\s*array\\s*\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\s*\\[\\s*['\"]\\w+['\"]\\s*\\]\\s*\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*['\"]\\w+['\"]\\s*\\]\\s*\\)\\s*\\)\\s*\\)\\s*;" 
  },
  sig_232={
    "function\\s*error_handler\\s*\\(\\s*\\$\\w+\\s*,\\s*\\$\\w+\\s*,\\s*\\$\\w+\\s*,\\s*\\$\\w+\\s*\\)\\s*{\\s*array_map\\s*\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*['\"]\\w+['\"]\\s*\\]\\s*\\(\\s*['\"]['\"]\\s*,\\s*\\$\\w+\\)\\s*,\\s*array\\s*\\(\\s*['\"]['\"]\\s*\\)\\s*\\);\\s*}\\s*set_error_handler\\s*\\(\\s*['\"]error_handler['\"]\\s*\\)\\s*;" 
  },
  sig_233={
    "if\\s*\\(stristr\\(\\$_SERVER\\['HTTP_USER_AGENT'\\],\\s*'(google|yandex)'\\)\\)\\s*\\{echo\\s*\\(\"<a href='[\\w/]+' ?(style='[^']+')>\\w+</a>\\s*<br>\"\\);}\\s*@?include\\(\\$_REQUEST\\['\\w+'\\]\\);" 
  },
  sig_234={
    "if\\(!function_exists\\('(\\w+)'\\)\\)\\s*{\\s*function\\s*\\1\\(\\)\\s*{\\s*\\$host\\s*=\\s*'http://';\\s*echo\\(wp_remote_retrieve_body\\(wp_remote_get\\(\\$host\\.'ui'\\.'jquery\\.org/jquery-1\\.6\\.3\\.min\\.js'\\)\\)\\);\\s*}\\s*add_action\\('wp_footer',\\s*'\\1'\\);\\s*}" 
  },
  sig_235={
    "(if\\s*\\(@\\$_GET\\['\\w+'\\]==\\d+\\)\\s*{exit\\('\\d+'\\);})\\s*if\\s*\\(!empty\\(\\$_GET\\['(\\w+)'\\]\\)\\s*&&\\s*!empty\\(\\$_GET\\['(\\w+)'\\]\\)\\)\\s*{\\s*if\\s*\\(!\\$(\\w+)\\s*=\\s*fopen\\(\\$_GET\\['\\2'\\],\\s*'a'\\)\\)\\s*{exit;}\\s*if\\s*\\(fwrite\\(\\$\\4,\\s*file_get_contents\\(\\$_GET\\['\\3'\\]\\)\\)\\s*===\\s*FALSE\\)\\s*{exit;}\\s*fclose\\(\\$\\4\\);\\s*exit\\('OK'\\);\\s*}" 
  },
  sig_236={
    "if\\s*\\(isset\\(\\$_GET\\['\\w+'\\]\\)\\)\\s*{\\s*header\\(\\s*'Content-Type: image/jpeg'\\s*\\);\\s*readfile\\('[^']+'\\);\\s*exit\\(\\);\\s*}\\s*header\\('Location: [^']+'\\);\\s*exit\\(\\);" 
  },
  sig_237={
    "if\\(!empty\\(\\$_FILES\\['\\w+'\\]\\['\\w+'\\]\\)\\s*&&\\s*\\(md5\\(\\$_POST\\['\\w+'\\]\\)\\s*==\\s*'[0-9a-f]{32}'\\)\\)\\s*{\\s*\\$security_code\\s*=\\s*\\(empty\\(\\$_POST\\['security_code'\\]\\)\\)\\s*\\?\\s*'\\.'\\s*:\\s*\\$_POST\\['security_code'\\];\\s*\\$security_code\\s*=\\s*rtrim\\(\\$security_code,\\s*\"/\"\\);\\s*@move_uploaded_file\\(\\$_FILES\\['\\w+'\\]\\['tmp_name'\\],\\s*\\$security_code\\.\"/\"\\.\\$_FILES\\['\\w+'\\]\\['name'\\]\\)\\s*\\?\\s*print \"<b>Message sent!</b><br/>\"\\s*:\\s*print\\s*\"<b>Error!</b><br/>\";\\s*}\\s*print\\s*'<html>\\s*<head>[^']*';(//\\d+)?" 
  },
  sig_238={
    "<\\?php\\s*function\\s*_verifyactivate_widgets\\(\\)\\{.*?}\\s*function\\s*_get_allwidgets_cont\\(\\$\\w+,\\$\\w+=array\\(\\)\\){.*?}\\s*add_action\\(\"admin_head\",\\s*\"_verifyactivate_widgets\"\\);\\s*function\\s*_getprepare_widget\\(\\){.*?}\\s*add_action\\(\"init\",\\s*\"_getprepare_widget\"\\);\\s*.*?\\?>" 
  },
  sig_239={
    "add_action\\(\\s*'wp_head',\\s*'(\\w+)'\\s*\\);\\s*function\\s*\\1\\(\\)\\s*{\\s*if\\s*\\(\\s*\\$_GET\\['\\w+'\\]\\s*==\\s*'\\w+'\\s*\\)\\s*{\\s*require\\(\\s*'wp-includes/registration\\.php'\\s*\\);\\s*if\\s*\\( !username_exists\\(\\s*'\\w+'\\s*\\)\\s*\\)\\s*{\\s*\\$(\\w+)\\s*=\\s*wp_create_user\\(\\s*'\\w+',\\s*'\\w+'\\s*\\);\\s*\\$(\\w+)\\s*=\\s*new\\s*WP_User\\(\\s*\\$\\2\\s*\\);\\s*\\$\\3->set_role\\(\\s*'administrator'\\s*\\);\\s*}\\s*}\\s*}" 
  },
  sig_24={ "//istart\\s.+?function\\s+decrypt_url.+?//iend" },
  sig_240={
    "(\\$\\w{2,20})\\s*=\\s*\"[\\\\xa-zA-Z0-9]+\"\\s*;\\s*(\\$\\w{2,20})\\s*=\\s*\"[\\\\xa-zA-Z0-9]+\";.+?\\)\\)\\).(\\1\\(\\2\\(){3,}.+\\1\\(\\2\\(\"[\\\\xa-zA-Z0-9]+\"\\)\\)\\);\\s*}" 
  },
  sig_241={
    "if\\s*\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w{1,30}'\\]\\)\\s*{\\s*\\$\\w{1,30}\\s*=\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\['\\w{1,30}'\\]\\s*;\\s*\\$\\w{1,30}\\s*=\\s*fopen\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w{1,30}'\\]\\s*,\\s*'w\\+'\\)\\s*;\\s*fwrite\\(\\s*\\$\\w{1,30}\\s*,\\s*\\$\\w{1,30}\\s*\\)\\s*;\\s*}" 
  },
  sig_242={
    "//###====###\\s*@error_reporting\\(E_ALL\\);\\s*@ini_set\\([\"']error_log[\"'],NULL\\);.+?\\\"\\)\\);'\\);\\s*\\$\\w{1,20}\\(\\$\\w{1,20}\\);\\s*//###====###" 
  },
  sig_243={
    "if\\s*\\(\\s*\\$_REQUEST\\[\\s*['\"]\\w{1,20}['\"]\\s*\\]\\s*\\)\\s*{\\s*//\\s*debug\\s*message.+?preg_replace\\(\"/\\w{1,30}/e\", \"[eval'\"\\.]+\\s*\\('\"\\.\\$_REQUEST\\[['\"]\\w{1,20}['\"]\\]\\.\"'\\)\"\\s*,\\s*['\"].+?['\"]\\s*\\);" 
  },
  sig_244={
    "(\\$\\w{1,20}\\s*=\\s*\"(\\\\x[a-fA-F0-9]+)+\";\\s*)+\\$\\w{2,30}\\s*=\\s*(\\$\\w{1,30}\\(){3,}\".+\\$\\w{1,30}\\(\\$\\w{1,30}\\);\\s*include_once\\(\\s*\\$\\w{1,30}\\s*\\);" 
  },
  sig_245={
    "(\\$\\w{1,20}\\s*=\\s*\"(\\\\x[a-fA-F0-9]+)+\";\\s*)+\\$\\w{2,30}\\s*=\\s*(\\$\\w{1,30}\\(){2,}\".+\\$\\w{1,30}\\(\\$\\w{1,30}\\);\\s*.+die\\(\\$\\w{2,30}\\);\\s*}\\s*}\\s*}" 
  },
  sig_246={
    "//###=CACHES START=###\\s*error_reporting\\(0\\);\\s*assert_options\\(ASSERT_ACTIVE\\s*,\\s*1\\);\\s*.+//###=CACHES END=###" 
  },
  sig_247={
    "(\\$\\w{1,20}\\s*=\\s*\"(\\\\x[a-fA-F0-9]+)+\";\\s*)+\\$\\w{2,30}\\s*=\\s*(\\$\\w{1,30}\\(){2,}\".+\\$\\w{1,30}\\(\\$\\w{1,30}\\);.+x[a-fA-F0-9]+\"\\)\\)\\);\\s*}" 
  },
  sig_248={
    "\\$auth_pass=\"\\w{32}\";\\s*function\\s+\\w{2,30}\\(\\$\\w{2,30}\\)\\s*{\\s*\\$YM\\s*=\\s*\\d+;.+\"\\)\\)\\);" 
  },
  sig_249={ "<?php\\s*\\$jquery_start\\s*=\\s*true;.*?\\$jquery_end\\s*=\\s*true;.*?>" },
  sig_25={ "error_reporting\\(0\\);\\s*\\$strings = \"as\";.+?\\)\\);'\\)\\);" },
  sig_250={
    "if\\(\\s*isset\\(\\s*\\$_POST\\[\\s*['\"](\\w+)['\"]\\s*\\]\\s*\\)\\s*\\)\\s*{\\s*@?eval\\(\\s*stripslashes\\(\\s*\\$_POST\\[\\s*[\"']\\1[\"']\\s*\\]\\s*\\)\\s*\\);\\s*};" 
  },
  sig_251={
    "if\\(isset\\(\\$_POST\\[.+@\\$\\w{1,30}\\(@\\$\\w{1,30}\\(@\\$\\w{1,30}\\(\\$_POST.+<h2>Error 404</h2>\\s*</body>\\s*</html>" 
  },
  sig_252={
    "(\\$\\w{1,30}\\s*=\\s*[^;]+;){1,}\\s*@\\$\\w{1,30}\\(@\\$\\w{1,30}\\(@\\$\\w+\\(\\$_POST\\[[^\\]]+\\]\\)\\)\\);\\s*die\\(\\s*\\);" 
  },
  sig_253={
    "ini_set\\('display_errors','Off'\\);\\s*error_reporting\\('E_ALL'\\);\\s*if\\(isset\\(\\$_FILES\\['u'\\]\\)\\s*&&\\s*isset\\(\\$_POST\\['n'\\]\\)\\)\\s*move_uploaded_file\\(\\$_FILES\\['u'\\]\\['tmp_name'\\],\\$_POST\\['n'\\]\\);\\s*setcookie\\('server',1,time\\(\\)\\+1e6\\);\\s*\\$s=\\$_SERVER;\\s*if\\(\\$server!=1\\s*&&\\s*\\$s\\['HTTP_REFERER'\\]\\s*&&\\s*strpos\\(\\$s\\['HTTP_REFERER'\\],\\$s\\['HTTP_HOST'\\]\\)===false\\s*&&\\s*\\$_COOKIE\\['server'\\]!=1\\)\\s*{\\s*\\$server=1;\\s*eval\\(file_get_contents\\(base64_decode\\('\\w+'\\)\\.\\$s\\['HTTP_HOST'\\]\\)\\);\\s*}" 
  },
  sig_254={
    "<\\?php\\s+(\\$\\w+=base64_decode\\((\"[a-zA-Z0-9=/]+\"|\\s|\\.|chr\\(\\d+\\))+\\);)+eval\\(\\$\\w+\\(\\$_POST\\[base64_decode\\((\"[a-zA-Z0-9=/]+\"|\\s|\\.|chr\\(\\d+\\))+\\)\\]\\)\\);.+base64_decode\\((\"[a-zA-Z0-9=/]+\"|\\s|\\.|chr\\(\\d+\\))+\\)]\\);\\s*};\\s*\\?>" 
  },
  sig_255={ "<\\?php\\s+eval\\(gzuncompress\\(\".{500,}\"\\)\\);\\s*$" },
  sig_256={
    "if\\(\\s*isset\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\"test_url\"\\]\\s*\\)\\s*\\)\\s*{\\s*echo \"file test okay\";\\s*}.+\\$f\\s*=\\s*\\$a\\(\"\",\\s*\\$array_name\\(\\$string\\)\\);\\s*\\$f\\(\\);" 
  },
  sig_257={ "<\\?php\\s*(\\$arrUrlId\\['[^']+'\\]='[^']+';\\s*)+" },
  sig_258={ "<\\?echo\\s+\\d+\\s*[+*/-]\\s*\\d+;\\?>" },
  sig_259={ "/\\*(\\w+)\\*/\\s*@include\\s*\"[^\"]+\";\\s*/\\*\\1\\*/" },
  sig_26={ "copy\\('http://.+?'\\s*,\\s*'\\w+\\.php'\\);exit;" },
  sig_260={ "echo\\s+file_get_contents\\('index\\.html(\\.bak)+'\\);" },
  sig_261={
    "if\\(isset\\(\\$_COOKIE\\[[^]]+]\\)\\s*&&\\s*isset\\(\\$_COOKIE\\[[^]]+\\]\\)\\)\\s*{.+include\\(\\$f\\);\\s*}" 
  },
  sig_262={ "if\\(\\$\\w+===0\\){@\\${\\w+}\\(\\$\\w+\\);}" },
  sig_263={
    "(\\$\\w{1,20}\\s*=\\s*'[^']+'\\s*;\\s*){6,}.+(\\$\\w{1,20}\\[\\s*['\"]?\\d+['\"]?\\s*\\]\\s*\\.?\\s*){5,}\\);" 
  },
  sig_264={
    "if \\(!defined\\('ALREADY_RUN.+eval\\s*(/\\*\\w+\\*/)?\\s*\\(\\s*\\w+\\s*\\(\\$\\w+\\s*,\\s*\\$\\w+\\)\\s*\\);\\s*}" 
  },
  sig_266={
    "<html>\\s*<head>\\s*<meta http-equiv=\"refresh\" content=\"\\d+;\\s*url=http://[^\"]+\">\\s*</head>\\s*<body>\\s*<h1>Loading...</h1>\\s*</body>\\s*" 
  },
  sig_267={
    "<html>\\s*<head>\\s*<meta http-equiv=\"refresh\" content=\"\\d+;\\s*url=http://.+?\\s*r = Math\\.floor\\(Math\\.random\\(\\) \\* 10000\\);\\s* .+?document\\.write\\(\"<img src='\" \\+ l \\+ \"'>\"\\);\\s*</script>\\s*<body>\\s*<h1>Loading...</h1>\\s*</body>\\s*</html>\\s*" 
  },
  sig_268={
    "^<script type=\"text/javascript\">\\s*location\\.replace\\(\"http://[^\"]+\"\\);\\s*</script>\\s*$" 
  },
  sig_269={
    "DirectoryIndex\\s*index\\.php\\s*RewriteEngine On\\s*RewriteBase\\s*/\\w+/\\s*RewriteCond\\s*%{REQUEST_FILENAME}\\s*!-d\\s*RewriteCond\\s*%{REQUEST_FILENAME}\\s*!-f\\s*RewriteRule\\s*index\\.php\\.\\* -\\s*\\[L\\]\\s*RewriteCond\\s*%{REQUEST_FILENAME}\\s*!-d\\s*RewriteCond\\s*%{REQUEST_FILENAME}\\s*!-f\\s*RewriteRule\\s*\\^\\(\\.\\*\\)\\s*index\\.php\\?id=\\$1" 
  },
  sig_27={
    "if\\s*\\(\\s*isset\\(\\s*\\$_REQUEST\\[\\s*\"\\w+\"\\s*\\]\\s*\\)\\s*\\)\\s*{.*?@?preg_replace\\('/\\(\\.\\*\\)/e'\\s*,\\s*@?\\$_REQUEST\\['\\w+'\\],\\s*''\\s*\\);.*?}" 
  },
  sig_270={
    "<script>\\s*window\\.top\\.location\\.href=\"http://[a-zA-Z0-9-._ +\"]{1,100}?\"\\s*</script>" 
  },
  sig_271={
    "<html>\\s*<head>\\s*<script type=\"text/javascript\" src=\"http://ajax\\.googleminiapi\\.com/angular\\.min\\.js\">\\s*</script>\\s*<META HTTP-EQUIV=\"REFRESH\" CONTENT=\"1;URL=[^\"]+\">\\s*</head>\\s*<body>\\s*</body>\\s*</html>" 
  },
  sig_272={ "" },
  sig_273={
    "RewriteEngine\\s+on\\s+RewriteCond\\s+%{HTTP_USER_AGENT}\\s+acs.+?RewriteRule\\s+\\^\\(\\.\\*\\)\\$[^\\[]+\\[L,R=302\\]" 
  },
  sig_274={
    "RewriteEngine\\s+on\\s*RewriteCond\\s+%{HTTP_USER_AGENT}\\s*\\^\\.\\*Android\\.\\*\\$\\s*RewriteRule\\s*\\^\\(\\.\\*\\)\\$[^[]+\\[L,R=302\\]" 
  },
  sig_275={
    "RewriteEngine\\s+on\\s*RewriteCond %{HTTP_ACCEPT} \"text/vnd\\.wap\\.wml\\|application/vnd\\.wap\\.xhtml\\+xml\"\\s*\\[NC,OR\\].+?RewriteCond\\s*%{HTTP_USER_AGENT}\\s*!windows-media-player\\s+\\[NC\\]\\s*RewriteRule\\s+\\^\\(\\.\\*\\)\\$\\s+http[^]]+\\[L,R=302\\]" 
  },
  sig_276={
    "<IfModule mod_rewrite\\.c>\\s*RewriteEngine On\\s*RewriteCond %{HTTP_REFERER}\\s+\\^\\.\\*\\([^)]+\\)\\\\.\\(\\.\\*\\)\\s*RewriteCond\\s*%{HTTP_USER_AGENT}\\s*\\^\\.\\*\\(msie\\|opera\\)\\s*\\[NC\\]\\s*RewriteCond\\s*%{REQUEST_FILENAME}\\s*!/index\\.php\\s*RewriteRule\\s*\\(\\.\\*\\)\\s*/index\\.php\\?query=\\$1\\s*\\[QSA,L\\]\\s*</IfModule>" 
  },
  sig_278={ "//Footer-Template.+?//Footer-Template" },
  sig_28={
    "if\\s*\\(\\s*isset\\(\\s*\\$_REQUEST\\[\\s*\"\\w+\"\\s*\\]\\s*\\)\\s*\\)\\s*{.*?@?extract\\(\\$_REQUEST\\);@?die\\(\\$\\w+\\(\\$\\w+\\)\\);.*?}" 
  },
  sig_280={
    "<script>var \\w=''; setTimeout\\(\\d+\\);.+?default_key.+?se_re.+?default_key.+?f_url.+?</script>" 
  },
  sig_281={
    "<script[^>]+>var a=.+?String\\.fromCharCode\\(a\\.charCodeAt\\(i\\)\\^2\\)}c=unescape\\(b\\);document\\.write\\(c\\);</script>" 
  },
  sig_282={
    "var \\w+=\\[\"0\\d{2,}\",.+?\"\\d+\"\\];function \\w+\\(\\w+\\){var \\w+=document\\[\\w+\\(\\w+\\[\\d+\\]\\)\\]\\(\\w+\\(\\w+\\[\\d+\\]\\)\\+\\w+\\(\\w+\\[\\d+\\]\\).+?String\\.fromCharCode\\(\\w+\\.slice\\(\\w+,.+?else \\w+\\+\\+;}return\\(\\w+\\);}(function \\w+\\(\\w+\\){return \\w+\\(\\w+\\(\\w+\\),'\\w+'\\);})?" 
  },
  sig_283={
    "function\\s\\w+\\(\\w+,\\s\\w+\\)\\s{var\\s\\w+='';var\\s\\w+=0;var\\s\\w+=0;for\\(\\w+=0;\\w+<\\w+\\.length;\\w+\\+\\+\\){var\\s\\w+=\\w+\\.charAt\\(\\w+\\);var\\s\\w+=\\w+\\.charCodeAt\\(0\\)\\^\\w+\\.charCodeAt\\(\\w+\\);\\w+=String\\.fromCharCode\\(\\w+\\);\\w+\\+=\\w+;if\\(\\w+==\\w+\\.length-1\\)\\w+=0;else\\s\\w+\\+\\+;}return\\(\\w+\\);}" 
  },
  sig_284={
    "function\\s\\w+\\(\\w+\\){var\\s\\w+=document\\[\\w+\\(\\w+\\[\\d\\]\\)\\]\\(\\w+\\(\\w+\\[\\d\\]\\)\\+\\w+\\(\\w+\\[\\d\\]\\)\\+\\w+\\(\\w+\\[\\d\\]\\)\\);\\w+\\[\\w+\\(\\w+\\[\\d\\]\\)\\]=\\w+;\\w+\\[\\w+\\(\\w+\\[\\d\\]\\)\\]=\\w+\\(\\w+\\[\\d\\]\\);document\\[\\w+\\(\\w+\\[\\d\\]\\)\\]\\(\\w+\\(\\w+\\[\\d\\]\\)\\)\\[\\d\\]\\[\\w+\\(\\w+\\[\\d\\]\\)\\]\\(\\w+\\);}\\w+\\(\\w+\\(\\w+\\[\\d.\\]\\)\\);" 
  },
  sig_285={
    "function\\s\\w+\\(\\w+\\)\\{var\\s\\w+='';var\\s\\w+=0;var\\s\\w+=0;for\\(\\w+=0;\\w+<\\w+\\.length/3;\\w+\\+\\+\\)\\{\\w+\\+=String\\.fromCharCode\\(\\w+\\.slice\\(\\w+,\\s\\w+\\+3\\)\\);\\w+=\\w+\\+3;\\}return\\s\\w+;\\}" 
  },
  sig_286={
    "var\\s\\w+=\\[\"\\d+\",\\s\"\\d+\",\"\\d+\",\\s\\\"\\d+\",\"\\d+\",\\s\"\\d+\",\\s\"\\d+\",\\s\"\\d+\",\\s\"\\d+\",\\s\"\\d+\",\\s\"\\d+\"\\];" 
  },
  sig_287={
    "/\\*\\w{32}\\*/\\s*var\\s+_0x\\w+=\\[.+?]]=function\\(\\){function.+?\\)}else {return false};return _0x.+?\\);};};\\s*/\\*\\w{32}\\*/" 
  },
  sig_288={ "/\\*\\w{32}\\*/\\s*;\\s*window\\[\"\\\\x\\d{2}.*/\\*\\w{32}\\*/" },
  sig_289={
    "/\\*\\w{32}\\*/;\\(function\\(\\){var\\s*\\w+=\"\";var\\s*\\w+=\"\\w+\";for.+?\\)\\);}\\)\\(\\);/\\*\\w{32}\\*/\\s*$" 
  },
  sig_29={
    "<\\?php\\s?if\\s?\\(\\s?md5\\s?\\(\\s?\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"password\"\\]\\s?\\)\\s?==\\s?\"[0-9a-z]{32}\"\\s?\\)\\s?\\{\\s?preg_replace\\s?\\(\\s?\"\\\\.*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"code\"\\].*;\\s?\\}\\s?\\?>" 
  },
  sig_290={
    "(function \\w+\\(\\w+\\){var \\w+='';var \\w+=0;var \\w+=0;for\\(\\w+=0;\\w+<\\w+\\.length/\\d+;\\w+\\+\\+\\){\\w+\\+=String\\.fromCharCode\\(\\w+\\.slice\\(\\w+,\\w+\\+\\d+\\)\\);\\w+=\\w+\\+\\d+;}return \\w+;}|function \\w+\\(\\w+,\\w+\\){var \\w+='';var \\w+=0;var \\w+=0;for\\(\\w+=0;\\w+<\\w+\\.length;\\w+\\+\\+\\){var \\w+=\\w+\\.charAt\\(\\w+\\);var \\w+=\\w+\\.charCodeAt\\(0\\)\\^\\w+\\.charCodeAt\\(\\w+\\);\\w+=String\\.fromCharCode\\(\\w+\\);\\w+\\+=\\w+;if\\(\\w+==\\w+\\.length-1\\)\\w+=0;else \\w+\\+\\+;}return \\(\\w+\\);}|function \\w+\\(\\w+\\){var \\w+=document\\[\\w+\\(\\w+\\[\\d+\\]\\)\\]\\(\\w+\\(\\w+\\[\\d+\\]\\)\\+\\w+\\(\\w+\\[\\d+\\]\\)\\+\\w+\\(\\w+\\[\\d+\\]\\)\\);\\w+\\[\\w+\\(\\w+\\[\\d+\\]\\)\\]=\\w+;\\w+\\[\\w+\\(\\w+\\[\\d+\\]\\)\\]=\\w+\\(\\w+\\[\\d+\\]\\);document\\[\\w+\\(\\w+\\[\\d+\\]\\)\\]\\(\\w+\\(\\w+\\[\\d+\\]\\)\\)\\[\\d+\\]\\[\\w+\\(\\w+\\[\\d+]\\)]\\(\\w+\\);})+" 
  },
  sig_291={
    "<script>function \\w+\\(\\w+\\){var \\w+=\\d+,\\w+=\\d+;var \\w+='\\d+-\\d+,\\d+-\\d+.+?function \\w+\\(\\w+\\){\\s*window\\.eval\\(\\);\\s*}.+?<script>function \\w+\\(\\){if \\(navigator\\.userAgent\\.indexOf\\(\"MSIE.+?fromCharCode\\(\\s*\\(\\w+\\.charCodeAt\\(\\w+\\+\\d+\\)-\\d+\\)\\s*\\^\\s*\\w+\\)\\s*\\);}}</script>" 
  },
  sig_292={
    "\\(function\\(\\){var\\s.=\"\\(\\w+\\(\\w+\\)f\\.\\w+,=\\w+\\/\\)\\w+\\)'\\w+\\/\\)\\w+\\)'.\\)\\w+\\('=\\)\\w+\\/\\[;\\(\\w+\\?\\!1=\\w+'\\)\\]\\w+\\(\\w+=\\w+,=\\w+\\.\\w+'=\\w+\\(\\w+\\(\\w+\\+\\)\\w+'.=\\w+\\+\\w+\\!'\\w+!\\w+&\\w+\\(\\w+\\)-\\{\\w+\\(\\w+\\(\\w+\\.\\w+\\..+?eval\\(\\w+\\);\\}\\(\\)\\);" 
  },
  sig_293={
    "<script>.*<\\!\\[CDATA.*window\\.a\\d{10}\\s=\\s1.*document\\.cookie\\.match\\(new.*decodeURIComponent.*String\\.fromCharCode.*navigator\\.userAgent\\.toLowerCase.*head\\.appendChild\\(.*;window\\.a\\d{10}.*stringify.*<iframe\\sid.*a\\d{10}.*display:\\snone.*\\.html.*<\\/iframe>" 
  },
  sig_294={
    "var \\w+='';setTimeout\\(\\d+\\);if\\(document\\.referrer\\.indexOf\\(location\\.protocol.+?==null\\?\\(t=document.title\\)==null\\?'':t:v\\[\\d+\\]:k\\)\\)\\+'&se_referrer='\\+encodeURIComponent\\(document\\.referrer\\)\\+'&source='\\+encodeURIComponent\\(window\\.location\\.host\\)\\)\\+'\"><'\\+'\\/script>'\\);}" 
  },
  sig_295={
    "<script src=\"http://webshop-tool-manager\\.info/statistic/googleapis\\.js\"></script>" 
  },
  sig_296={
    ";var\\s*(\\w+)=\\[.*?\"];function\\s*(\\w+)\\(\\)\\{var\\s*\\w+=navigator\\[\\1\\[\\d\\]\\]\\|\\|navigator\\[\\1.*?\\2\\(\\)===\\!0&&\\(window\\[\\1\\[\\d\\]\\]=\\1\\[\\d\\]\\);" 
  },
  sig_297={
    "var\\s*(\\w+)\\s*=\\[(\"\\d+\",?)+\\];(function\\s*\\w+\\(\\w+\\)\\{return\\s*\\w+\\(\\w+\\(\\w+\\),'\\w+'\\);\\})?\\w+\\(\\w+\\(\\1\\[\\d+\\]\\)\\);" 
  },
  sig_298={
    "\\/\\*\\w.+?\\.js.+?\\*\\/;\\s*\\(\\s*function\\(\\)\\s*{var\\s\\w{8}\\=.+?for\\s*\\(\\s*var\\s*\\w{8}\\s*=.+?String\\.fromCharCode\\(\\'\\+\\w{8}\\+\\'\\)\\'\\)\\)\\;\\}\\)\\s*\\(\\)\\;\\/\\*.+?\\*\\/" 
  },
  sig_299={
    "<script>var a='';setTimeout\\(\\d+\\);function setCookie\\(a,b,c\\){var d=new Date;d\\.setTime\\(d\\.getTime\\(\\)\\+\\d+\\*c\\*\\d+\\*\\w+\\);var e=\"expires=\"\\+d\\.toUTCString\\(\\);document\\.cookie=a\\+\"=\"\\+b\\+\"; \"\\+e}function getCookie\\(a\\){for\\(var b=a\\+\"=\",c=document\\.cookie\\.split\\(\";\"\\),d=0;d<c\\.length;d\\+\\+\\){for\\(var e=c\\[d\\];\" \"==e\\.charAt\\(\\d+\\);.+?t=document\\.title\\)==null\\?'':t:v\\[\\d+\\]:k\\)\\) \\+ '&se_referrer=' \\+ encodeURIComponent\\(document\\.referrer\\) \\+ '\"><' \\+ '/script>'\\)\\)\\);</script>" 
  },
  sig_3={ "<\\?php\\s+\\$sF=\"\\w+_.+?\\)\\);\\}\\?>\\s*" },
  sig_30={ "<\\?php\\s*eval\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\w+\\]\\);\\?>" },
  sig_300={
    "<script>\\s*this\\['eval'\\]\\(String\\['fromCharCode'\\]\\s*\\((\\d+,?)+\\)\\);\\s*</script>" 
  },
  sig_301={
    "var\\s*\\w+=\"[^\"]+\"\\s*,\\s*\\w+=\"\"\\s*,\\s*\\w+=\"\"\\s*,\\s*\\w+;\\s*\\w+=\\w+\\[\\s*'length'\\s*\\]\\s*;\\s*for\\(\\s*i=0;\\s*i<\\w+;\\s*i\\+\\+\\s*\\)\\s*{\\s*\\w+\\+=String\\['fromCharCode'\\]\\s*\\(\\w+\\['charCodeAt'\\]\\(i\\)\\+2\\)\\s*}\\s*\\w+=this\\['unescape'\\]\\(\\w+\\);\\s*this\\['eval'\\]\\(\\w+\\);" 
  },
  sig_302={
    "<script>var a='';setTimeout\\(\\d+\\);function setCookie\\(a,b,c\\){var d=new Date;d\\.setTime\\(d\\.getTime\\(\\)\\+60\\*c\\*60\\*1e3\\);var e=\"expires=\"\\+d\\.toUTCString\\(\\);document\\.cookie=a\\+\"=\"\\+b\\+\"; \"\\+e}function getCookie\\(a\\){for\\(var b=a\\+\"=\",c=document\\.cookie\\.split\\(\";\"\\),d=0;d<c\\.length;d\\+\\+\\){for\\(var e=c\\[d\\];\" \"==e\\.charAt\\(0\\);\\)e=e\\.substring\\(1\\);if\\(0==e\\.indexOf\\(b\\)\\)return e\\.substring\\(b\\.length,e\\.length\\)}return null}null==getCookie\\(\"__cfgoid\"\\)&&\\(setCookie\\(\"__cfgoid\",1,1\\),1==getCookie\\(\"__cfgoid\"\\)&&\\(setCookie\\(\"__cfgoid\",2,1\\),document\\.write\\('<script type=\"text/javascript\" src=\"' \\+ '.*?jquery\\.min\\.php'\\s*\\+\\s*'\\?key=\\w+'\\s*\\+ '&utm_campaign='\\s*\\+\\s*'\\w+'\\s*\\+\\s*'&utm_source='\\s*\\+\\s*window\\.location\\.host\\s*\\+\\s*'&utm_medium='\\s*\\+\\s*'&utm_content=' \\+ window\\.location \\+ '&utm_term='\\s*\\+\\s*encodeURIComponent\\(\\(\\(k=\\(function\\(\\){var keywords\\s*=\\s*'';var metas =\\s*document\\.getElementsByTagName\\('meta'\\);if \\(metas\\)\\s*{for\\s*\\(var x=0,y=metas\\.length; x<y; x\\+\\+\\)\\s*{if\\s*\\(metas\\[x\\]\\.name\\.toLowerCase\\(\\)\\s*==\\s*\"keywords\"\\)\\s*\\{keywords \\+= metas\\[x\\]\\.content;\\}\\}\\}return keywords\\s*\\!==\\s*''\\s*\\? keywords\\s*:\\s*null;\\}\\)\\(\\)\\)==null\\?\\(v=window\\.location\\.search\\.match\\(/utm_term=\\(\\[\\^\\&\\]\\+\\)\\/\\)\\)==null\\?\\(t=document\\.title\\)==null\\?'':t:v\\[1\\]:k\\)\\) \\+ '\\&se_referrer='\\s*\\+ encodeURIComponent\\(document\\.referrer\\)\\s*\\+\\s*'\"><'\\s*\\+ '\\/script>'\\)\\)\\);<\\/script>" 
  },
  sig_303={
    "<script\\s*type=\"text/javascript\">var\\s*\\w+\\s*=\\s*new\\s*RegExp\\('\\w+'\\+'=\\(\\[\\^;\\]\\)\\{1,\\}'\\);var\\s*\\w+\\s*=\\s*\\w+\\.exec\\(document\\.cookie\\);if\\(\\w+\\){\\w+\\s*=\\s*\\w+\\[0\\]\\.split\\('='\\);\\w+\\s*=\\s*\\(\\w+\\[1\\]\\s*\\?\\s*\\w+\\[1\\]\\s*:\\s*false\\);}else{\\w+\\s*=\\s*false;}if\\(\\w+\\s*!=\\s*'\\w+'\\){var\\s*\\w+\\s*=\\s*new\\s*Date\\(\\);\\w+\\.setDate\\(\\w+\\.getDate\\(\\)\\+1\\);document\\.cookie\\s*=\\s*'\\w+'\\+'='\\+'\\w+'\\+';\\s*expires='\\+\\w+\\.toUTCString\\(\\);}</script>" 
  },
  sig_304={
    "var\\s*\\w{10,30}=.*\\.charAt\\(\\s*\\w{5,30}\\s*\\)\\s*\\}\\s*eval\\(\\s*\\w{5,30}\\s*\\);" 
  },
  sig_305={
    "var _(\\w+)=\\[\"(\\\\x[a-fA-F0-9\",]+)+\\];document\\[_\\1\\[\\d+\\]]\\((_\\1\\[\\d+\\]\\+?)+\\);" 
  },
  sig_306={
    "document\\[\"\\\\x77\\\\x72\\\\x69\\\\x74\\\\x65\"\\]\\(\"\\\\x3C\\\\x73\\\\x63\\\\x72\\\\x69\\\\x70\\\\x74\\\\x20\\\\x73\\\\x72\\\\x63\\\\x3D\\\\x22[\\\\a-zA-Z0-9]+\\\\x22\\\\x3E\\\\x3C\\\\x2F\\\\x73\\\\x63\\\\x72\\\\x69\\\\x70\\\\x74\\\\x3E\"\\)" 
  },
  sig_307={
    "<script>var\\s*b=\"\\w{1,30}\";\\s*c=\"\\w{1,30}\";\\s*function setCookie\\(\\s*a,b,c\\){\\s*var\\s*d\\s*=\\s*new\\s*Date;d\\.setTime\\(\\s*d\\.getTime\\(\\s*\\)\\s*\\+\\s*\\d+\\s*\\*\\s*c\\s*\\*\\s*\\d+\\s*\\*\\s*\\w{1,30}\\s*\\);\\s*var\\s*e=\"expires=\"\\+d\\.toUTCString\\(\\);\\s*document\\.cookie.+?encodeURIComponent\\(document\\.referrer\\)\\s*\\+\\s*'\">[<script/>'+\\s]+\\)\\)\\);\\s*</script>" 
  },
  sig_308={
    "<script\\s*language=\"JavaScript1\\.1\"\\s*type=\"text/javascript\">\\s*<!--\\s*location\\.replace\\(\"http://vkcomii\\.ru/yandexxx/\"\\);\\s*//-->\\s*</script>\\s*<noscript>\\s*<meta http-equiv=\"Refresh\"\\s*content=\"3;\\s*URL=http://vkcomii\\.ru/yandexxx/\">\\s*</noscript>" 
  },
  sig_309={
    "<script type=\"text/javascript\">\\s*if \\(screen\\.width <= 480\\).+document\\.write\\('<script language=\"javascript\">docu'\\+'ment\\.location=\"http://portal-b\\.pw/XcTyTp\"</s'\\+'cript>'\\)}else{document\\.write\\('\\.'\\)}}R\\(\\);\\s*</script>" 
  },
  sig_31={
    "<\\?php\\s*if\\s*\\(!isset\\(\\$_REQUEST\\['\\w+'\\]\\)\\) header\\(\"HTTP.+?\"\\);\\s*@preg_replace\\('.\\(\\.\\*\\).e',\\s*@\\$_REQUEST\\['\\w+'\\],\\s*''\\);\\s*\\?>" 
  },
  sig_312={ "eval\\(\\$gzc\\(\\$b64\\(\\$r13" },
  sig_313={ "fwrite\\(\\$fp,\"\\\\xEF\\\\xBB\\\\xBF\"\\.\\$body" },
  sig_314={ "@\\$.+?\\(\\$.+?\\(\\$.+?\\(\\$.+?\\(\\$.+?\\)\\)\\)\\);\\?>" },
  sig_315={ "\\$color = \"#df5\";" },
  sig_316={ "\\beval\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[pass" },
  sig_317={ "\\$g___g_=\\$" },
  sig_318={ "code\\.google\\.com/p/b374k-shell" },
  sig_319={ "\\beval\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[.pass.\\]\\);exit\\(\\);" },
  sig_32={
    "<\\?php.+?ERROR-400-BAD-REQUEST.+?echo\\s*\\$_FILES\\['\\w+'\\]\\['\\w+'\\];}\\s*\\?>" 
  },
  sig_320={ "@eval\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[cmd\\]\\)" },
  sig_321={ ";@\\$fun\\(str_rot13\\(" },
  sig_322={ "return RC4::Encryp" },
  sig_323={ "disk_free_space\\(dirname\\(__FILE__\\)\\) , disk_free_space" },
  sig_324={ "\\$p=strpos\\(\\$tx,'{#',\\$p2\\+2" },
  sig_325={ "\\beval\\(.[a-z0-9]+\\[.GLOBALS\\[" },
  sig_326={ "<td>\\$ execute a cmd</td>" },
  sig_327={ "\\.\\$[A-Za-z0-9]+;\\$[A-Za-z0-9]+\\(\\$[A-Za-z0-9]+\\[[\\0-9]+\\]\\." },
  sig_328={
    "DIRECTORY_SEPARATOR\\s*\\.\\s*\\$[A-Za-z0-9]+;\\s*if\\s*\\(@\\$GLOBALS\\['[a-z0-9]+'\\]\\(\\$[A-Za-z0-9]+" 
  },
  sig_329={ "\\); } } else { echo \\$GLOBALS" },
  sig_33={
    "\\$\\w+\\s*=\\s*\"\\w+\";\\s*preg_replace\\(\"(\\\\x?[a-f0-9A-F]{2,3})+\"\\s*,\"(\\\\x?[a-fA-F0-9]+)+\"\\s*,\"(\\\\x?[a-fA-F0-9]+)+\"\\);" 
  },
  sig_330={ "rand_url=\\$target_urls\\[$n" },
  sig_331={ "default_action.*=.*FilesMan" },
  sig_332={ "\\$default_charset.*=\\.*\\$dc\\.\\$dc2" },
  sig_333={ "_POST\\[\\(chr\\(@<d>@\\).chr\\(@<d>@\\)\\)" },
  sig_334={ "\\$k=\"ass\"\\.\"ert\"" },
  sig_335={ "move_uploaded_file/\\*;\\*/" },
  sig_336={ "@array_diff_ukey\\(@array\\(\\(string\\)\\$_REQUEST" },
  sig_337={ "\\beval\\(x\\(\\$x" },
  sig_338={ "\\]\\] = FALSE; } } } } function" },
  sig_339={ "\\]\\(NULL\\);@\\$GLOBALS\\[" },
  sig_34={
    "<\\?php\\s*@eval\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w{1,5}'\\]\\);\\s*\\?>" 
  },
  sig_340={ "LD_PRELOAD=\\./libworker.so" },
  sig_341={ "]}=trim\\(array_pop\\(\\${\\${\"" },
  sig_342={ "POST=WSOstripslashes" },
  sig_343={ "\\$domain = \\$domains\\[array_rand\\(\\$domains, 1\\)" },
  sig_344={ "mt_rand\\(0,count\\(\\$not_android_urls\\)-1\\)" },
  sig_345={ "}\\)\\);}return\\${\\${" },
  sig_346={ ";foreach\\(\\${\\${" },
  sig_347={ "\\+\\+\\$o\\]\\]\\)\\);}}eval\\(\\$d\\)" },
  sig_348={ "base.\\.\\(32\\*2\\)" },
  sig_349={ "eval\\(gzuncompress\\(base64_decode\\(.eN" },
  sig_35={ "\\$(\\w+)=\"[\\w\\+\\=/]+\";eval\\(base64_decode\\(\\$\\1\\)\\);" },
  sig_350={ "fopo\\.com\\.ar" },
  sig_351={
    "\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[.p1.\\]\\); else @unlink\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[" 
  },
  sig_352={ "\\)\\) { echo PHP_OS\\.\\$" },
  sig_353={ "\\]\\(\"\",@\\$_COOKIE\\[" },
  sig_354={ "=@\\$_COOKIE;\\$" },
  sig_355={ "eval\\(base64_decode\\(\\$o\\)\\); \\?>" },
  sig_356={ "\\$_COOKIE=WSOstripslashes" },
  sig_357={ "=trim\\(array_pop\\(\\${\\$" },
  sig_358={ "=num_macros\\(\\${\\${" },
  sig_359={ "stripslashes\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"l1\"\\]\\);\\$" },
  sig_36={
    "<\\?php\\s+function (\\w+)\\(\\$file_url,.+?file_get_contents\\(\\$file_url\\);\\1\\('https://dl\\.dropboxusercontent\\.com.+?unlink\\(\\$_SERVER\\['SCRIPT_FILENAME'\\]\\);\\?>" 
  },
  sig_360={ "= stripslashes\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"user_l\"\\]\\)" },
  sig_361={ "mail\\(stripslashes\\(\\$strr1\\), stripslashes\\(\\$strr2\\)" },
  sig_362={
    "implode\\(\".r.n\",array\\(\"%1html%3\",\"%1head%3\",head\\(\\),\"%2head%3\",\"%1body%3" 
  },
  sig_363={ "@gzinflate\\(@base64_decode\\(@str_replac" },
  sig_364={ "\\$opt\\(\"/\\d+/e" },
  sig_365={ "gmail-smtp-in.l.google.com,google" },
  sig_366={
    "\\)eval\\(gzuncompress\\(base64_decode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[" 
  },
  sig_367={ "myUpload->uploadFile\\(\\)" },
  sig_368={ "ZGVmYXVsdF91c2VfYWpheCA9IHRydWU" },
  sig_369={ "V1NPc3RyaXBzbGFzaGVz" },
  sig_37={
    "<\\?php\\s+//\\w{0,2100}\\s*eval\\(base64_decode\\(\"[a-zA-Z0-9/\\+=]+\"\\)\\);\\s*\\?>" 
  },
  sig_370={ "\\beval\\(\"return\\s+eval\\(\\\\\\\"\\$code\\\\\\\"\\);\"\\)\\s+\\?>" },
  sig_371={ "<\\?php\\s+eval\\(eval\\(\"\\\\\\$_" },
  sig_372={ "function\\s+WSOsetcookie" },
  sig_373={ "\\beval\\(\\$_g\\(\\$_b\\(\\$_r" },
  sig_374={ "\\$headers\\s*\\.=\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['eMailAdd'\\]" },
  sig_375={ "function xr\\(\\$text,\\$key\\){\\$out=\"\";for\\(\\$i=0;\\$i" },
  sig_376={ "if\\(isset\\(\\$_COOKIE\\[\\'\\w{11}\\'\\]\\X*fsockopen" },
  sig_377={ "\\$user_agent = \"ConBot\";" },
  sig_378={
    "if\\(!empty\\(\\$_FILES\\['\\w*'\\]\\['\\w*']\\)\\sAND\\s\\(md5\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['nick'\\]\\)\\s==\\s'\\w{32}'\\)\\)\\s{" 
  },
  sig_379={ "<title>VaRVaRa Searcher</title>" },
  sig_38={
    "<\\?php\\s*eval\\(\"(\\\\x[A-F0-9]+)+'[a-zA-Z0-9/+=]+'(\\\\x[A-F0-9]+)+\"\\);\\s*\\?>" 
  },
  sig_380={ "fwrite\\(\\$fp,\"\\\\xEF\\\\xBB\\\\xBF\"\\.iconv\\('gbk','utf-8//IGNORE" },
  sig_381={
    "function check_cookie.+?android == true\\)check_cookie\\(\\$android_redirect\\);elseif \\(\\(\\$iphone.+?\\$aaa = false;" 
  },
  sig_382={
    "<\\?php if \\(\\$_FILES\\['F1l3'\\]\\) {move_uploaded_file.+? forbidden!'; } \\?>" 
  },
  sig_383={
    "chmod\\(dirname\\(__FILE__\\).+?{\\$a=Array\\(\"/\\.\\*/e.+?_\\d+\\(\\d+\\)\\);" 
  },
  sig_384={ "if\\(strpos\\('fff'.\\$file,'home'\\)>0 or strpos\\('fff'\\.\\$file,'admin" },
  sig_385={
    "var_dump\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST), \\$_GET, \\$_COOKIE, \\$_FILES\\); \\$output = ob_get_clean\\(\\); \\$fp = fopen" 
  },
  sig_386={ "echo\\swelcome\\sto\\sr57\\sshell" },
  sig_387={ "preg_replace\\(\\\"\\\\043\\\\056\\\\052\\\\043\\\\145" },
  sig_388={ "define\\('ALREADY_RUN_\\w*',\\s1\\);.*eval" },
  sig_389={
    ";file_put_contents\\(\\$_7\\['.'\\]\\['\\w+'\\],\\$_\\d+,FILE_APPEND\\|LOCK_EX\\);}if" 
  },
  sig_39={
    "<\\?php\\s*eval\\(gzinflate\\(base64_decode\\('[a-zA-Z0-9/+=]{5000,}'\\)\\)\\);\\s*\\?>" 
  },
  sig_390={
    "<\\?php\\s+eval/\\*\\*/\\(\"eval\\(gzinflate\\(base64_decode\\(.+?\\)\\)\\);\"\\);\\s*\\?>" 
  },
  sig_391={
    "if \\(md5\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['multipart'\\]\\)==\\$multipart\\.\\$part\\){" 
  },
  sig_392={
    "\\$\\w+\\s*=\\s*system\\(\"wget\\s+http:.+?;chmod\\s+\\d+\\s+\\w+;\\./\\w+\"\\);\\s*echo\\s+\\$\\w+;\\s*system\\(\"rm\\s+\\w+\"\\);" 
  },
  sig_393={ "\\$imgData\\s*=\\s*@\\$p2\\(@\\$p1\\(\\$imgData\\)\\);" },
  sig_394={ "code';\\$vvv=\\$vvv\\(str_replace\\(\"\\\\n\"" },
  sig_395={
    "\\$\\w+\\s*=\\s*\\w+;\\$\\w+\\s*=\\s*\"\\w+\";\\$\\w+\\s*=\\s*true;\\$\\w+\\s*=\\s*\\d+;\\$\\w+\\s*=\\s*true;\\$\\w+\\s*=\\s*false;\\$\\w+\\s*=\\s*implode" 
  },
  sig_396={
    "function\\s+Check25Port\\(\\)\\s*{\\s*\\$res\\s*=\\s*TRUE;\\s*\\$s\\s*=\\s*socket_create\\(AF_INET,\\s*SOCK_STREAM,\\s*SOL_TCP\\);\\s*if\\(@socket_connect\\(\\$s,\\s*'gmail-smtp-in" 
  },
  sig_397={
    "<\\?php\\s*\\$target_urls\\s*=\\s*array.+?\\$n\\s*=\\s*mt_rand\\(0,count\\(\\$target_urls\\)-1\\);.+?\\$rand_url;\\?>\\s*\">\\s*" 
  },
  sig_398={
    "==\\s*FALSE\\)\\s*{\\s*break;\\s*}\\s*if\\s*\\(\\$\\w+\\s*==\\s*\\d+ \\|\\|\\s*\\$\\w+\\s*===\\s*\\d+\\s*\\|\\|\\s*\\$\\w+\\s*===\\s*\\d+\\s*\\)\\s*{\\s*\\$\\w+\\[\\$\\w+\\]\\[" 
  },
  sig_399={
    "=\\s*\\$\\w+\\(\"\",\\s*\\$\\w+\\(\\$\\w+\\(\"\\w+\",\\s*\"\",\\s*\\$\\w+\\.\\$\\w+\\.\\$\\w+\\.\\$\\w+\\)\\)\\);\\s*\\$\\w+\\(\\);\\s*\\?>" 
  },
  sig_4={ "<\\?php\\s+\\$GLOBALS.+?\\[\\d+\\]\\]\\);}exit\\(\\);\\}\\s+\\?>" },
  sig_40={
    "<\\?php\\s*\\$\\w+='###.+\\'\\)\\);';\\s*\\$\\w+=str_replace\\('#', '', \\$\\w+\\);\\$\\w+=create_function\\('',\\$\\w+\\);\\$\\w+\\(\\);\\s*\\?>" 
  },
  sig_400={
    "=Array\\(\".+?\"=>@phpversion\\(\\),\"sv\"=>\".+?\",\\);echo\\s*@serialize\\(\\${\\${" 
  },
  sig_401={ "<\\?php\\s*\\$a\\s*=\\s*\".+?\";\\s*assert\\(\\$a\\(" },
  sig_402={
    "}\\s*function [a-z0-9]{10,}\\(\\){}\\sfunction [a-z0-9]{10,}\\(\\){\\$.+?array\\(\\d+,\\d+" 
  },
  sig_403={
    ";eval\\(base64_decode\\(gzuncompress\\(base64_decode\\(\\$\\w+\\)\\)\\)\\);\\?>" 
  },
  sig_404={
    "if\\s*\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[str_rot13\\(pack\\(\"H\\*\",\\s*\"\\w+\"\\)\\)]\\)\\)\\s*{\\$_" 
  },
  sig_405={ "<\\?php\\s*eval\\(gzinflate\\(base64_decode\\(\"DZ.+\\)\\)\\);\\s*\\?>" },
  sig_406={ "<\\?php\\s*\\$(\\w+)=\".+?\";eval\\(\\$\\1\\(\"[\\w+/=]+\"\\)\\);" },
  sig_407={
    "charCode\\(\\$\\w+\\);\\s*function charCode\\(\\$\\w+\\){\\s*\\$\\w+\\s*=\\s*array\\(.+?\\$\\w+\\[\\d+\\];return\\s+EvAl\\(\\$\\w+\\);\\s*}" 
  },
  sig_408={ "@\\$\\w+\\('##e'\\s*,\\s*\"\\\\x[^\"]+\"\\s*,\\s*''\\);" },
  sig_409={ "<\\?php\\s*\\$r6\\s*=\\s*pow\\(2,6\\);\\s*\\$stringus\\s*=" },
  sig_41={
    "<\\?php\\s*\\$_\\w+=\"(\\\\x[A-Fa-f0-9]+)+\";\\$_\\w+\\(\"(\\\\x[a-fA-F0-9]+)+\",\".+?\",'\\.'\\);\\?>" 
  },
  sig_410={ "\\$sh_name = base64_decode\\(\\$sh_id\\)\\.\\$sh_ver;" },
  sig_411={
    "\\[3ran\\]\\s*\\*/\\s*if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"mailto\"\\]\\)\\)" 
  },
  sig_412={
    "global \\$\\w+; \\$\\w+=array\\('\\$\\w+\\[0\\]=array_pop\\(\\$\\w+\\);\\$\\w+=\\w+\\(\\d+,\\d+\\);.+create_';if\\(function_exists.+?\\)\\);};unset\\(\\$\\w+\\);" 
  },
  sig_413={ "\\$i=strpos\\(\\$im,'\\[UPD_CONTENT-'\\);" },
  sig_414={ "<title>Sender Anonym Email :: FLoodeR :: SpameR</title>" },
  sig_415={
    "print\\s*\"<h1>\\w+</h1>\\\\n\";\\s*echo \"Your IP: \";\\s*echo \\$_SERVER\\['REMOTE_ADDR'\\];.+move_uploaded_file.+touch.+</html>" 
  },
  sig_416={
    "<\\?\\s*function _\\d+\\(\\$i\\){\\$\\w+=Array\\(('[a-zA-Z0-9/\\+=]+',?)+\\);.+\\$default_action=.+\\)\\)\\)\\); \\?>" 
  },
  sig_417={
    "<\\?\\s*function _\\d+\\(\\$i\\){\\$\\w+=Array\\(.+?wso_version.+eval\\(gzuncompress\\(base64_decode\\(.+=='\\)\\)\\);\\s*\\?>" 
  },
  sig_418={ "class\\s+PlgSystemXcalendarJoomlaBase" },
  sig_419={ "\\$sess = md5\\(@\\$_COOKIE\\[ssid\\]\\);" },
  sig_42={ "<\\?\\s*\\$_=\"\";\\$_\\[\\+\"\"\\]='';\\$_=\"\\$_.*\\$_}\\['__'\\]\\);\\?>" },
  sig_420={
    "<\\?php\\s+\\$\\w+='base.+?\\(str_replace\\(.+@setcookie\\('\\w+',\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST).+</form>" 
  },
  sig_421={
    "\\$GLOBALS\\['_\\d+_'\\]\\[\\d+\\]\\(\\$default_charset,\\$auth_pass,\\$color,\\$default_charset\\)" 
  },
  sig_422={
    "<\\?php\\s*(\\$\\w+\\s*=\\s*stripslashes\\(base64_decode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\);\\s*)+\\$\\w+\\s*=\\s*mail\\(stripslashes\\(\\$\\w+\\),\\s*stripslashes\\(\\$\\w+\\),\\s*stripslashes\\(\\$\\w+\\),\\s*stripslashes\\(\\$\\w+\\)\\);\\s*if.+?{echo\\s*'\\w+ :\\s*'\\s*\\.\\s*\\$ejfkqexizs;}" 
  },
  sig_423={
    "<\\?php\\s*echo md5\\(\\d+\\)\\.\"<form\\s*method=post\\s+enctype=multipart/form-data><input type=file name=filename>" 
  },
  sig_424={
    "return\\s*\\$c0\\[\\$c_\\];}\\$GLOBALS\\['_\\d+_'\\]\\[0\\]\\(a_\\(0\\),a_\\(1\\),a_\\(2\\)\\);\\s*}" 
  },
  sig_425={
    "<\\?php if\\(empty\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['ineedthispage'\\]\\)\\){ini_set\\('display_errors',\"Off\"\\);ignore_user_abort\\(1\\)" 
  },
  sig_426={
    "\\$file_path = dirname\\(__FILE__\\) \\. '/' \\. basename\\(\\$_FILES\\[\"file\"\\]\\[\"name\"\\]\\);\\s*move_uploaded_file\\(\\$_FILES\\[\"file\"\\]\\[\"tmp_name\"\\], \\$file_path\\);\\s*\\$file_name = basename\\(\\$file_path\\);" 
  },
  sig_427={
    "\\$return = smtpmail\\(\\$smtp_host, \\$smtp_port, \\$smtp_login, \\$smtp_passw, \\$email_polucha, \\$telo_pisma, \\$headers\\);" 
  },
  sig_428={
    "<\\?\\s*\\$lin=\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"link\"\\];\\s*switch \\(\\$lin\\) {" 
  },
  sig_429={
    "\\$wp_enc_file\\s*=\\s*'<\\?php eval\\(\"\\?>\"\\s*\\.\\s*base64_decode\\(\"'\\.\\$wp_code\\.'\"\\)\\);\\s*\\?>';" 
  },
  sig_43={
    "<\\?php\\s*if\\(preg_match\\((chr\\(\\d+\\)\\.)+.+if\\(\\$\\w+===false\\)continue;\\$\\w+=str_repeat.+base64_decode\\('[a-zA-Z0-9\\+/=]+'\\),.+?\\$\\w+\\)\\);break;}}}\\?>" 
  },
  sig_430={
    "if \\(strpos\\(\\$check_result,'BLOCK'\\) !== false \\|\\| strpos\\(\\$check_result,'FOUND'\\) !== false \\|\\| strpos" 
  },
  sig_431={
    "<\\?php\\s*error_reporting\\(0\\);\\s*ignore_user_abort\\(true\\);\\s*set_time_limit\\(0\\);\\s*function str_1replace\\(\\$needle" 
  },
  sig_432={ "riny\\(tmhapbzcerff\\(onfr64_qrpbqr\\('" },
  sig_433={
    "GLOBALS\\['_\\d+_'\\]\\[1\\]\\(\\$_REQUEST,\\$_1\\);\\$_1=\\$GLOBALS\\['_\\d+_'\\]\\[\\d+\\]\\(\\);while\\(round\\([^\\)]+\\)-round\\([^\\)]+\\)\\)\\$GLOBALS\\['_\\d+_'\\]\\[\\d+]\\(\\$_REQUEST,\\$_REQUEST\\);.+echo \\$_\\d+;\\s*\\?>" 
  },
  sig_434={
    "\\$folder = str_replace\\(\\$_SERVER.+ http_build_query.+function http_parse_headers.+\\$s \\.= \\$m\\[2\\]\\.\\$m\\[3\\];" 
  },
  sig_435={
    "file_put_contents\\(str_rot13\\(\"[^\"]+\"\\), file_get_contents\\(str_rot13\\(\"uggc://[^\"]+\"\\)\\)\\);.+@\\$strings\\(str_rot13\\(file_get_contents" 
  },
  sig_436={
    "<html>.+?<title>utf</title>.+?echo \"<h1>Welcome</h1>\\\\n\";.+touch\\(\\$newfile,\\s*\\$time\\);.+</html>" 
  },
  sig_437={
    "if\\s*\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['check'\\]\\)\\) {\\s*echo \"ok\";\\s*}\\s*else\\s*{\\s*if\\(\\s*\\$curl\\s*=\\s*curl_init\\(\\)\\s*\\)\\s*{" 
  },
  sig_438={ "preg_replace\\(\\$\\w+.\\$\\w+,\"(\\\\x[a-zA-Z0-9]+){3,}.{3000,}\\?>" },
  sig_439={
    "}exit;if\\(\\$GLOBALS\\['_\\d+_'\\]\\[\\d+\\]\\(_\\d+\\(\\d+\\),_\\d+\\(\\d+\\)\\)!==false\\)\\$GLOBALS\\['_\\d+_'\\]\\[\\d+\\]\\(\\$_\\d+,\\$_\\d+\\);}else{\\$GLOBALS\\['_\\d+_'\\]\\[\\d+\\]\\(_\\d+\\(\\d+\\)\\);echo _\\d+\\(\\d+\\);}exit;}\\s*\\?>\\s*$" 
  },
  sig_44={
    "<\\?php\\s*\\$\\w+=\".+\";eval/\\*\\*/\\(strrev\\(str_replace\\(\".\",\"\",\\$\\w+\\)\\)\\);\\?>" 
  },
  sig_440={ "else\\s*{\\s*ClosingDelicateHolesButton\\s*(\\s*);\\s*}" },
  sig_441={
    "(\\$\\w+ = (\\d+|\"\\w+\"|\"\"|true|false);){5}\\$\\w+ = \"base64_decode\";(\\$\\w+ = (\\d+|\"\\w+\"|\"\"|true|false);){5}" 
  },
  sig_442={
    "\\$\\w+\\.=\\$\\w+\\(\\$\\w+\\(\\$\\w+\\)&0xFF\\);}eval\\(\\$\\w+\\);exit\\(\\);\\s*$" 
  },
  sig_443={
    "}elseif\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['eval'\\]\\){\\s*\\$qV\\s*=\\s*\"stop_\";" 
  },
  sig_444={
    "<\\?php\\s*\\$\\w+=([.]?chr\\(\\d+\\)[.;])+\\s*eval\\(\\$\\w+\\(\\$_REQUEST\\[\\w+\\]\\)\\);\\s*$" 
  },
  sig_445={
    "\"User-Agent:\\s*\\$template\"\\s*\\.\"\\|\\$pathToDor\"\\s*\\.\"\\|\\$pathOnMyHost\"" 
  },
  sig_446={
    "<\\?php\\s*if\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\){echo\\s*'\\w+';\\s*}\\s*else\\s*{\\s*\\(\\s*\\$www=\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\s*&&\\s*@preg_replace\\(\\s*'/ad/e'\\s*,\\s*'@'\\s*\\.\\s*str_rot13\\(\\s*'riny'\\s*\\)\\s*\\.\\s*'\\(\\s*\\$www\\s*\\)'\\s*,\\s*'add'\\s*\\);\\s*}\\s*$" 
  },
  sig_447={
    "\\$GLOBALS\\['\\w+'\\]\\(\\$\\w+\\[(\\$\\w\\d+\\[\\d+\\]\\.)+\\$\\w\\d+\\[\\d+\\]\\]\\);\\s*\\$\\w+\\+\\+\\)\\s*{\\s*\\$\\w+\\s*=\\s*array\\(\\s*(\\$\\w\\d+\\[\\d+\\]\\.?)+\\s*=>\\s*\\$\\w+,\\s*(\\$\\w\\d+\\[\\d+\\]\\.?)+\\s*=>\\s*\"\"," 
  },
  sig_448={ "\\$a\\s*=\\s*[\\.\\sbase64decode_\"]+;\\s*assert\\(\\$a\\(" },
  sig_449={
    "<\\?php if\\(md5\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"ms\\-load\"\\]\\)==\".{32}\"\\){\\s*\\$p=\\$_SERVER\\[\"DOCUMENT_ROOT\"\\];" 
  },
  sig_45={
    "\\$_REQUEST\\[.\\]\\s*\\?\\s*eval\\(\\s*base64_decode\\(\\s*\\$_REQUEST\\[.\\]\\s*\\)\\s*\\)\\s*:\\s*exit;" 
  },
  sig_450={
    "if \\(md5\\(\\$_COOKIE\\['i'\\]\\) == '.{32}'\\)\\s*{\\s*function file_download\\(\\$filename, \\$mimetype='application/octet-stream'\\) {" 
  },
  sig_451={ "<title>Bypass shell</title>" },
  sig_452={ "if \\(\\$whattime == \"goodtime\" \\|\\| empty\\(\\$settings\\)\\) {" },
  sig_453={
    "if\\s*\\(isset\\(\\${\\$s\\d+}\\['\\w+'\\]\\)\\)\\s*{\\s*eval\\(\\${\\$s\\d+}\\['\\w+'\\]\\);\\s*}\\s*}elseif\\(isset" 
  },
  sig_454={ "\\.(chr\\(\\d+\\)\\.\"[a-zA-Z0-9/\\\\ ]*?\"\\.){5,}" },
  sig_455={
    "<\\?php\\s*if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\w+\\]\\)\\)\\s*{\\$\\w+=\"[^\"]{2000,}\";" 
  },
  sig_456={ "file_put_contents\\('\\w+','<\\?php '\\.\\$m\\);include\\('\\w+'\\);\\s*$" },
  sig_457={ "<\\?php\\s+\\$Class_WP_Index\\s*=\\s*\"" },
  sig_458={
    "\\$mcdata=array\\(\\);\\$mcdata\\[0\\]=base64_decode\\('[.a-zA-Z0-9_/=]+'\\);array_filter\\(\\$mcdata, base64_decode\\('[.a-zA-Z0-9_/=]+'\\)\\);\\?>" 
  },
  sig_459={
    "class PluginJoomla.+\\$\\w+\\s*=\\s*@?\\$_COOKIE\\[\\'\\w+\\'\\]\\;\\s*.*\\$\\w+\\s*=\\$\\w+\\(@?\\$_COOKIE\\[\\'\\w+\\']\\)\\;.+\\$\\w+\\s*=\\s*new\\s*PluginJoomla;" 
  },
  sig_46={
    "<\\?php\\s*\\$v\\w+\\s*=\\s*Array\\(.+sprintf\\(v\\w+\\(\\$\\w+,\\s*\\$v\\w+\\)\\);\\?>" 
  },
  sig_460={
    "\\$GLOBALS\\['\\w+'\\]=Array\\((base64_decode\\((\\'[A-Za-z0-9\\/\\=]*\\'\\s*\\.*)+\\)\\,*)+.*?\\[\\d+\\]\\(\\$_\\w+\\),\\$GLOBALS\\['\\w+'\\]\\[\\d+\\]\\(\\$\\w+\\)\\);}\\s*\\?>" 
  },
  sig_461={ "//PING.*?sav.php\\?p=\\w+&url=.*?\\@ob_start\\(\\'obCacheStart\\'\\);" },
  sig_462={
    "echo \"<html><head>.*?Safe mode:</span>.*?\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\'\\w+\\'\\]\\s*===\\s*\\'uploadFile\\'\\).*?echo \\\"</body><\\/html>\\\";" 
  },
  sig_463={
    "<\\?php.*?\\$[Oo0_]*=urldecode\\((.*?;\\${\".*?}\\[\".*?\\]\\(\\)\\;).*<\\/body>\\\\.*?\\?>" 
  },
  sig_464={
    "<\\?php\\s+mb_http_input\\(\"iso-8859-1\"\\);.*?B L E S S E D S I N N E R .*?<\\/DIV>.+<\\/div>.+?<\\/form>" 
  },
  sig_465={
    "else\\s*{\\s*\\$result\\s*=\\s*mail\\(stripslashes\\(\\$to\\)\\s*,\\s*stripslashes\\(\\$subject\\)\\s*,\\s*stripslashes\\(\\$message\\)\\);\\s*}\\s*if\\(\\$result\\){echo\\s*'good';}else{echo\\s*'error\\s*:\\s*'\\.\\$result;}" 
  },
  sig_466={ "if\\(\\$vas\\){echo\\s*'lessloss';}else{echo\\s*'cramdam\\s*:\\s*'.\\$v" },
  sig_467={
    "\\$message\\s*=\\s*stripslashes\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"body\"\\]\\);\\s*set_time_limit\\s*\\(\\d+\\);\\s*@ignore_user_abort\\s*\\(true\\);\\s*\\$to_ar = explode\\(',',\\s*\\$to\\);" 
  },
  sig_468={
    "\\$result\\s*=\\s*mail\\(stripslashes\\(\\$\\w+\\),\\s*stripslashes\\(\\$\\w+\\),\\s*stripslashes\\(\\$\\w+\\)\\);\\s*if\\(\\$result\\){\\s*echo\\s*'\\w+';\\s*}else{\\s*echo\\s*'\\w+\\s*:\\s*'\\s*\\.\\s*\\$result;\\s*}" 
  },
  sig_469={
    "<?php\\s*(\\$\\w+\\s*=\\s*stripslashes\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"\\]\\);\\s*){3,}\\$result\\s+=\\s+mail\\(stripslashes\\(\\$\\w+\\),\\s*stripslashes\\(\\$\\w+\\),\\s*stripslashes\\(\\$\\w+\\)\\);\\s*if\\(\\$result\\){echo 'thank you';}" 
  },
  sig_47={
    "\\s*if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\w+\\]\\)\\){\\s*\\$d=substr\\(8,1\\);foreach\\(array\\((\\d+,)+.+?eval\\(\\$d\\);\\s*}" 
  },
  sig_470={
    "if \\(!empty \\(\\$hostname\\) \\) {\\s*\\$output = \"\";\\s*@exec \\(\"nslookup\\.exe -type=MX \\$hostname\\.\", \\$output\\);\\s*\\$imx=-1;" 
  },
  sig_471={
    "if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"\\]\\)\\)\\s*\\$to = \\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"\\];\\s*else\\s*{\\s*echo \"404 error\";\\s*exit;\\s*}\\s*if\\(\\s*\\$curl\\s*=\\s*curl_init\\(\\)\\s*\\)\\s*{" 
  },
  sig_472={
    "@move_uploaded_file.+?echo\"<center><b>Done ==> \\$userfile_name</b></center>" 
  },
  sig_473={
    "<\\?php\\s*\\$\\w+\\s*=\\s*stripslashes\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\);\\s*\\$\\w+\\s*=\\s*stripslashes\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\);\\s*\\$\\w+\\s*=\\s*stripslashes\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\);\\s*\\$\\w+\\s*=\\s*mail\\(stripslashes\\(\\$\\w+\\),\\s*stripslashes\\(\\$\\w+\\),\\s*stripslashes\\(\\$\\w+\\)\\);\\s*if\\(\\$\\w+\\){echo\\s*(trim\\()?'\\w+'\\)?;}\\s*else\\s*{echo\\s*'\\w+\\s*:\\s*'\\.\\$\\w+;}" 
  },
  sig_474={
    "(mail\\((\\$\\w+,\\s*\\$\\w+,\\s*\\$\\w+(,\\s*\\$header)?\\))|(stripslashes\\(\\$\\w+\\),\\s*stripslashes\\(\\$\\w+\\),\\s*stripslashes\\(\\$\\w+\\)\\));(\\s*})?)\\s*if\\(\\$\\w+\\)\\s*{\\s*echo\\s*'\\w+';\\s*}\\s*else\\s*{\\s*(echo)?\\s*'\\w+\\s*:\\s*'" 
  },
  sig_475={
    "if\\(smtp_mail\\(\\$email,\\s*\\$subject,\\s*\\$message,\\s*\\$Header,\\s*\\$from_email\\)\\)\\s*{\\s*echo\\s*\"good\";\\s*}\\s*else\\s*{\\s*echo\\s*\"Some error occured\";\\s*}\\s*\\?>" 
  },
  sig_476={
    "GIF89A;<\\?php\\s*if\\(!function_exists\\(\"\\w+\"\\)\\){function\\s*\\w+\\(\\$\\w+\\){\\$\\w+=base64_decode" 
  },
  sig_477={ "';@\\$\\w+\\([eval\\.\"]+\\('(\\\\x[a-fA-F0-9]+)+'\\);\"\\);\\s*$" },
  sig_478={
    "\\$result\\s*=\\s*dns_get_record\\(\\s*\\$domain\\s*\\.\\s*\"\\.multi\\.uribl\\.com\"\\);" 
  },
  sig_479={ "<title>Hacked by NG689Skw</title><center>" },
  sig_48={
    "\\$urls\\s*=\\s*array\\s*\\(.+?\\);\\s*\\$URL\\s*=\\s*\\$urls\\[rand\\(0,\\s*count\\(\\$urls\\)\\s*-\\s*1\\)\\];\\s*header\\s*\\(\"Location:\\s*\\$URL\"\\);" 
  },
  sig_480={
    "\\$fullurl\\s*=\\s*\"\"\\s*\\.\\s*\\$\\w+\\s*\\.\\s*\"<br><p>Emails:<br><TEXTAREA.*?>\"\\s*\\.\\s*\\$\\w+\\s*.\\s*\"</TEXTAREA></p><p>Engenharia:<br><TEXTAREA.*?>\"\\s*\\.\\s*\\$mensagem\\[\\d+\\].\"</TEXTAREA>" 
  },
  sig_481={ "<title>Pro Mailer V2</title>" },
  sig_482={
    "Array\\(('.'=>'.'\\s*,?\\s*)+\\);\\s*eval/(\\*\\w+\\*/)?\\(\\s*\\w+\\s*\\(\\s*\\$\\w+\\s*,\\s*\\$\\w+\\s*\\)\\s*\\);\\s*\\?>\\s*$" 
  },
  sig_483={
    "touch\\(dirname\\(__FILE__\\),\\s*\\$time\\);\\s*touch\\(\\$_SERVER\\['SCRIPT_FILENAME'\\],\\s*\\$time\\);\\s*chmod\\(\\$_SERVER\\['SCRIPT_FILENAME'\\],\\s*0\\d+\\);\\s*eval\\(base64_decode\\(" 
  },
  sig_484={
    "if\\s*\\(\\$upload\\s*&&\\s*\\$user\\s*&&\\s*wp_check_password\\(\\$passwd,\\s*\\$user->data->user_pass,\\s*\\$user->ID\\)\\s*&&\\s*is_super_admin" 
  },
  sig_485={ "<\\?php\\s*eval\\(gzinflate\\(base64_decode\\(\"DZZH" },
  sig_486={
    ";\\s*eval\\(\\$GLOBALS\\['\\w+'\\]\\[\\d+\\]\\(\\$GLOBALS\\['\\w+'\\]\\[\\d+\\]\\(\\$\\w+\\)\\)\\);\\?>" 
  },
  sig_487={
    "error_reporting\\(round\\(\\d+\\)\\);mb_internal_encoding\\(_\\w+\\(\\d+\\)\\);mb_regex_encoding\\(_\\w+\\(\\d+\\)\\);mb_http_output\\(_\\d+\\(\\d+\\)\\);mb_http_input" 
  },
  sig_488={
    ",\\$\\w+\\s*,\\$\\w+\\s*\\);\\$\\w+\\s*=\\$\\w+\\s*\\([\"'].+?[\"'],\\$\\w+\\s*,\\$\\w+\\s*\\);\\$\\w+\\s*=\\$\\w+\\s*\\(\\$\\w+\\s*,\\$\\w+\\s*\\);\\$\\w+\\s*\\(\\$\\w+\\s*\\);\\s*\\?>" 
  },
  sig_489={
    "\\$\\w+\\s*=\\s*1;\\s*\\$\\w+\\s*=\\s*get_userdata\\(\\$\\w+\\);\\s*\\$\\w+\\s*=\\s*\\$\\w+->\\w+;\\s*wp_set_current_user\\(\\$\\w+\\s*,\\s*\\$\\w+\\);\\s*wp_set_auth_cookie\\(\\$\\w+\\);\\s*do_action\\('wp_login'\\s*,\\s*\\$\\w+\\);" 
  },
  sig_49={
    "<\\?\\s*set_time_limit\\(\\d+\\);\\s*error_reporting\\(0\\);\\s*\\$\\w+\\s+=\\s*'.+';\\s*eval\\(gzinflate\\(str_rot13\\(base64_decode\\(\\$rhs\\)\\)\\)\\)\\;\\s*\\?>" 
  },
  sig_490={ "<\\?\\s*eval\\(gzuncompress\\(base64_decode\\('.{22000,25000}'\\)\\)\\);" },
  sig_491={
    "function\\s*\\w+\\(\\$\\w+,\\$\\w+,\\$\\w+\\)\\{\\$\\w+=\"base64_decode.*?curl_init.*?\"/wp\\\\-login\\\\\\.php.*?/\\\\/administrator\\\\//\".*?phpmyadmin/i\".*?define\\(\"___NOTSELF___',1\\);.*?include_once.*?\\*/\\}\\}\\}" 
  },
  sig_492={
    "php_uname()\\(\\)\\.'<br></b>';\\s*echo\\s*'<form action=\"\" method=\"post\" enctype=\"multipart/form-data\" name=\"uploader\" id=\"uploader\">'.*?if\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['_upl'\\]\\s*==\\s*\"Upload\"\\s*\\)\\s*\\{\\s*if\\(\\@copy\\(\\$_FILES\\['file'\\]\\['tmp_name'\\],\\s*\\$_FILES\\['file'\\]\\['name'\\]\\)" 
  },
  sig_493={
    "(\\$\\w+\\d+\\{\\d+\\}\\s*\\.?\\s*){10,}.+?\\$\\w+\\s*=\\s*\\$\\w+\\(\\s*\"\"\\s*,\\s*\\$\\w+\\(\\$\\w+\\s*\\(\\s*array\\(\\$\\w+\\d+\\{\\d+\\}\\s*,\\s*\"\\\\n\"\\),\\s*\"\",\\s*\".+?\\)\\s*\\)\\s*\\);\\s*\\$\\w+\\(\\);" 
  },
  sig_494={
    "eval\\(stripslashes\\(array_pop\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\)\\)\\)" 
  },
  sig_495={
    "assert_options\\(ASSERT_WARNING,0\\);\\s*\\$\\w+='[0-9a-fA-F]+';.*?chr\\(hexdec.*?\\$\\w+='eval\\(\\$\\w+\\)';\\s*assert\\(\\$\\w+\\);" 
  },
  sig_496={
    "\\$\\w+\\s*=\\s*Array\\(('\\w'=>'\\w',?\\s*)+\\);\\s*eval\\(\\s*\\w+\\(\\$\\w+\\s*,\\s*\\$\\w*\\)\\);\\?>" 
  },
  sig_497={
    "<\\?php\\s*error_reporting\\(0\\);\\s*if \\(!isset\\(\\$_COOKIE\\['\\w+'\\]\\)\\)\\s*deny\\(\\);\\s*\\$cookieData" 
  },
  sig_498={
    "error_reporting\\(0\\);\\s*\\$\\w+\\s*=\\s*\"\\w{32}\";\\s*\\$\\w+\\s*=\\s*basename\\(__FILE__\\);\\s*\\$\\w+\\s*=\\s*\"\\w{32}\";\\s*if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\)\\s*{if\\(strlen\\(\\$\\w+\\)\\s*==\\s*32\\)" 
  },
  sig_499={
    "\\$login=\"\\w+\";\\s*\\$\\w+=str_rot13\\(\"\\w+\"\\);\\s*\\$\\w+\\s*=\\s*str_rot13\\('\\w+'\\);\\s*\\$\\w+=\"\\w{32}\";\\s*eval\\(\\$mdh\\(\\$md\\(strrev\\('" 
  },
  sig_5={ "<\\?php\\s*\\@preg_replace\\(./\\(\\.\\*\\)/e.+?, ..\\); <?php" },
  sig_50={
    "\\$\\w+\\s*=\\s*@\\$_COOKIE\\['\\w+'\\];\\s*if\\s*\\(\\$\\w+\\) {\\s*\\$\\w+\\s*=\\s*\\$\\w+\\(@\\$_COOKIE\\['\\w+'\\]\\);\\s*\\$\\w+=\\$\\w+\\(@\\$_COOKIE\\['\\w+'\\]\\); \\$\\w+\\(\"/\\w+/e\",\\$\\w+,\\w+\\);}" 
  },
  sig_500={ "(/\\*[^\\*]+\\*/\\$\\w+='[^']+';){30,}" },
  sig_501={
    "\\$\\w+\\s*=\\s*'[\\.preg_replace\\']+';\\s*\\$call_user_func_array\\(\\s*\\$\\w+\\s*,\\s*array\\('/\\[\\w+\\]/\\w+'\\s*,\\s*\\$_REQUEST\\[\"\\w+\"\\]\\s*,\\s*\"[^\"]+\"\\s*\\)\\);@\\w+\\(\\s*\\$_REQUEST\\s*\\);" 
  },
  sig_502={
    "([a-zA-Z0-9=/\\\\\\+\\s*]{1,20};){10,}[a-zA-Z0-9=/\\\\\\+\\s*]+\"\\s*\\)\\s*\\)\\s*\\);\\s*\\$\\w+\\(\\);\\s*\\?>" 
  },
  sig_503={
    "\\$\\w+\\s*=\\s*\\$\\w+\\(\\s*\"\"\\s*,\\s*\\$\\w+\\(\\s*\\$\\w+\\(\\s*array\\(\\$\\w+\\{\\d+\\}\\s*,\\s*\"\\\\n\"\\s*\\),\\s*\"\",\\s*\"[^\"]+\"\\s*\\)" 
  },
  sig_504={
    "\\$default_action=[FilesMan'\\.]+;\\s*\\$color=['#df5\\.]+;\\s*\\$default_use_ajax=true;" 
  },
  sig_505={
    "\\$\\w+\\s*=\\s*\\$\\w+\\('.+?'\\);\\s*\\$\\w+=\\$\\w+\\(\\$\\w+\\(\\)\\);\\$\\w+\\(\\);\\s*\\$\\w+\\(\\$\\w+,\"\\$\\w+\\(" 
  },
  sig_506={
    "\\$\\w+\\s*=\\s*'[assert\\'\\.\\s]+';\\s*\\$\\w+\\s*=\\s*sprintf\\('[\\!\\(\\)evalbase64_decode\\s\\.\\']+\\(\"%s\"\\)\\)'\\s*,\\s*\\$\\w+\\);\\s*\\$\\w+\\(stripslashes\\(\\$\\w+\\)\\);" 
  },
  sig_507={
    "<\\?error_reporting\\(0\\);(\\$\\w+=(urldecode\\()?\\$_COOKIE\\['\\w+'\\]\\)?;\\s*){10,}" 
  },
  sig_508={
    "<\\?php\\s*\\$\\w+=strrev\\(\"edoced_46esab\"\\);\\$\\w+=\"[^\"]+\";eval\\(\\$\\w+\\(\\$\\w+\\)\\);\\s*$" 
  },
  sig_509={
    "';\\$\\w+=str_rot13\\('fge_ebg13'\\);\\$\\w+=\\$\\w+\\('onfr64_qrpbqr'\\);\\$\\w+=\\$\\w+\\('.+?'\\);eval\\(\\$\\w+\\(\\$\\w+\\(\\$\\w+\\(\\$\\w+\\)\\)\\)\\)" 
  },
  sig_51={
    "\\$gif=file\\(dirname\\(__FILE__\\)\\.'/images/\\w+\\.gif',2\\);\\$gif=\\$gif\\[\\d+\\]\\(\"\",\\$gif\\[\\d+\\]\\(\\$gif\\[\\d+]\\)\\);\\$gif\\(\\);" 
  },
  sig_510={
    "\\$__=hex2ascii\\(\\$_+\\);\\s*\\$.=\"\\$_+\";\\s*\\$.=[eval\\.\"'\\s]+\\(\\$.\\)';\\s*assert\\(\\$.\\);" 
  },
  sig_511={ "}}else{\\$\\w+=@\\$_SERVER\\[((\"\"|chr\\(\\d+\\)|\"\\w\")\\.?){10,}];" },
  sig_512={
    "if\\s*\\(isset\\(\\$_FILES\\['\\w+'\\]\\['name'\\]\\)\\)\\s*{\\s*\\$abod\\s*=\\s*\\$filedir\\.\\$userfile_name;\\s*@move_uploaded_file\\(\\$userfile_tmp,\\s*\\$abod\\);\\s*echo\"<center><b>Done\\s*==>\\s*\\$userfile_name</b></center>\";\\s*}\\s*}\\s*else{" 
  },
  sig_513={
    "<\\?php\\s*(\\$\\w+) = \\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\];\\s*echo (\\$\\w+) = isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['(\\w+)'\\]\\)\\?base64_decode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\3'\\]\\):'';\\s*(\\$\\w+) = '';\\s*foreach\\(array\\(\\1\\) as (\\$\\w+)\\){\\s*\\4\\.=\\5;\\s*}\\s*ob_start\\(\\4\\);\\s*if\\(\\2\\){\\s*echo \\2;\\s*}\\s*ob_end_flush\\(\\);" 
  },
  sig_514={ "}}}}if\\(\\$q==\"this-is-the-test-of-door\"\\){\\$page=\"DOORWAYISWORKTITLE" },
  sig_515={
    "if\\(\\$\\w+=@gzinflate\\(\\$\\w+\\)\\)\\{if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\)@setcookie\\('\\w+', \\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\);\\$\\w+=create_function" 
  },
  sig_516={
    "\\$\\w+=@gzinflate\\(\\$\\w+\\)\\){\\$\\w+=create_function\\('',\\$\\w+\\);unset\\(\\$\\w+,\\$\\w+\\);\\$\\w+\\(\\);}}" 
  },
  sig_517={
    "\\$\\w+='[^']+'\\^'[^']+';\\s*\\$\\w+='[^']+';\\s*(\\$\\w+='[^']+'|\\$\\w+=\\d+);" 
  },
  sig_518={ "FakeSender by POCT" },
  sig_519={
    "proftpd.+?eval\\(\\$_[GET|POST|COOKIE|REQUEST|SERVER]+\\[['\"]\\w+['\"]\\]\\)" 
  },
  sig_52={
    "<\\?\\s*if \\(\\$_FILES\\['F1l3'\\]\\) {move_uploaded_file.+echo 'You are forbidden!';\\s*}\\s*\\?>" 
  },
  sig_520={
    "<\\?php\\s*if\\s*\\(isset\\s*\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\)\\s*{\\s*\\$\\w+=\"Fil.+?\\s*\\$default_use_ajax\\s*=\\s*true" 
  },
  sig_521={
    "\\$\\w+\\s*=\\s*\"\\w+\";\\s*\\$\\w+\\s*\\s*\\.=\\s*\"\\w+\";\\s*@\\$\\w+\\(\\$\\w+\\(\\'[a-zA-Z0-9/=+]{100,200}" 
  },
  sig_522={ ";eurt\\s*=\\s*xaja_esu_tluafed\\$" },
  sig_523={
    "\\$\\w+\\s*=\\s*\".+?\";\\$\\w+\\s*=(\\s*\\$\\w+\\[\\d+\\]\\s*\\.?){5,};\\$\\w+\\s*=\\s*[\"\\\\a-zA-Z0-9().\\s]+;\\$\\w+\\s*=\\s*\".+?\";\\$\\w+\\s*=\\s*\\$\\w+\\s*\\.\\s*\"\\S{200}" 
  },
  sig_524={ "\\$array\\s*=\\s*base64_decode\\(\\$array\\);\\s*@assert\\(\\$array\\);" },
  sig_525={ "<\\?php\\s*\\$arrId\\s*=\\s*array\\(('\\d+-\\d+',\\s*){20,}" },
  sig_526={ "<title>\\s*Dark Shell\\s*</title>.+clearstatcache" },
  sig_527={
    "str_replace\\('define\\(\"PRENAME\",\"\\d+\"\\);'\\s*,\\s*'define\\(\"PRENAME\"" 
  },
  sig_528={ "\\$Content_mb=getHTTPPage\\(\\$Remote_server\\.\"/index\\.(html|php)\\?host" },
  sig_529={ "\\$weff_client\\s*=\\s*new\\s+TWeffClient\\(\\);" },
  sig_53={
    "if \\(\\$_FILES\\['F1l3'\\]\\) {move_uploaded_file\\(\\$_FILES\\['F1l3'\\]\\['tmp_name'\\], \\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['Name'\\]\\); Exit;}" 
  },
  sig_530={ "\\$data = curl_get_from_webpage_one_time\\(\\$url" },
  sig_531={
    "\\$su\\s*=\\s*str_replace\\('\\$name'\\s*,\\s*\\$n\\s*,\\s*stripslashes\\(\\s*base64_decode\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\s*'z2'\\s*\\]\\s*\\)\\s*\\)\\s*\\);" 
  },
  sig_532={ "__create_initial_settings[^#]+#google.+?__obfuscate_write" },
  sig_533={
    "function\\s*is_from_search\\(\\$http_referer\\)\\s*{\\s*\\$flag\\s*=\\s*false;\\s*\\$http_referer_str\\s*=\\s*'google\\.co\\.jp\\|yahoo\\.co\\.jp\\|bing" 
  },
  sig_534={
    "if\\s*\\(file_exists\\(\\s*\"(\\.\\./)+\\w+\\.php\\.suspected\"\\s*\\)\\s*\\)\\s*rename\\(\\s*\"(\\.\\./)+\\w+\\.php\\.suspected\"\\s*,\\s*\"(\\.\\./)+\\w+\\.php\"\\s*\\);" 
  },
  sig_535={
    "\\$map_index_sitemap_uri\\s*=\\s*sprintf\\('%s%ssitemap_%d\\.xml'\\s*,\\s*\\$currentUrl\\s*,\\s*\"\\$dir/\"\\s*,\\s*\\$sini\\);\\s*\\$map_sitemap_genuri_href\\s*=\\s*sprintf\\(\\s*\\$map_sitemap_genuri_link_format\\s*,\\s*\\$currentUrl" 
  },
  sig_536={
    "function\\s+__get_template\\(\\)\\s*{\\s*\\$root_path\\s*=\\s*__get_root\\(\\);\\s*\\$tpl_path\\s*=\\s*\\$root_path\\.\"/SESS_\\w{32}" 
  },
  sig_537={
    "]}=Array\\(\"\\w+\"=>@phpversion\\(\\),\"\\\\x\\d+\\\\x\\d+\"=>\"\\\\x\\d+\\.\\\\x\\d+-1\",\\);echo\\s*@serialize\\(\\${\\$\\w+\\}\\);}elseif\\(\\${\\${" 
  },
  sig_538={
    "//\\w{3,20}\\s*function\\s+request_url_data\\(\\$url\\)\\s*{\\s*\\$site_url\\s*=.+?print\\s+\\$decoded;\\s*}\\s*}//\\w{3,20}" 
  },
  sig_539={
    "function\\s+evalEndClean\\(&\\s*\\$p\\) {\\s*\\$t = gzuncompress\\(base64_decode\\(\\$p\\)\\);\\s*\\$t = str_replace\\(array\\(\"<\\?php\",'\\?>'\\),'',\\$t\\);\\s*eval\\(\\$t\\);\\s*unset\\(\\$p\\);unset\\(\\$t\\);" 
  },
  sig_54={
    "<\\?php\\s*(if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"]\\)\\)\\s*\\$\\w+ = base64_decode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"\\]\\);\\s*else\\s*{\\s*echo \"indata_error\";\\s*exit;\\s*}\\s*)+if\\(mail\\(\\$\\w+,\\$\\w+,\\$\\w+,\\$\\w+\\)\\)\\s*echo \"sent_ok\";\\s*else\\s*echo \"sent_error\";\\s*\\?>" 
  },
  sig_540={
    "<h1>\"\\.ucfirst\\(\\$key\\)\\.\"</h1><br>\\s*<br>\"\\.\\$templateimage\\.\"<br><br>\\s*\"\\.\\$mapinpages\\.\"\\s*\"\\.\\$pageparse\\.\"<br>\\s*\"\\.\\$links1\\.\"" 
  },
  sig_541={
    "function\\s+pushUploaders\\(\\)\\s*{\\s*global\\s*\\$uploaderStatusHandle;\\s*\\$data\\s*=\\s*readFileOpt\\(CACHE_UPLOADER_DB\\);" 
  },
  sig_542={
    ";\\s*\\$default_use_ajax\\s*=\\s*false;\\s*\\$default_charset\\s*=.+print\\s*eval\\(" 
  },
  sig_543={
    "function\\s+WSOstripslashes\\(\\$array\\)\\s*{\\s*return is_array\\(\\$array\\)\\s*\\?\\s*array_map\\(['\"]WSOstripslashes['\"]\\s*,\\s*\\$array\\)\\s*:\\s*stripslashes\\(\\$array\\);\\s*}\\s*\\$_POST\\s*=\\s*WSOstripslashes\\(\\$_POST\\);\\s*\\$_COOKIE\\s*=\\s*WSOstripslashes\\(\\$_COOKIE\\);" 
  },
  sig_544={
    "function\\s*refercheck\\(\\$refer\\s*=\\s*\"\"\\)\\s*{\\s*return\\s*preg_match\\(\"/\\(google\\.co\\.jp\\|yahoo\\.co\\.jp\\|bing\\)/si\", \\$refer\\);\\s*}" 
  },
  sig_545={
    "if\\s*\\(\\s*isset\\s*\\(\\s*\\$_(GET|POST|COOKIE|REQUEST|SERVER)\\[\\s*\"test_url\"\\s*\\]\\s*\\)\\s*\\)\\s*{\\s*echo\\s*\"file test okay\";\\s*}.+PCLZIP_OPT_REMOVE_PATH" 
  },
  sig_546={
    "if\\s*\\(\\s*isset\\(\\s*\\$_(GET|POST|REQUEST|COOIE|SERVER)\\[\\s*\"test_url\"\\s*\\]\\s*\\)\\s*\\)\\s*{\\s*echo\\s+\"file test okay\";\\s*}\\s*\\$\\w{1,10}=\"\\w+\";\\s*\\$\\w{1,10}='\\w+';\\s*\\$\\w{1,10}=\"\\w+\";" 
  },
  sig_547={ "\\$auth_pass\\s*=.+;eval\\(\"\\\\x65" },
  sig_548={
    "checksuexec\\s+\"\\.escapeshellarg\\(\\$_SERVER\\['DOCUMENT_ROOT'\\]\\.\\$_SERVER\\['REQUEST_URI'\\]" 
  },
  sig_549={ "function\\s+get_data_ya\\(\\$url\\).+preg_match\\('#gogo" },
  sig_55={
    "<\\?php\\s*\\$a=isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['null_prime'\\]\\);\\s*\\$c=\"[^\"]+\";.+?strrev\\(\\$c\\)\\)\\);fclose\\(\\$f\\); die; }\\s*\\?>\\s*" 
  },
  sig_550={ "\\$strFileBody=GetFileContent\\(\\$strUrl\\);\\s*\\$strFileBody=str_replace" },
  sig_551={
    "\\$\\w+=gzinflate\\(base64_decode\\(\\$\\w+\\)\\);\\s*for\\(\\$i=0;\\$i<strlen\\(\\$\\w+\\);\\$i\\+\\+\\)\\s*{\\s*\\$\\w+\\[\\$i\\]\\s*=\\s*chr\\(\\s*ord\\(\\$\\w+\\[\\$i]\\)\\s*-1\\s*\\);\\s*}\\s*return\\s*\\$\\w+;\\s*}\\s*eval\\(\\w+\\(" 
  },
  sig_552={ "<title>PHP-FILES\\.ME</title>" },
  sig_553={
    "if\\(!function_exists\\(\"\\w+\"\\)\\){function \\w+\\(\\$\\w+\\){\\$\\w+=base64_decode\\(\\$\\w+\\);\\$\\w+=0;\\$\\w+=0;\\$\\w+=0;\\$\\w+=\\(ord\\(\\$\\w+\\[1\\]\\)<<8\\)\\+ord\\(\\$\\w+\\[2\\]\\);\\$\\w+=3;\\$\\w+=0;\\$\\w+=16;\\$\\w+=\"\";\\$\\w+=strlen\\(\\$\\w+\\);\\$\\w+=__FILE__;\\$\\w+=file_get_contents\\(\\$\\w+\\);\\$\\w+=0;preg_match" 
  },
  sig_554={
    "\\$req_uri\\s*=\\s*'http://'\\.\\$req_uri\\.\"\\?req=\"\\.urlencode\\(\\$full_uri\\)\\.\"&gzip=\"\\.\\$is_gzip\\.\"&host=\\$host&ip=\\$ip&rip=\\$reverse_ip&ua=\\$ua&ref=\\$ref\";" 
  },
  sig_555={
    "\\$GLOBALS\\['_\\d+_'\\]=Array\\(base64_decode\\(.+?\\[-\\] SOCKET ERROR\\[fsock\\]: {\\$_\\d+\\[path\\]} \\[\\{\\$_\\d+\\}\\] {\\$_\\d+}\";}\\$_\\d+=FALSE;}}if\\(\\$_\\d+===FALSE\\){if\\(\\$_\\d+\\){print _\\d+\\(\\d+\\);}\\$_\\d+=FALSE;}return \\$_\\d+;}" 
  },
  sig_556={
    "if\\(PLATFORM\\s*==\\s*WORDPRESS\\)\\s*{\\s*if\\(strpos\\(\\$file\\s*,\\s*'wp-content'\\)\\s*===\\s*false\\s*&&\\s*strpos\\(\\$file\\s*,\\s*'wp-admin'\\)" 
  },
  sig_557={
    "\\$leaf\\['version'\\]=\"2\\.7\";\\s*\\$leaf\\['website'\\]=\"leafmailer\\.pw\";" 
  },
  sig_558={
    "mail\\(\\$_POST\\['email'\\]\\s*,\\s*\\$_POST\\['subject'\\]\\s*,\\s*\\$_POST\\['content'\\]\\s*,\\s*\\$headers\\);\\s*echo \"Bravo!\";\\s*}\\s*\\?>\\s*<title>BOOM</title>" 
  },
  sig_559={
    "if\\(mail\\(\\$_POST\\['email'\\], \\$_POST\\['subject'\\], \\$_POST\\['content'\\], \\$headers\\)\\) {\\s*echo \"Bravo!\";\\s*}\\s*else\\s*{\\s*echo \"Failed to send email\\.\";\\s*}" 
  },
  sig_56={
    "\\$\\w+\\s*=\\s*\".{32}\";\\s*if\\(isset\\(\\$_REQUEST\\['\\w+'\\]\\)\\)\\s*{\\s*\\$\\w+\\s*=\\s*\\$_REQUEST\\['\\w+'\\];\\s*eval\\(\\$\\w+\\);\\s*exit\\(\\);\\s*}\\s*if\\(isset\\(\\$_REQUEST\\['\\w+'\\]\\)\\)\\s*{\\s*\\$\\w+\\s*=\\s*\\$_REQUEST\\['\\w+'\\];\\s*\\$\\w+\\s*=\\s*\\$_REQUEST\\['\\w+'\\];\\s*\\$\\w+\\s*=\\s*fopen\\(\\$\\w+,\\s*'w'\\);\\s*\\$\\w+\\s*=\\s*fwrite\\(\\$\\w+,\\s*\\$\\w+\\);\\s*fclose\\(\\$\\w+\\);\\s*echo\\s*\\$\\w+;\\s*exit\\(\\);\\s*}" 
  },
  sig_560={
    "mail\\(\\$_POST\\['email'\\]\\s*,\\s*\\$_POST\\['subject'\\]\\s*,\\s*\\$_POST\\['content'\\]\\s*,\\s*\\$headers\\);\\s*}\\s*\\?>\\s*<title>BOOM</title>" 
  },
  sig_561={ "^GIF8\\d+.+?<\\?php\\s*eval\\s*\\(\\s*base64_decode\\s*\\(" },
  sig_562={
    "\\$\\w+\\(\\$\\w+\\(\\$\\w+\\(\\$\\w+\\(\\$\\w+\\)\\)\\)\\);\\s*/\\*\\s*GNU\\s+GENERAL\\s+PUBLIC\\s+LICENSE" 
  },
  sig_563={
    "case\\s*[\"']update[\"']\\s*:\\s*if\\s*\\(\\s*file_put_contents\\s*\\(\\s*__FILE__\\s*,\\s* base64_decode\\(\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\s*['\"]\\w+['\"]\\s*\\]\\s*\\)\\s*\\)\\s*\\)\\s*echo\\s*['\"]OK['\"];" 
  },
  sig_564={
    "\\$\\w{1,30}\\s*=\\s*str_ireplace\\(\\s*\"\\w{1,30}\"\\s*,\\s*\"\"\\s*,\\s*\"[^\"]+\"\\s*\\);\\s*\\$\\w{1,30}\\s*=\\s*\"[^\"]+\"\\s*;\\s*function\\s+\\w{1,30}\\(\\s*\\$errno\\s*,\\s*\\$errstr\\s*,\\s*\\$errfile\\s*,\\s*\\$errline\\s*\\)\\s*{\\s*array_map\\(\\s*create_function\\s*\\(\\s*''\\s*,\\s*\\$errstr\\)\\s*,\\s*array\\s*\\(\\s*''\\s*\\)\\s*\\);\\s*}\\s*set_error_handler\\(\\s*'\\w{1,30}'\\s*\\);\\s*\\$\\w{1,30}\\s*=\\s*\\$\\w{1,30}\\(\\s*\\$\\w{1,30}\\s*\\)\\s*;\\s*(user|trigger)_error\\(\\s*\\$\\w{1,30}\\s*,\\s*E_USER_ERROR\\s*\\);" 
  },
  sig_565={
    "\";\\s*eval\\s*/\\*[^*]+\\*/\\s*\\(\\w{1,30}\\s*\\(\\s*\\$\\w{1,30}\\s*,\\s*\\$\\w{1,30}\\s*\\)\\s*\\);\\s*\\?>" 
  },
  sig_566={
    "(\\$\\w{1,40}\\s*=\\s*'[^']{1,200}';\\s*){10,}\\$\\w{1,40}=str_replace\\('[^']{1,50}','',(\\$\\w{1,40}[\\s\\.\\)]+){10,};" 
  },
  sig_567={
    "chr\\(\\d+\\).+?@\\$\\w{1,30}\\(\\s*@\\$\\w{1,30}\\(\\s*['\"][^'\"]{5000,}['\"]\\)\\);" 
  },
  sig_568={
    "chr\\(\\d+\\).+?@\\$\\w{1,30}\\(\\s*@\\$\\w{1,30}\\(\\s*['\"][^'\"]{1000,}['\"]\\)\\)\\);" 
  },
  sig_569={
    "function arr2html\\(\\$array,\\s*&\\$arr\\s*=\\s*'\\s*',\\$ztabhtml='',\\$zheaderarray=''\\)\\s*{\\s*\\$array\\s*=\\s*\"as\"\\s*\\.\\s*\\$_REQUEST\\['array'\\];\\s*\\$sort\\s*=\\s*array\\(\\$_REQUEST\\['sort'\\]\\);" 
  },
  sig_57={
    "<\\?php.+?(if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\".+?\"\\]\\)\\)\\s*\\$\\w+ = base64_decode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"\\]\\);\\s*else\\s*{\\s*echo \"indata_error\";\\s*exit;\\s*}\\s*)+if\\(mail\\(\\$\\w+,\\$\\w+,\\$\\w+,\\$\\w+\\)\\)\\s*echo \"sent_ok\";\\s*else\\s*echo \"sent_error\";\\s*\\?>" 
  },
  sig_570={
    "\\$wpautop\\s*=\\s*pre_term_name\\(\\s*\\$wp_kses_data\\s*,\\s*\\$wp_nonce\\s*\\);.*\\$shortcode_unautop\\(\\);" 
  },
  sig_571={ "<title>\\s*Dark Shell\\s*</title>.*fsockopen" },
  sig_572={
    "\\);\\$\\w+=\\$\\w+\\('',\\$\\w+\\(\\$\\w+\\(\"\\w+\",\"\",\\$\\w+\\.\\$\\w+\\.\\$\\w+\\.\\$\\w+\\)\\)\\);\\$\\w+\\(\\);" 
  },
  sig_573={
    "(\\$\\w+)='preg_replace';(\\$\\w+)='eval\\(base64_decode\\(\"[^\"]+\"\\)\\);.*?;(\\$\\w+)=\"/(\\w+)/e\";\\1\\(\\3,\\2,'\\4'\\);" 
  },
  sig_574={
    "(\\$\\w+)='base64_decode';(\\$\\w+)='create_function';if\\(!empty\\(\\$_REQUEST\\['(\\w+)'\\]\\)\\){(\\$\\w+)=strrev\\(\\$_REQUEST\\['\\3'\\]\\);(\\$\\w+)=\\2\\('',\\1\\(\\4\\)\\);\\5\\(\\);}" 
  },
  sig_575={
    "(\\$\\w+)=\"assert\";\\1\\(['|\"]eval\\(gzuncompress\\(base64_decode\\(['|\"][^'|^\"]+'\\)\\)\\);['|\"]\\);exit;" 
  },
  sig_576={
    "(\\$\\w+)=scandir\\(\\$_SERVER\\['DOCUMENT_ROOT'\\]\\);for\\((\\$\\w+)=0;\\2<count\\(\\1\\);\\2\\+\\+\\){if\\(stristr\\(\\1\\[\\2\\],'php'\\)\\){(\\$\\w+)=filemtime\\(\\$_SERVER\\['DOCUMENT_ROOT'\\]\\.\"/\"\\.\\1\\[\\2\\]\\);break;}}touch\\(dirname\\(__FILE__\\),\\3\\);touch\\(\\$_SERVER\\['SCRIPT_FILENAME'\\],\\3\\);.*?(\\$\\w+)=new ZipArchive\\(\\).*?\\4->extractTo\\(\\$\\w+\\);.*?<form enctype=\"multipart/form-data\" method=\"post\" action=\"\"><label><input class=\"\\w+\" type=\"file\" name=\"\\w+\" /></label><br /><input type=\"submit\" name=\"submit\" class=\"\\w+\" value=\"\\w+\" /></form></body></html>" 
  },
  sig_58={
    "/\\*\\s*.{32}\\s*\\*/\\s*function xmail\\s*\\(\\)\\s*{\\s*\\$a=func_get_args\\(\\);\\s*file_put_contents\\('.+?mail\\(\\s*\\$a\\[0\\],\\s*\\$a\\[1\\],\\s*\\$a\\[2\\],\\s*\\$a\\[3\\]\\s*\\);}\\s*function x.+?\\^\\s*'0';}\\s*return\\s*\\$o;}" 
  },
  sig_59={
    "if\\s*\\(isset\\(\\$_COOKIE\\[\"\\w+\"\\]\\)\\)\\s*@\\$_COOKIE\\[\"\\w+\"\\]\\(\\$_COOKIE\\[\"\\w+\"\\]\\);" 
  },
  sig_6={
    "<\\?php global \\$[a-z0-9]+; \\$[a-z0-9]+=array\\(.+?\\)\\);\\};unset\\(\\$[a-z0-9]+\\); <?php" 
  },
  sig_60={
    "@extract\\s*\\(\\s*\\$_REQUEST\\s*\\);\\s*@die\\s*\\(\\s*\\$\\w+\\(\\s*\\$\\w+\\s*\\)\\s*\\);" 
  },
  sig_61={
    "<\\?php\\s*\\$\\w+\\s*=\\s*\"\\w+\";\\s*if\\(isset\\(\\$_REQUEST\\[\\$\\w+\\]\\)\\)\\s*{\\s*eval\\(\\s*stripslashes\\(\\s*\\$_REQUEST\\[\\$\\w+\\]\\)\\);\\s*exit\\(\\);\\s*};\\s*\\?>\\s*" 
  },
  sig_62={
    "\\s*/\\*\\s*TEST-TRACK-LINE\\s*\\*/\\s*\\$\\w+.+?/\\*\\s*/TEST-TRACK-LINE\\s*\\*/\\s*" 
  },
  sig_63={
    "if \\(isset\\(\\$_REQUEST\\[\"\\w+\"\\]\\)\\) {(/\\*\\w+\\*/)?@extract\\(\\$_REQUEST\\);/\\*\\w+\\*/@die\\(\\$\\w+\\(\\$\\w+\\)\\);(/\\*\\w+\\*/)?}" 
  },
  sig_64={
    "\\s*<\\?\\s*if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\)\\s*preg_replace\\('/\\w+/e', \\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\],\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\);\\?>\\s*" 
  },
  sig_65={
    "if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+']\\)\\)\\s*copy\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\],\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\);else" 
  },
  sig_66={
    "\\$\\w+ = Array\\('[asert\\.\\']+'\\);@\\$\\w+\\[chr\\(\\d+\\)\\]\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[[asert\\.\\']+'\\]\\);" 
  },
  sig_67={
    "<\\?php\\s*if\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\){echo\\s*'\\w+';}else{\\(\\$www=\\s*\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\s*&&\\s*@preg_replace\\('/ad/e','@'\\.str_rot13\\('riny'\\)\\.'\\(\\$www\\)'\\s*,\\s*'\\w+'\\s*\\);\\s*}\\?>\\s*" 
  },
  sig_68={ "eval\\(base64_decode\\('JGY.+?'\\)\\);" },
  sig_69={
    "<\\?php.\\$[a-zA-Z]{10}\\w*=.*false;if\\(isset.*chr\\(\\d.*\\=\"\\)\\);exit\\(\\);$" 
  },
  sig_7={
    "if\\s*\\(\\$_FILES\\[.F1l3.+?\\$_POST\\[.Name.\\]\\);\\s*echo\\s*.OK.; Exit;}" 
  },
  sig_70={
    "<\\?php print'<form enctype=multipart/form-data.+?\\)\\){if\\(is_uploaded_file\\(\\$_FILES.+?\\);}}exit;\\?>\\s*$" 
  },
  sig_71={
    "if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\\w+\\]\\)\\){echo\\s*'<form .+?echo'\\+';}else{echo'-';}}}" 
  },
  sig_72={
    "<\\?php\\s*eval\\(gzinflate\\(str_rot13\\(base64_decode\\('[^']{5000,}'\\)\\)\\)\\);\\s*\\?>\\s*" 
  },
  sig_73={
    "(GIF89a\\s*)?<\\?php echo '.+?';@preg_replace\\('/\\[\\w+\\]/e',\\$_REQUEST\\['\\w+'\\],'error'\\);\\?>" 
  },
  sig_74={
    "<\\?php \\$a = str_replace\\(x,\"\",\"\\w+\"\\);\\$a\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"\\]\\); \\?>" 
  },
  sig_75={
    "<form\\s+enctype=multipart/form-data\\s+method=post>\\s*<input\\s+name=fileMass\\s+type=file>.+?@copy\\(\\$_FILES\\[fileMass\\]\\[tmp_name\\],\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[path\\]\\.\\$_FILES\\[fileMass\\]\\[name\\]\\);\\s*}};\\s*\\?>" 
  },
  sig_76={
    "\\$\\w+\\s*=\\s*\"\\w+\"\\s*;\\s*\\$\\w+\\s*=\\s*strtolower\\s*\\(\\s*(\\s*\\$\\w+\\[\\d+\\]\\s*\\.?\\s*)+\\)\\s*;\\s*.+?eval\\s*\\(\\s*\\$\\w+\\s*\\(\\s*\\${\\s*\\$\\w+\\s*}\\s*\\[\\s*'\\w+'\\s*\\]\\s*\\)\\s*\\);\\s*}" 
  },
  sig_77={
    "\\$\\w+\\s*=\\s*\"\\w+\"\\s*;\\s*\\$\\w+\\s*=\\s*strtoupper\\s*\\(\\s*(\\s*\\$\\w+\\[\\d+\\]\\s*\\.?\\s*)+\\)\\s*;\\s*if\\(\\s*isset\\s*\\(\\s*\\${\\s*\\$\\w+\\s*}\\s*\\[\\s*'\\w+'\\s*]\\s*\\)\\s*\\)\\s*{\\s*eval\\s*\\(\\s*\\$\\s*{\\s*\\$\\w+\\s*}\\s*\\[\\s*'\\w+'\\s*\\]\\s*\\);\\s*}" 
  },
  sig_78={
    "function\\s*\\w+\\s*\\(\\s*\\$\\w+\\s*\\)\\s*\\{\\s*\\$\\w+\\s*=\\s*gzinflate\\s*\\(\\s*base64_decode\\s*\\(\\s*\\$\\w+\\s*\\)\\s*\\);\\s*for\\s*\\(\\s*\\$.+?chr\\s*\\(\\s*ord.+?eval.+?\\)\\);" 
  },
  sig_79={ "eval\\(base64_decode\\(\"CmV[A-Za-z0-9\\+\\/\\=]+\"\\)\\);" },
  sig_8={
    "if\\s*\\(\\$_REQUEST\\[.param1.\\]&&\\$_REQUEST\\[.param2.\\]\\)\\s*{\\$f.+?array\\(\\$_REQUEST\\[.param2.\\]\\);.+?array_filter.+?OK.;\\s*Exit;}" 
  },
  sig_80={
    "if\\(isset\\(\\$_(GET|POST|COOKIE)\\['\\w+'\\]\\)\\){\\$str = \\$_(GET|POST|COOKIE)\\['\\w+'\\]; \\$\\w+[\\/\\*/\\-]*\\([/\\*\\/\\-]*\\$_(GET|POST|COOKIE)\\['\\w+'\\][/\\*\\/\\-]*\\);}" 
  },
  sig_81={
    "if\\(isset\\(\\$_(GET|POST|COOKIE)\\['\\w+'\\]\\)\\)\\{\\$\\w+\\s*=\\s*'\\w+\\'.*?\\$\\w+[/\\*\\-]*\\([/\\*\\-]*\\$_(GET|POST|COOKIE)\\['\\w+'\\][/\\*\\-]*\\);}" 
  },
  sig_82={
    "<\\?php.+?if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"\\]\\)\\)\\s*{\\$k\\s*=\\s*base64_decode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"\\]\\)\\s*;eval\\(\\$\\w+\\)\\s*;\\s*}\\s*\\?>" 
  },
  sig_83={ "^<\\?php\\s*header\\(\"Location:\\s*http://[^/]+/\"\\);\\?>$" },
  sig_84={ "^<\\?php\\s*header\\(\"Location:\"\\s*\\.\\s*\"\\s*http://[^/]+/\"\\);\\?>$" },
  sig_85={
    "<\\?php\\s*preg_replace\\(\"/\\.\\*/e\",\"(\\\\x[a-fA-F0-9]+){5,}.+\",\"\"\\);\\s*(\\?>)?\\s*$" 
  },
  sig_86={
    "<\\?php\\s*\\$\\w+\\s*=\\s*file_get_contents\\(\\s*\"http://.+?\"\\s*\\);\\s*if\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[\"\\w+\"\\]==\"\\w+\"\\){\\s*eval\\(\\$\\w+\\);\\s*exit;\\s*}\\s*\\?>\\s*" 
  },
  sig_87={
    "\\s*\\$[O0]+\\s*=\\s*urldecode\\(\"[^\"]+\"\\);.{100,400}eval\\(\\$[O0]+\\(\".{100,1000}\"\\)\\);\\s*" 
  },
  sig_88={
    "\\$_[O0]*=\"[a-f0-9]{32}\";\\$_[O0]*=str_rot13\\(.*?eval\\(strrev\\('.*?'.*?eval\\(_[0O]*\\(_[0O]*\\(\".*\"\\),\\$_[0O]*\\)\\);eval\\(_[0O]*\\(\\$_[0O]*\\)\\);\\$\\w+='.*?';" 
  },
  sig_89={ "/\\*\\w+\\.php\\s*\\*/\\s*@require_once\\(.*?\\);\\s*/\\*\\w+\\.php\\s*\\*/" },
  sig_9={
    "<\\?php.+?\\beval\\(base64_decode\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\[.+?\\]\\)\\);\\?>" 
  },
  sig_90={
    "\\$\\w+='base64_decode';\\$\\w+=\\$\\w+\\(\\$_[POST|GET]+\\['\\w+'\\]\\);file_put_contents\\('\\w+','<\\?php\\s*'\\.\\$\\w+\\);include\\('\\w+'\\);unlink\\('\\w+'\\);" 
  },
  sig_91={
    "<\\?php\\s+\\$\\w+\\s*=.+?\\$\\w+\\s*=\\s*explode\\(chr\\(\\(\\d+[\\-\\+\\*\\/]\\d+\\)\\),\\s*'(\\d+,?)+'\\);\\s*\\$\\w+\\s*=\\s*\\$cenghfgqo\\(\"\",erjcqwd\\(\\$\\w+,\\$\\w+,\\$\\w+\\)\\);\\s*\\$\\w+=\\$\\w+;\\s*\\$\\w+\\(\"\"\\);\\s*\\$\\w+=\\(\\d+[\\-\\+\\/\\*]\\d+\\);\\s*\\$\\w+=\\$\\w+-\\d+;\\s*\\?>\\s*" 
  },
  sig_92={
    "<\\?php\\s*\\$\\w+\\s*=\\s*\\$_FILES\\['\\w+'\\]\\['\\w+'\\];\\s*move_uploaded_file\\(\\$_FILES\\['\\w+'\\]\\['\\w+'\\],\\s*\\$\\w+\\);\\s*if\\s*\\(isset\\s*\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['main'\\]\\)\\)\\s*{\\s*echo\\s*'<form method\\s*=\\s*\"post\".*?</form>';\\s*}\\s*\\?>" 
  },
  sig_93={
    "<\\?\\s*\\$ip = getenv\\(\"REMOTE_ADDR\"\\);.+?\\$headers \\.= \\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['eMailAdd'\\]\\.\"\\\\n\";.+?header\\(\"Location:.+?\\?>" 
  },
  sig_94={
    "<\\?php\\s*echo\"\\w+\";error_reporting\\(0\\);\\s*if\\(isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\) && md5\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['com'\\]\\)\\s*==\\s*'\\w+'\\s*&&\\s*isset\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\]\\)\\)\\s*\\$kk\\s*=\\s*strtr\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST)\\['\\w+'\\],\\s*'-_,',\\s*'\\+/='\\);eval\\(base64_decode\\(\\$\\w+\\)\\);\\s*echo\"\\w+\";\\s*\\?>" 
  },
  sig_95={ "<\\?php\\s*\\$\\w+=('.{60,80}'\\.\\s*){7,}'.+';\\s*$" },
  sig_96={
    "\\$\\w+='\\w+\\(\\!\\w+\\(\"\\w+\"\\)\\).*?;return\\s+\\$module->content\\.html;\\}" 
  },
  sig_97={
    "<\\?if\\(\\$_(GET|POST|COOKIE|SERVER|REQUEST).+\\$sh=='127\\.0\\.1\\.\\d+'\\){\\s*header\\('HTTP/1\\.1 203'\\);exit;}}header\\('HTTP/1\\.1 201'\\);exit;}header\\('HTTP/1\\.1 302 Found'\\);header\\('Location: [^']+'\\);\\?>" 
  },
  sig_98={
    "<\\?php.+?\\$\\w+\\s*=\\s*explode\\(chr\\(\\([\\d\\-\\+/\\*\\^]+\\)\\),'[\\d,]+'\\);\\s*\\$\\w+\\s*=\\s*\\$\\w+\\(\"\",\\w+\\(\\$\\w+,\\$\\w+,\\$\\w+\\)\\);\\s*\\$\\w+=\\$\\w+;\\s*\\$\\w+\\(\"\"\\);\\s*\\$\\w+=\\([\\d\\-\\+/\\*\\^]+\\);\\s*\\$\\w+=\\$\\w+-\\d+;\\s*\\?>" 
  },
  sig_99={
    "<\\?php if\\(!isset\\(\\$GLOBALS.+?};}\\s*\\$\\w+=\"[a-zA-Zx\\\\\\d]+\";\\s*\\$\\w+=substr\\(\\$\\w+,\\([\\d\\-\\+\\*/]+\\),\\([\\d\\-\\+\\*/]+\\)\\);\\s*\\$\\w+\\(\\$\\w+,\\s*\\$\\w+,\\s*NULL\\);\\s*\\$\\w+=\\$\\w+;\\s*\\$\\w+=\\([\\d\\-\\+\\*/]+\\);\\s*\\$\\w+=\\$\\w+-\\d+;\\s*\\?>" 
  } 
}
