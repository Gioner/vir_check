#!./luajit
--------------------------------------------------------------------------------

excluded_urls = {'/', '/wp-admin/admin-ajax.php', '/wp-cron.php', '/bitrix/admin/index.php', '/ajax.php', '/wp-login.php',
'/wp-admin/post.php', '/wp-admin/admin-post.php', '/bitrix/components/bitrix/catalog.bigdata.products/ajax.php',
'/bitrix/components/bitrix/pull.request/ajax.php', '/admin/index.php', '/wp-comments-post.php', '/', '/index.php',
'/login/index.php', '/xmlrpc.php', '/ucp.php', '/order.php', '/bitrix/components/bitrix/catalog.element/ajax.php',
'/bitrix/tools/conversion/ajax_counter.php', '/login.php', '/register.php',
'/administrator/index.php', '/wp-admin/async-upload.php', '/wp-admin/options-permalink.php',
'/manager/index.php',
'/bitrix/admin/iblock_element_edit.php', '/bitrix/components/idea/vote.registred/ajax.php',
'/bitrix/admin/1c_exchange.php',
'/wp-admin/admin.php',
'/administrator/components/com_joomlaupdate/restore.php',
'/forum/login.php','/forum/search.php',
'/forum/profile.php',
'/bisend.php',
'/ajax/ajax_lastorders.php', -- Mb bad
'/payment.php' -- May be custom eval.

}

excluded_files = { 'virusdie', 'tgbots', 'blankprint', 'installer.php', 'vasjon', 'basket','bimailer'}

function get_files_from_nginx_log()
    local nginx_log = io.open(default_nginx_log_path, 'r')

     -- Create structure for analyze
    WORKING_TABLE = {}
    for line in nginx_log:lines() do
        local domain, ip, method, uri_query, answer = parse_nginx_log(line)
        build_domain(domain, ip, method, uri_query, answer)

    end

    nginx_log:close() -- Close file

    -- Fill structure with root_dir
    fill_root_dir(WORKING_TABLE)

    return WORKING_TABLE

end

function get_server_domains(server_name)

    local output_value = ''
    local function output_function(string)
        output_value = output_value..string
    end


    local url = '192.168.111.104:28283/get_domains?pass=benderok&server=%s'
    url = url:format(server_name)
    curl.easy()
      :setopt_url(url)
      :setopt_writefunction(output_function)
      :perform()
    :close()
    if output_value == "null\n" then
        return {}
    end

    return cjson.decode(output_value)
end

function get_all_log()
    local domains_list = get_server_domains(hostname)
    local nginx_log = io.open(default_nginx_log_path, 'r')
     -- Create structure for analyze
    LOG_TABLE = {}
    for line in nginx_log:lines() do
        local domain, ip, method, uri_query, answer = parse_nginx_log(line)
        for k, avlb_domain in pairs(domains_list) do
            if domain == avlb_domain then
                table.insert(LOG_TABLE, line)
            end
        end

    end

    nginx_log:close() -- Close file


    return LOG_TABLE
end

function get_files_from_clickhouse_log(hostname, rdate, rtime)
    local house_query = [[
    Select distinct (domain, ip, request, status)
      from access_logs
    where
      hostname='%s'
    and
      request like ('POST%%')
    and
      rdate='%s'
    and
      rtime>'%s'
    ]]
    local house_query = house_query:format(hostname,rdate,rtime)
    local raw_data    = get_info(house_query)

    local exploded_data = explode(raw_data, '\n')

    -- Set last element nil because is empty line
    exploded_data[#exploded_data] = nil


    -- Create structure for analyze
    WORKING_TABLE = {}
    for k, line in pairs(exploded_data) do
        -- local domain, ip, method, uri_query, answer = parse_clickhouse_log(line)
        local domain, ip, method, uri_query, answer  =  string.match(line, "%('(.+)','(.+)','(.+) (.+)',(%d%d%d)%)")
        build_domain(domain, ip, method, uri_query, answer)

    end


    fill_root_dir(WORKING_TABLE)

    return WORKING_TABLE

end

function fill_root_dir(table_domains)
    -- Fill structure with root_dir
    for domain, info in pairs(table_domains) do
        local nginx_config_path = default_nginx_vhosts_path..domain
        local status, nginx_config_content = pcall(get_file, nginx_config_path)
        if status == true then
            local root_dir = rex.match(nginx_config_content, '(?<=root)(.*home.*)(?=;\n)')
            if root_dir == nil then
                --print(nginx_config_path)
            else -- ALL TO APACHE TODO
                table_domains[domain].root_dir = trim(root_dir)
            end
        end
    end
    return table_domains
end

function build_domain(domain, ip, method, request_query, answer)
    -- Match only this type of request
    local request_query = rex.match(request_query, '(.*?php)') or '/'

    -- Exclude block
    for k, exclude_request in pairs(excluded_urls) do
       if request_query == exclude_request then
           goto skip_request -- Skip excluded request
       end
    end
    for k, file in pairs(excluded_files) do
       if string.find(request_query, file) then
           goto skip_request -- Skip excluded request
       end
    end

    -- Math only POST and certain http answers
    if method == 'POST' and (answer ~= '403' and answer ~= '404' and answer ~= '405') then

        if WORKING_TABLE[domain] == nil then
            WORKING_TABLE[domain]          = {}
            WORKING_TABLE[domain].requests  = {}
            WORKING_TABLE[domain].root_dir = ''
        end



        if WORKING_TABLE[domain].requests[request_query] == nil then
            WORKING_TABLE[domain].requests[request_query]             = {}
            WORKING_TABLE[domain].requests[request_query].ip          = ip
            WORKING_TABLE[domain].requests[request_query].virus_types = {}
        end


    end
    ::skip_request:: -- Skip excluded request
end

function get_files_from_test_dir(test_dir_path)
    local domain = 'test'

    local
    table_for_test              = {}
    table_for_test[domain]      = {}
    table_for_test[domain].urls = {}
    table_for_test[domain].root_dir = test_dir_path

    local checked_files = list_dir(test_dir_path)


    for k,file_name in pairs(checked_files) do
        table_for_test[domain].urls[file_name] = 1;
    end

    return table_for_test
end
